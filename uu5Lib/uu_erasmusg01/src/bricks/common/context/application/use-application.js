
//@@viewOn:imports
import { useContext } from "uu5g04-hooks";
import Context from "./application-context";
//@@viewOff:imports

export function useApplication() {
  return useContext(Context);
}
export default useApplication;
