
//@@viewOn:imports
import { useContext } from "uu5g04-hooks";
import Context from "./application-list-context";
//@@viewOff:imports

export function useApplicationList() {
  return useContext(Context);
}
export default useApplicationList;
