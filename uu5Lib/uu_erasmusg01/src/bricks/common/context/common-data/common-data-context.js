//@@viewOn:imports
import UU5 from "uu5g04";
//@@viewOff:imports

export const CommonDataContext = UU5.Common.Context.create();
export default CommonDataContext;
