
import { useContext } from "uu5g04-hooks";
import Context from "./common-data-context";
//@@viewOff:imports

export function useCommonData() {
  return useContext(Context);
}
export default useCommonData;
