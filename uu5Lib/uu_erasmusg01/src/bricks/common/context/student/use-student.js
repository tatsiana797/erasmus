
//@@viewOn:imports
import { useContext } from "uu5g04-hooks";
import Context from "./student-context";
//@@viewOff:imports

export function useStudent() {
  return useContext(Context);
}

export default useStudent;
