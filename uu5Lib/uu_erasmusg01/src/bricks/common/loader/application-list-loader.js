//@@viewOn:imports
import UU5 from "uu5g04";
import { createComponent, useDataObject } from "uu5g04-hooks";
import "uu_plus4u5g01-bricks";
import Config from "../../config/config";
import Calls from "calls";
import { ApplicationListContext } from "../context/applicaton-list/context";
//@@viewOff:imports

export const ApplicationListLoader = createComponent({
  //@@viewOn:statics
  displayName: Config.TAG + "ApplicationListLoader",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {
    const applicationDataList = useDataObject({
      handlerMap: {
        load: handleLoad,
        update: handleUpdateList,
        removeAttachments: handleRemoveAttachments,
      },
    });

    //@@viewOn:handlers
    async function handleLoad() {
      return await Calls.applicationList(getInitLoadDtoIn(props.baseUri))
    }
    async function handleUpdateList(dtoIn) {
      return await Calls.applicationUpdateList(props.baseUri, dtoIn)
    }
    async function handleRemoveAttachments(dtoIn) {
      return await Calls.removeAttachments(props.baseUri, dtoIn)
    }
    //@@viewOff:handlers
    //@@viewOn:render
    return <ApplicationListContext.Provider value={applicationDataList}>{props.children}</ApplicationListContext.Provider>;
    //@@viewOff:render
  },
});

function getInitLoadDtoIn(baseUri) {
  let dtoIn = {};
  if (baseUri) {
    dtoIn.baseUri = baseUri;
  }
  return dtoIn;
}
//@@viewOn:helpers
//@@viewOff:helpers

export default ApplicationListLoader;
