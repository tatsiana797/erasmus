//@@viewOn:imports
import UU5 from "uu5g04";
import { createComponent, useDataObject } from "uu5g04-hooks";
import { ApplicationContext } from "../context/application/context";
import Config from "../../config/config.js";
import Calls from "calls";
//@@viewOff:imports

export const ApplicationLoader = createComponent({
  //@@viewOn:statics
  displayName: Config.TAG + "ApplicationLoader",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
    uuIdentity: UU5.PropTypes.string,
    skipInitialLoad: UU5.PropTypes.bool
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
    uuIdentity: undefined,
    skipInitialLoad: false
  },
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:hooks
    const applicationDataObject = useDataObject({
      handlerMap: {
        load: handleLoad,
        updateAttachment: handleUpdateAttachment,
        update: handleUpdate,
        removeAttachments: handleRemoveAttachments,
      },
      initialDtoIn: { uuIdentity: props.uuIdentity },
       skipInitialLoad: props?.skipInitialLoad
    });
    //@@viewOff:hooks

    //@@viewOn:handlers
    async function handleLoad() {
      return await Calls.getApplicationByUuIdentity(props.baseUri, { uuIdentity: props.uuIdentity })
    }
    async function handleUpdateAttachment(dtoIn) {
      return Calls.updateAttachment(props.baseUri, dtoIn)
    }
    async function handleUpdate(dtoIn) {
      return await Calls.applicationUpdate(props.baseUri, dtoIn)
    }
    async function handleRemoveAttachments(dtoIn) {
      return await Calls.removeAttachments(props.baseUri, dtoIn)
    }
    //@@viewOff:handlers

    //@@viewOn:render
    return <ApplicationContext.Provider value={applicationDataObject}>{props.children}</ApplicationContext.Provider>;
    //@@viewOff:render
  }
});

export default ApplicationLoader;
