//@@viewOn:imports
import UU5 from "uu5g04";
import { createComponent, useDataObject } from "uu5g04-hooks";
import "uu_plus4u5g01-bricks";
import Config from "../../config/config";
import Calls from "calls";
import { CommonDataContext } from "../context/common-data/context";
//@@viewOff:imports

export const CommonDataLoader = createComponent({
  //@@viewOn:statics
  displayName: Config.TAG + "CommonDataLoader",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {

    const commonDataList = useDataObject({
      handlerMap: {
        load: handleLoad,
        updateCommonData: handleUpdateCommonData,
        createCommonData: handleCreateCommonData
      },
      initialDtoIn: getInitLoadDtoIn(props.baseUri)
    });

    //@@viewOn:handlers
    async function handleLoad() {
      return await Calls.listErasmusInfo(getInitLoadDtoIn(props.baseUri))
    }
    async function handleUpdateCommonData(dtoIn) {
      return await Calls.updateErasmusInfo(props.baseUri, dtoIn)
    }
    async function handleCreateCommonData(dtoIn) {
      return await Calls.addErasmusInfo(props.baseUri, dtoIn)
    }

    //@@viewOff:handlers
    //@@viewOn:render
    return <CommonDataContext.Provider value={commonDataList}>{props.children}</CommonDataContext.Provider>;
    //@@viewOff:render
  },
});

function getInitLoadDtoIn(baseUri) {
  let dtoIn = {};
  if (baseUri) {
    dtoIn.baseUri = baseUri;
  }
  return dtoIn;
}
//@@viewOn:helpers
//@@viewOff:helpers

export default CommonDataLoader;
