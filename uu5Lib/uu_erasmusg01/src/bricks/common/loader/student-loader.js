//@@viewOn:imports
import UU5 from "uu5g04";
import { createComponent, useDataObject } from "uu5g04-hooks";
import { StudentContext } from "../context/student/context";
import Config from "../../config/config.js";
import Calls from "calls";
//@@viewOff:imports

export const StudentLoader = createComponent({
  //@@viewOn:statics
  displayName: Config.TAG + "StudentLoader",
  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
    uuIdentity: UU5.PropTypes.string
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
    uuIdentity: undefined
  },
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:hooks
    const studentDataObject = useDataObject({
      handlerMap: {
        load: handleLoad,
      },
      initialDtoIn: { uuIdentity: props.uuIdentity },
    });
    //@@viewOff:hooks

    //@@viewOn:handlers
    async function handleLoad() {
      return await Calls.getStudent(props.baseUri, { uuIdentity: props.uuIdentity })
    }
    //@@viewOff:handlers

    //@@viewOn:render
    return <StudentContext.Provider value={studentDataObject}>{props.children}</StudentContext.Provider>;
    //@@viewOff:render
  }
});

export default StudentLoader;
