//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./erasmus-applicants-list-lsi"
import "uu5richtextg01";
import Css from "./erasmus-list-css"
import { useEffect, useState } from "react";
//@@viewOff:imports

const AddCommonNote = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "AddCommonNote",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        onCancel: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        noteValue: UU5.PropTypes.string,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        noteValue: undefined,
        shown: false,
        onCancel: () => { },
        onSave: () => { },
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {
        return (
            <UU5.Forms.ContextModal
                className={Css.ButtonPadding()}
                shown={props.shown}
                header={<Header />}
                footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                onClose={props.onCancel}
                size="l"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    onSave={props.onSave}
                    onSaveDone={props.onSaveDone}
                    onSaveFail={props.onSaveFail}
                    onCancel={props.onCancel}
                    noteValue={props?.noteValue}
                    colorSchema={props.colorSchema}
                    borderRadius={props.borderRadius}
                />
            </UU5.Forms.ContextModal>
        );
        //@@viewOff:render
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return (
        <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} controlled={false} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.submit} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
    );
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.addCommonNote} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.sharedNoteInfo} />}
        />
    );
}


function Form({ onSave, onCancel, onSaveDone, onSaveFail, borderRadius, colorSchema, noteValue }) {

    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onSaveDone={onSaveDone}
            onSaveFail={onSaveFail}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            {/* {value !== undefined && (
      
            )} */}
            {noteValue !== undefined && (<UU5.Bricks.Section >
                <UU5.Bricks.Text content={<UU5.Bricks.Lsi lsi={Lsi.sharedNoteHeader} />} />
                <UU5.RichText.EditorInput colorSchema={colorSchema} borderRadius={borderRadius} name="commonNote" value={noteValue} />
            </UU5.Bricks.Section>)}
        </UU5.Forms.ContextForm>
    )
};
export { AddCommonNote }
export default AddCommonNote;
