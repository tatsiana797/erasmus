import UU5 from "uu5g04";
import Config from "../../config/config.js";

const TAG = Config.TAG + "ErasmusApplicantsList.";

export default {
  ...Config,
  SEMESTER: {
    FISRT: "1",
    SECOND: "2",
    THIRD: "3",
    FOURTH: "4",
    FIFTH: "5",
    SIXTH: "6",
    SEVENTH: "7",
    EIGHTH: "8",
    NINTH: "9",
    TENTH: "10"
  },

  UNIVERSITY: [
    "Haute Ecole EPHEC, Belgium",
    "Business Academy Aarhus, Denmark",
    "Escuela Superior Politécnica del Litoral, Ecuador",
    "Laurea University of Applied Sciences, Finland",
    "EPITA School of Engineering and Computer Science, France",
    "Kedge business school, France",
    "University Niccolò Cusano, Italy",
    "PA College, Cyprus",
    "The University of Economics and Culture, Latvia",
    "University of Applied Sciences Würzburg-Schweinfurt, Germany"
  ],

  SPECIALIZATION: {
    ECONOM_MANAGEMENT: {
      en: "Economics and Management",
      cs: "Ekonomika a management"
    },
    IT: {
      en: "Information Technology",
      cs: "Informační technologie"
    },
    ICT_PROJECT_MANAGEMENT: {
      en: "ICT Project Management",
      cs: "Řízení projektů ICT"
    }
  },

  LANGUAGE: {
    WELL: { en: "Very well", cs: "Velmi dobře" },
    OK: { en: "More or less OK", cs: "Víceméně OK" },
    SURVIVAL: { en: "Survival level", cs: "Úroveň přežití" }
  },

  UUECC_COMPONENT_TAG_ERASMUS_APPLICANTS_LIST: "UuErasmus.Bricks.ErasmusApplicantsList",
  erasmusApplicationState: [
    {
      code: "active",
      type: "success",
      name: "Active",
    },
    {
      code: "closed",
      type: "info",
      name: "Closed",
    }
  ],
  erasmusApplicationSetState: [
    {
      code: "active",
      type: "active",
      name: "Active",
    },
    {
      code: "closed",
      type: "final",
      name: "Closed",
    }
  ],
  universityApprovalState: [
    {
      code: "initial",
      type: "blue",
      name: "Initial",
    },
    {
      code: "alternative-final",
      type: "pink-rich",
      name: "In process",
    },
    {
      code: "final",
      type: "orange",
      name: "Approved",
    },
    {
      code: "canceled",
      type: "grey-rich",
      name: "Denied",
    }
  ],

  TAG,
  Css: UU5.Common.Css.createCssModule(
    TAG.replace(/\.$/, "")
      .toLowerCase()
      .replace(/\./g, "-")
      .replace(/[^a-z-]/g, ""),
    process.env.NAME + "/" + process.env.OUTPUT_NAME + "@" + process.env.VERSION // this helps preserve proper order of styles among loaded libraries
  ),
};
