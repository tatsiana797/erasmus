const Lsi = {
  remove: {
    en: "Remove",
    cs: "Odebrat"
  },
  summer: {
    en: "Summer",
    cs: "Léto"
  },
  winter: {
    en: "Winter",
    cs: "Zimní"
  },
  noData: {
    en: "No applications are available",
    cs: "Nejsou k dispozici žádné žádosti",
  },
  table: {
    en: "Table",
    cs: " Tabulka"
  },
  updateApplicantInfo: {
    en: "Update information about applicant",
    cs: "Aktualizace informací o žadateli"
  },
  destination: {
    en: "Destination",
    cs: "Destinace"
  },
  addNote: {
    en: "Add a note",
    cs: "Přidat poznámku"
  },
  semester: {
    en: "Semester",
    cs: "Semestr"
  },
  approval: {
    en: "Approval",
    cs: "Schválení"
  },
  studyPrograme: {
    en: "Study programme",
    cs: "Studijní program"
  },
  prerecognition: {
    en: "Pre-recognition",
    cs: "Před uznání"
  },
  insurance: {
    en: "Insurance",
    cs: "Pojištění"
  },
  la: {
    en: "LA",
    cs: "US"
  },
  grant: {
    en: "Grant",
    cs: "Grant"
  },
  notes: {
    en: "Notes",
    cs: "Poznámky"
  },
  state: {
    en: "State",
    cs: "Stav"
  },
  setInterviewButton: {
    en: "Set interview",
    cs: "Přidat termín pohovoru"
  },
  exportButton: {
    en: "Export data",
    cs: "Exportovat data"
  },
  interviewDateHeader: {
    en: "Interview date",
    cs: "Termín pohovoru"
  },
  approvalStatusHeader: {
    en: "Approval status",
    cs: "Status schválení"
  },
  schoolHeader: {
    en: "School",
    cs: "Škola"
  },
  setState: {
    en: "Set state",
    cs: "Nastavit stav"
  },
  removeButton: {
    en: "Remove",
    cs: "Odstranit"
  },
  addButton: {
    en: "Add",
    cs: "Přidat"
  },
  updateButton: {
    en: "Update",
    cs: "Aktualizovat"
  },
  nameHeader: {
    en: "Full name",
    cs: "Jméno a příjmení"
  },
  submit: {
    en: "Submit",
    cs: "Odeslat"
  },
  removeAppl: {
    en: "Remove application",
    cs: "Odstranit aplikaci"
  },
  removeInfo: {
    en: "Important information about removal an applicant will be displayed here.",
    cs: "Zde se zobrazí důležité informace o odstranění žadatele."
  },
  deleteConfirm1: {
    en: "Are you sure you want to delete an application of ",
    cs: "Jste si jisti, že chcete odstranit aplikaci "
  },
  deleteConfirm2: {
    en: " Please note that all attached files will be removed!",
    cs: " Upozorňujeme, že všechny přiložené soubory budou smazány!"
  },
  applStateHeader: {
    en: "Set application state",
    cs: "Nastavení stavu aplikace"
  },
  infoSetApplState: {
    en: "Here will be displayed information about setting an application state.",
    cs: "Zde se zobrazí informace o nastavení stavu aplikace."
  },
  state: {
    en: "State",
    cs: "Stav"
  },
  setInterviewHeader: {
    en: "Set interview date",
    cs: "Stanovení termínu pohovoru"
  },
  setInterviewInfo: {
    en: "Here will be displayed information about setting interview date.",
    cs: "Zde se zobrazí informace o nastavení data pohovoru."
  },
  interviewDate: {
    en: "Add date",
    cs: "Přidat datum"
  },
  addCommonNote: {
    en: "Add shared information",
    cs: "Přidat sdílené informace"
  },
  sharedNoteInfo: {
    en: "Here will be displayed information about adding shared notes.",
    cs: "Zde se zobrazí informace o přidávání sdílených poznámek."
  },
  sharedNoteHeader: {
    en: "This information will be displayed for each applicant in this semester.",
    cs: "Tyto informace se zobrazí u každého žadatele v tomto semestru."
  }
};

export default Lsi;
