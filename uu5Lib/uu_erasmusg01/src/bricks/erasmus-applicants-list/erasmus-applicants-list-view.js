//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponent, useState, useEffect } from "uu5g04-hooks";
import Config from "./config/config";
import ErasmusApplicantsTable from "./erasmus-applicants-table"
import DataObjectStateResolver from "../common/resolver/data-object-state-resolver";
import { useCommonData } from "../common/context/common-data/use-common-data";
import ErasmusApplicantsUpdateForm from "./erasmus-applicants-update-form";
import SetInterviewDateForm from "./set-interview-date-form";
import AddCommonNote from "./add-common-note";
import Uu5Tiles from "uu5tilesg02";
import SetErasmusApplicationState from "./set-erasmus-application-state";
import RemoveApplication from "./remove-application";
import "uu5g04-forms";
import "uu5g04-bricks";
import useApplicationList from "../common/context/applicaton-list/use-application-list";
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "ErasmusApplicantsListView"
  //@@viewOff:statics
};

export const ErasmusApplicantsListView = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined
  },
  //@@viewOff:defaultProps

  //@@viewOn:render
  render(props) {
    //@@viewOn:hooks
    const commonDataList = useCommonData();
    const commonList = commonDataList?.data?.itemList;
    const applicationDataList = useApplicationList();
    let applicationList = applicationDataList?.data
    //.sort((a, b) => parseFloat(b._id.split(" ")[1]) - (parseFloat(a._id.split(" ")[1])))
    const [showAdd, setShowAddModal] = useState(false);
    const [showOpenSetInterview, setShowOpenSetInterview] = useState(false)
    const [showOpenSetState, setShowOpenSetState] = useState(false)
    const [studentData, setStudentData] = useState();
    const [showOpenAddCommonNote, setShowOpenAddCommonNote] = useState(false);
    const [showOpenRemoveApplication, setShowRemoveApplication] = useState(false);
    const [notesForSemester, setNotesForSemester] = useState();
    const [filterValueName, setFilterValueName] = useState();
    const [filterValueSemestr, setFilterValueSemester] = useState();
    const [filterValueYear, setFilterValueYear] = useState();
    const [erasmusStateCode, setErasmusState] = useState();
    const [commonNote, setCommonNote] = useState(undefined);
    useEffect(() => {
      const notes = commonList?.filter(item => item.semesterId === notesForSemester)[0]?.commonNotes
      notes === undefined ? setCommonNote(" ") : setCommonNote(notes)
    }, [commonList, applicationList])
    //@@viewOff:hooks
    if (filterValueSemestr) {
      applicationList = applicationList?.map(item => {
        return item?._id?.toLowerCase().replace(/\s+/g, '').includes(filterValueSemestr?.toLowerCase().trim().replace(/\s+/g, '')) ? item : null
      }).filter(item => item !== null)
    }
    if (filterValueYear) {
      applicationList = applicationList?.map(item => {
        return item._id?.toLowerCase().replace(/\s+/g, '').includes(filterValueYear?.toLowerCase().trim().replace(/\s+/g, '')) ? item : null
      }).filter(item => item !== null)
    }
    if (erasmusStateCode) {
      applicationList = applicationList?.map(item => {
        const list = item.itemList?.filter(el => {
          return el?.erasmus?.erasmusState?.toLowerCase().replace(/\s+/g, '').includes(erasmusStateCode?.toLowerCase().trim().replace(/\s+/g, ''))
        });
        return list.length !== 0 ? item : null
      }).filter(item => item !== null)
    }
    if (filterValueName) {
      applicationList = applicationList?.map(item => {
        const list = item.itemList?.filter(el => {
          return el?.firstName?.toLowerCase().replace(/\s+/g, '').includes(filterValueName?.toLowerCase().trim().replace(/\s+/g, '')) || el?.lastName?.toLowerCase().replace(/\s+/g, '').includes(filterValueName.toLowerCase().trim().replace(/\s+/g, ''));
        });
        return list.length !== 0 ? item : null
      }).filter(item => item !== null)
    }

    function handleOpenUpdateForm(application) {
      setShowAddModal(true)
      setStudentData(application)
    }

    function handleCloseUpdateForm() {
      setShowAddModal(false)
    }

    function handleOpenAddCommonNoteForm(studentsInSemester) {
      setNotesForSemester(studentsInSemester?._id)
      setShowOpenAddCommonNote(true)
      listReloadCommonData()
    }
    function handleCloseAddCommonNoteForm() {
      setCommonNote(undefined)
      setShowOpenAddCommonNote(false)
    }

    function handleOpenSetInterviewDateForm(studentsInSemester) {
      setNotesForSemester(studentsInSemester?._id)
      setShowOpenSetInterview(true)
      listReloadCommonData()
    }
    function handleCloseSetInterviewDateForm() {
      setShowOpenSetInterview(false)
    }

    function handleOpenSetErasmusState(student) {
      setStudentData(student)
      setShowOpenSetState(true)
    }

    function handleCloseSetErasmusStateForm() {
      setShowOpenSetState(false)

    }

    async function handleSaveSetInterviewDate(opt) {
      let semesterId = commonList?.filter(item => item?.semesterId === notesForSemester)[0]?.id
      const interviewDates = Object.keys(opt?.values).map((dateTime) => opt?.values[dateTime]).filter(item => item !== null)

      if (!semesterId) {
        let newData = { semesterId: notesForSemester, interviewDates: [...interviewDates], }
        try {
          await commonDataList?.handlerMap.createCommonData(newData);
          opt.component.reset();
          handleCloseSetInterviewDateForm();
        } catch (e) {
          console.log(e);
          return;
        }
      }
      if (semesterId) {
        let newData = {
          id: commonList?.filter(item => item?.semesterId === notesForSemester)[0]?.id,
          interviewDates: [...interviewDates]
        }
        try {
          await commonDataList?.handlerMap?.updateCommonData(newData);
          opt.component.reset();
          handleCloseSetInterviewDateForm();
        } catch (e) {
          console.log(e);
          return;
        }
      }
    }


    async function handleSaveRemoveApplication(opt) {
      const codeArr = []
      codeArr.push(studentData?.erasmus?.attachments.cv)
      codeArr.push(studentData?.erasmus?.attachments.ml)
      codeArr.push(...studentData?.erasmus?.attachments.syllabus.map(item => item.code))
      const newData = {
        studentId: studentData?.studentId,
        code: codeArr,
        list: true
      }
      try {
        await applicationDataList.handlerMap.removeAttachments(newData);
      } catch (e) {
        opt.component.saveFail(e);
        return;
      }
      opt.component.saveDone();
    }

    async function listReloadCommonData() {
      try {
        const response = await commonDataList.handlerMap.load({ dtoIn: props.baseUri });
        return response
      } catch (e) {
        console.log(e);
        return;
      }
    }

    function handleRemoveDone({ component }) {
      clearForm(component);
      setShowRemoveApplication(false);
    }

    function handleRemoveFail({ component, dtoOut: e }) {
      console.error(e);
      component.getAlertBus().addAlert({
        content: <Error errorData={e} />,
        colorSchema: "danger",
      });
    }


    function handleOpenRemoveApplicationForm(application) {
      setStudentData(application)
      setShowRemoveApplication(true)
    }

    function handleCloseRemoveApplication() {
      setShowRemoveApplication(false)
    }

    async function handleSaveAddCommonNote(opt) {
      let semesterId = commonList?.filter(item => item?.semesterId === notesForSemester)[0].id

      if (!semesterId) {
        let newData = { semesterId: notesForSemester, commonNotes: opt.values.commonNote, }
        try {
          await commonDataList.handlerMap.createCommonData(newData);
          handleCloseAddCommonNoteForm();
        } catch (e) {
          console.log(e);
          return;
        }
      }

      if (semesterId) {
        let newData = {
          id: commonList?.filter(item => item.semesterId === notesForSemester)[0]?.id,
          interviewDates: commonList?.filter(item => item.semesterId === notesForSemester)[0]?.interviewDates,
          commonNotes: opt.values.commonNote,
        }
        try {
          await commonDataList.handlerMap.updateCommonData(newData);
          handleCloseAddCommonNoteForm();
        } catch (e) {
          console.log(e);
          return;
        }
      }
    }

    async function handleSaveSetErasmusState(opt) {
      const studentErasmus = {
        personalInfo: studentData?.erasmus?.personalInfo,
        interviewDate: studentData?.erasmus?.interviewDate,
        requestForPrerecogn: studentData?.erasmus?.requestForPrerecogn,
        insurance: studentData?.erasmus?.insurance,
        learningAgreement: studentData?.erasmus?.learningAgreement,
        grant: studentData?.erasmus?.grant,
        currentSemester: studentData?.erasmus?.currentSemester,
        motivationDescription: studentData?.erasmus?.motivationDescription,
        attachments: studentData?.erasmus?.attachments,
        semesterYear: studentData?.erasmus?.semesterYear,
        destionations: studentData?.erasmus?.destionations,
        erasmusState: opt.values.state,
        language: studentData?.erasmus?.language || "",
        personalNote: studentData?.erasmus?.personalNote || "",
        noteForCoordinator: studentData?.erasmus?.noteForCoordinator,
        studyProgramme: studentData?.erasmus?.studyProgramme,
      }
      const newData = {
        studentId: studentData?.studentId,
        id: studentData?.applicationId,
        firstName: studentData?.firstName,
        lastName: studentData?.lastName,
        uuIdentity: studentData?.uuIdentity,
        erasmus: studentErasmus
      }
      try {
        await applicationDataList.handlerMap.update(newData);
      } catch (e) {
        opt.component.saveFail(e);
        return;
      }
      opt.component.saveDone();
    }

    function handleSetStateDone({ component }) {
      clearForm(component);
      setShowOpenSetState(false);
    }

    function handleSetStateFail({ component, dtoOut: e }) {
      console.error(e);
      component.getAlertBus().addAlert({
        content: <Error errorData={e} />,
        colorSchema: "danger",
      });
    }

    async function handleSaveApplicantsUpdateForm(opt) {
      let approvalStatesCode = (stateName) => {
        let status = Config.universityApprovalState.find(item => {
          return item.name === stateName
        })
        return status?.code
      }
      const studentErasmus = {
        studyProgramme: studentData?.erasmus?.studyProgramme,
        language: studentData?.erasmus?.language,
        personalInfo: studentData?.erasmus?.personalInfo,
        interviewDate: studentData?.erasmus?.interviewDate,
        requestForPrerecogn: opt.values.requestForPrerecogn.toString(),
        insurance: opt.values.insurance.toString(),
        learningAgreement: opt.values.learningAgreement.toString(),
        grant: opt.values.grant.toString(),
        currentSemester: studentData?.erasmus?.currentSemester,
        motivationDescription: studentData?.erasmus?.motivationDescription,
        attachments: studentData?.erasmus?.attachments,
        semesterYear: studentData?.erasmus?.semesterYear,
        erasmusState: studentData?.erasmus?.erasmusState || "active",
        personalNote: opt.values.personalNote,
        noteForCoordinator: opt.values.noteForCoordinator,
        subjectsRecognition: studentData?.erasmus?.subjectsRecognition,
        destionations: studentData?.erasmus?.destionations.map((item, i) => {
          const approvalStatus = `universityApprovalStatus${i + 1}`
          return (
            {
              order: i.toString(),
              school: studentData?.erasmus?.destionations[i].school,
              from: studentData?.erasmus?.destionations[i].from,
              to: studentData?.erasmus?.destionations[i].to,
              numberOfCreadits: studentData?.erasmus?.destionations[i].numberOfCreadits,
              universityApprovalStatus: approvalStatesCode(opt.values[approvalStatus]),
            }
          )
        })
      }

      const newData = {
        studentId: studentData?.studentId,
        id: studentData?.applicationId,
        firstName: studentData?.firstName,
        lastName: studentData?.lastName,
        uuIdentity: studentData?.uuIdentity,
        erasmus: studentErasmus
      }
      try {
        await applicationDataList.handlerMap.update(newData);
      } catch (e) {
        opt.component.saveFail(e);
        return;
      }
      opt.component.reset()
      opt.component.saveDone();
    }

    function handleUpdateDone({ component }) {
      clearForm(component);
      setShowAddModal(false);
    }

    function handleUpdateFail({ component, dtoOut: e }) {
      console.error(e);
      component.getAlertBus().addAlert({
        content: <Error errorData={e} />,
        colorSchema: "danger",
      });
    }

    function clearForm(form) {
      if (form.reset) form.reset();
      if (form.getAlertBus) form.getAlertBus().clearAlerts();
    }

    const FILTERS = [
      {
        key: "fullName",
        label: { cs: "Jméno a příjmení", en: "Full name" },
        filterFn: (item) => {
          let itemValue =
            typeof item?.fullName === "object"
              ? UU5.Common.Tools.getLsiItemByLanguage(item?.fullName)
              : item?.fullName;
          return setFilterValueName(itemValue)
        },
        colorSchema: props.colorSchema
      },
      {
        key: "year",
        label: { cs: "Rok", en: "Year" },
        removeFilterFn: (key) => {

        },
        component: (
          <UU5.Forms.DatePicker colorSchema={props.colorSchema} style={{
            marginLeft: "2rem",
            marginTop: "auto"
          }}
            key="year"
            name="year"
            step="years"
          />
        ),

        filterFn: (item) => {
          let itemValue =
            typeof item?.year === "object"
              ? UU5.Common.Tools.getLsiItemByLanguage(item?.year)
              : item?.year;
          return setFilterValueYear(itemValue)
        },
        colorSchema: props.colorSchema
      },
      {
        key: "semester",
        label: { cs: "Semestr", en: "Semester" },
        component: (
          <UU5.Forms.Select
            name="semester"
            key="semester"
            colorSchema={props.colorSchema}
          >
            <UU5.Forms.Select.Option content={<UU5.Bricks.Lsi lsi={{ cs: "Zima", en: "Winter" }} />} value="Winter" />
            <UU5.Forms.Select.Option content={<UU5.Bricks.Lsi lsi={{ cs: "Léto", en: "Summer" }} />} value="Summer" />
          </UU5.Forms.Select>
        ),

        filterFn: (item, value) => {
          let itemValue =
            typeof item?.semester === "object"
              ? UU5.Common.Tools.getLsiItemByLanguage(item?.semester)
              : item?.semester;
          return setFilterValueSemester(itemValue)
        },
        colorSchema: props.colorSchema
      },
      {
        key: "erasmusState",
        label: { en: "Application state", cs: "Stav žádosti", },
        component: (
          <UU5.Forms.Select
            label={<UU5.Bricks.Lsi lsi={{ en: "Application state", cs: "Stav žádosti", }} />}
            name="erasmusState"
            key="erasmusState"
            colorSchema={props.colorSchema}
          >
            <UU5.Forms.Select.Option content={<UU5.Bricks.Lsi lsi={{ cs: "Aktivní", en: "Active" }} />} value='active' />
            <UU5.Forms.Select.Option content={<UU5.Bricks.Lsi lsi={{ cs: "Uzavřený", en: "Closed" }} />} value='closed' />
          </UU5.Forms.Select>
        ),
        filterFn: (item) => {
          let itemValue =
            typeof item?.erasmusState === "object"
              ? UU5.Common.Tools.getLsiItemByLanguage(item?.erasmusState)
              : item?.erasmusState;
          return setErasmusState(itemValue)
        },
        colorSchema: props.colorSchema
      },
    ];

    function handleChangeFiltersSorters(val) {
      if (val?.activeFilters[0]?.key === 'semester') {
        setFilterValueSemester(val?.activeFilters[0]?.value)
      }
      else if (val?.activeFilters[1]?.key === 'semester') {
        setFilterValueSemester(val?.activeFilters[1]?.value)
      }
      else {
        setFilterValueSemester("")
      }
      if (val?.activeFilters[0]?.key === 'year') {
        setFilterValueYear(val?.activeFilters[0]?.value)
      }
      else if (val?.activeFilters[1]?.key === 'year') {
        setFilterValueYear(val?.activeFilters[1]?.value)
      }
      else {
        setFilterValueYear("")
      }

      if (val?.activeFilters[0]?.key === 'fullName') {
        setFilterValueName(val?.activeFilters[0]?.value);
      }
      else if (val?.activeFilters[1]?.key === 'fullName') {
        setFilterValueName(val?.activeFilters[1]?.value);
      } else {
        setFilterValueName("")
      }

      if (val?.activeFilters[0]?.key === 'erasmusState') {
        setErasmusState(val?.activeFilters[0]?.value);
      }
      else if (val?.activeFilters[1]?.key === 'erasmusState') {
        setErasmusState(val?.activeFilters[1]?.value);
      } else {
        setErasmusState("")
      }
    }

    function handleExportData(student) {
      document.getElementById(`${student?._id}`).click();
    }

    return (
      <>
        <DataObjectStateResolver dataObject={commonDataList} >
          <DataObjectStateResolver dataObject={applicationDataList} >
            <ErasmusApplicantsUpdateForm
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
              baseUri={props.baseUri}
              studentData={studentData}
              onSaveDone={handleUpdateDone}
              onSaveFail={handleUpdateFail}
              shown={showAdd}
              onCancel={handleCloseUpdateForm}
              onSave={handleSaveApplicantsUpdateForm}
            />
            <AddCommonNote
              noteValue={commonNote}
              shown={showOpenAddCommonNote}
              onCancel={handleCloseAddCommonNoteForm}
              onSave={handleSaveAddCommonNote}
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
            />
            <SetInterviewDateForm
              notesForSemester={notesForSemester}
              commonList={commonList}
              shown={showOpenSetInterview}
              onCancel={handleCloseSetInterviewDateForm}
              onSave={handleSaveSetInterviewDate}
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
            />
            <SetErasmusApplicationState
              onSaveDone={handleSetStateDone}
              onSaveFail={handleSetStateFail}
              shown={showOpenSetState}
              onSave={handleSaveSetErasmusState}
              onCancel={handleCloseSetErasmusStateForm}
              studentData={studentData}
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
            />
            <RemoveApplication
              onSaveDone={handleRemoveDone}
              onSaveFail={handleRemoveFail}
              shown={showOpenRemoveApplication}
              onSave={handleSaveRemoveApplication}
              onCancel={handleCloseRemoveApplication}
              studentData={studentData}
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
            />
            <Uu5Tiles.ControllerProvider
              onChangeFilters={handleChangeFiltersSorters}
              filters={FILTERS}
              removeFilter={false}
            >
              <Uu5Tiles.FilterBar style={{ marginBottom: '1rem' }} />
            </Uu5Tiles.ControllerProvider>

            {applicationList?.map((studentsInSemester, index) => {
              return (<UU5.Bricks.Div key={index} style={{ marginBottom: "3rem", width: "initial !important" }}>
                <ErasmusApplicantsTable
                  dataSet={studentsInSemester?.itemList}
                  colorSchema={props.colorSchema}
                  borderRadius={props.borderRadius}
                  elevation={props.elevation}
                  semester={studentsInSemester._id}
                  bgStyle={props.bgStyle}
                  onOpenUpdateForm={handleOpenUpdateForm}
                  onOpenRemoveForm={handleOpenRemoveApplicationForm}
                  onOpenSetErasmusStateForm={handleOpenSetErasmusState}
                  onOpenSetInterviewForm={() => handleOpenSetInterviewDateForm(studentsInSemester)}
                  onOpenAddCommonNoteForm={() => handleOpenAddCommonNoteForm(studentsInSemester)}
                  onClickExportData={() => handleExportData(studentsInSemester)}
                /></UU5.Bricks.Div>)
            })}
          </DataObjectStateResolver>
        </DataObjectStateResolver>
      </>
    )
  }
  //@@viewOff:render
})


//viewOn:exports
export default ErasmusApplicantsListView;
//viewOff:exports
