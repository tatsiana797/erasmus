import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "../config/config";
import ErasmusApplicantsListView from "./erasmus-applicants-list-view";
import CommonDataLoader from "../common/loader/common-data-loader";
import Css from './erasmus-list-css'
import ApplicationListLoader from "../common/loader/application-list-loader"
//@@viewOff:imports

const STATICS = {
    //@@viewOn:statics
    displayName: Config.TAG + "ErasmusApplicantsList"
    //@@viewOff:statics
};
export const ErasmusApplicantsList = createVisualComponent({
    ...STATICS,
    //@@viewOn:propTypes
    propTypes: {
        baseUri: UU5.PropTypes.string,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        baseUri: undefined,
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {
        const errorsToWarn = ["Warning:"]
        const oldConsError = console.error;
        console.error = function (...args) {
            let toWarn = false;
            if (typeof args[0] === 'string') {
                errorsToWarn.map(function (_s) {
                    if (args[0].startsWith(_s)) {
                        toWarn = true;
                    }
                })
            }
            toWarn ? console.warn(...args) : oldConsError(...args);
        }
        return (
            <ApplicationListLoader baseUri={props.baseUri}>
                <CommonDataLoader baseUri={props.baseUri}>
                    <UU5.Bricks.Div style={{ display: "block", padding: "5px" }} className={Css.ZoomMain()} >
                        <ErasmusApplicantsListView
                            baseUri={props.baseUri}
                            colorSchema={props.colorSchema}
                            borderRadius={props.borderRadius}
                            elevation={props.elevation}
                            bgStyle={props.bgStyle}
                        />
                    </UU5.Bricks.Div>
                </CommonDataLoader>
            </ApplicationListLoader>
        )
    }
    //@@viewOff:render
})
//viewOff:helpers
//viewOn:exports
export default ErasmusApplicantsList;
//viewOff:exports
