//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useScreenSize } from "uu5g04-hooks";
import Uu5Tiles from "uu5tilesg02";
import "uu5g04-forms";
import Config from "./config/config";
import Lsi from "./erasmus-applicants-list-lsi";
import "uu_pg01-bricks";
import ReactExport from "react-data-export";
//@@viewOff:imports
const STATICS = {
  displayName: Config.TAG + "ErasmusApplicantsTable",

};

export const ErasmusApplicantsTable = createVisualComponent({
  //@@viewOn:statics
  ...STATICS,

  //@@viewOff:statics

  //@@viewOn:propTypes
  propTypes: {
    applicationList: UU5.PropTypes.array,
    dataSet: UU5.PropTypes.array,
    onClickExportData: UU5.PropTypes.func,
    onOpenUpdateForm: UU5.PropTypes.func,
    onOpenRemoveForm: UU5.PropTypes.func,
    onOpenSetErasmusStateForm: UU5.PropTypes.func,
    onOpenSetInterviewForm: UU5.PropTypes.func,
    onOpenAddCommonNoteForm: UU5.PropTypes.func,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    applicationList: null,
    dataSet: null,
    onClickExportData: () => { },
    onOpenUpdateForm: () => { },
    onOpenRemoveForm: () => { },
    onOpenSetErasmusStateForm: () => { },
    onOpenSetInterviewForm: () => { },
    onOpenAddCommonNoteForm: () => { },
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined
  },
  //@@viewOff:defaultProps
  render(props) {
    const screenSize = useScreenSize();
    const visibleControls = true
    let data = props?.dataSet
    data === null && (data = []);
    const ExcelFile = ReactExport.ExcelFile;
    const ExcelSheet = ReactExport.ExcelFile.ExcelSheet;
    const ExcelColumn = ReactExport.ExcelFile.ExcelColumn;
    const className = Config.Css.css``;
    const attrs = UU5.Common.VisualComponent.getAttrs(props, className);
    const GET_ACTIONS = ({ screenSize }) => {
      let actionList = [];
      if (visibleControls) {
        actionList.push(
          {
            content: Lsi.addNote,
            onClick: props.onOpenAddCommonNoteForm,
            bgStyle: props.bgStyle,
            colorSchema: props.colorSchema,
            active: true,
            borderRadius: props.borderRadius,
            elevation: props.elevation

          },
          {
            content: Lsi.setInterviewButton,
            onClick: props.onOpenSetInterviewForm,
            colorSchema: props.colorSchema,
            bgStyle: props.bgStyle,
            active: true,
            borderRadius: props.borderRadius,
            elevation: props.elevation
          },
          {
            content: Lsi.exportButton,
            onClick: props.onClickExportData,
            colorSchema: props.colorSchema,
            bgStyle: props.bgStyle,
            active: true,
            borderRadius: props.borderRadius,
            elevation: props.elevation

          }
        )
      } else {
        return null;
      }
      return actionList;
    }
    let columns = [
      {
        cell: cellProps => cellProps.data.firstName + " " + cellProps.data.lastName,
        header: <div style={{ color: "black", fontWeight: 500 }} > <UU5.Bricks.Lsi lsi={Lsi.nameHeader} /></div>,
      },
      {
        cell: cellProps => cellProps.data.erasmus.currentSemester,
        header: <div style={{ color: "black", fontWeight: 500 }}><UU5.Bricks.Lsi lsi={Lsi.semester} /></div>
      },
      {
        cell: cellProps => {
          let isNotAllDenied = cellProps?.data?.erasmus?.destionations?.some(item => {
            return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final' || item.universityApprovalStatus === 'initial'
          });
          let isNotInitial = cellProps?.data?.erasmus?.destionations.some(item => {
            return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
          });

          let statusType;
          if (isNotInitial) {
            statusType = cellProps.data.erasmus.destionations.find(item => {
              return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
            }).school
          } else if (!isNotAllDenied) {
            let reversed = cellProps?.data?.erasmus?.destionations.reverse()
            statusType = reversed.find(item => {
              return item.universityApprovalStatus === 'canceled'
            })?.school
          }
          else {
            statusType = cellProps?.data?.erasmus?.destionations?.find(item => {
              return item.universityApprovalStatus === 'initial'
            })?.school
          }
          return statusType || "";
        },
        header: <div style={{ color: "black", fontWeight: 500 }}><UU5.Bricks.Lsi lsi={Lsi.destination} /></div>
      },
      {
        cell: cellProps => {
          let isNotAllDenied = cellProps?.data?.erasmus?.destionations.some(item => {
            return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final' || item.universityApprovalStatus === 'initial'
          });
          let isNotInitial = cellProps?.data?.erasmus.destionations.some(item => {
            return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
          });
          let state = Config.universityApprovalState.find(item => {
            let statusType;
            if (isNotInitial) {
              statusType = item.code === cellProps?.data?.erasmus.destionations.find(item => {
                return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
              })?.universityApprovalStatus
            } else if (!isNotAllDenied) {
              let reversed = cellProps?.data?.erasmus.destionations.reverse()
              statusType = item.code === reversed.find(item => {
                return item.universityApprovalStatus === 'canceled'
              })?.universityApprovalStatus
            }
            else {
              statusType = item.code === cellProps?.data?.erasmus?.destionations.find(item => {
                return item.universityApprovalStatus === 'initial'
              })?.universityApprovalStatus
            }
            return statusType || ""
          }
          )
          return (
            <UU5.Bricks.Label bgStyle={'outline'} colorSchema={state.type}>{state.name}</UU5.Bricks.Label>

          )
        },
        header: <div style={{ color: "black", fontWeight: 500 }}><UU5.Bricks.Lsi lsi={Lsi.approval} /></div>,
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.studyProgramme,
        header: <div style={{ color: "black", fontWeight: 500 }}><UU5.Bricks.Lsi lsi={Lsi.studyPrograme} /></div>
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.interviewDate || "",
        header: <div style={{ color: "black", fontWeight: 500 }}><UU5.Bricks.Lsi lsi={Lsi.interviewDateHeader} /></div>
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.requestForPrerecogn === "false" ?
          <UU5.Bricks.Icon icon="mdi-minus" style={{ color: "black" }} /> : <UU5.Bricks.Icon icon="mdi-check" />,
        header: <div style={{ color: "black", fontWeight: 500 }} > <UU5.Bricks.Lsi lsi={Lsi.prerecognition} /></div>
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.insurance === "false" ?
          <UU5.Bricks.Icon icon="mdi-minus" style={{ color: "black" }} /> : <UU5.Bricks.Icon icon="mdi-check" />,
        header: <div style={{ color: "black", fontWeight: 500 }} ><UU5.Bricks.Lsi lsi={Lsi.insurance} /></div>
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.learningAgreement === "false" ?
          <UU5.Bricks.Icon icon="mdi-minus" style={{ color: "black" }} /> : <UU5.Bricks.Icon icon="mdi-check" />,
        header: <div style={{ color: "black", fontWeight: 500 }} ><UU5.Bricks.Lsi lsi={Lsi.la} /></div>
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.grant === "false" ?
          <UU5.Bricks.Icon icon="mdi-minus" style={{ color: "black" }} /> : <UU5.Bricks.Icon icon="mdi-check" />,
        header: <div style={{ color: "black", fontWeight: 500 }} ><UU5.Bricks.Lsi lsi={Lsi.grant} /></div>
      },
      {
        cell: cellProps => cellProps?.data?.erasmus?.noteForCoordinator ?
          <UU5.Bricks.Icon icon="mdi-comment-outline" style={{ color: "black" }} /> : "",
        header: <div style={{ color: "black", fontWeight: 500 }} ><UU5.Bricks.Lsi lsi={Lsi.notes} /></div>
      },
      {
        cell: cellProps => {
          let state = Config.erasmusApplicationState.filter(item => {
            return item.code === cellProps?.data?.erasmus?.erasmusState
          })
          return (
            <UU5.Bricks.Label bgStyle="outline" colorSchema={state[0]?.type}> {state[0]?.name}</UU5.Bricks.Label>
          )
        },
        header: <div style={{ color: "black", fontWeight: 500 }} ><UU5.Bricks.Lsi lsi={Lsi.state} /></div>
      },
      {
        cell: cellProps => {
          return cellProps?.data?.erasmus?.erasmusState === "closed" ? (
            <UU5.Bricks.Dropdown
              label={<UU5.Bricks.Icon icon="mdi-dots-vertical" />}
              bgStyle="outline"
              popoverLocation="local"
              iconHidden
            >
              <UU5.Bricks.Dropdown.Item
                label={<UU5.Bricks.Lsi lsi={Lsi.remove} />}
                onClick={() => props.onOpenRemoveForm(cellProps?.data)}
              />
              <UU5.Bricks.Dropdown.Item
                label={<UU5.Bricks.Lsi lsi={Lsi.setState} />}
                onClick={() => props.onOpenSetErasmusStateForm(cellProps?.data)}
              /></UU5.Bricks.Dropdown>
          ) :
            (<UU5.Bricks.Div>
              <UU5.Bricks.Dropdown
                label={<UU5.Bricks.Icon icon="mdi-dots-vertical" />}
                bgStyle="outline"
                popoverLocation="local"
                iconHidden
              ><UU5.Bricks.Dropdown.Item
                  label={<UU5.Bricks.Lsi lsi={Lsi.updateButton} />}
                  onClick={() => props.onOpenUpdateForm(cellProps?.data)}
                />
                <UU5.Bricks.Dropdown.Item
                  label={<UU5.Bricks.Lsi lsi={Lsi.setState} />}
                  onClick={() => props.onOpenSetErasmusStateForm(cellProps?.data)}
                />
              </UU5.Bricks.Dropdown>
            </UU5.Bricks.Div>
            )
        },
        header: "",
        textAlignment: "right",
      }

    ];

    let child;

    if (data && data.length && columns) {
      child = (
        <UU5.Bricks.Div >
          <Uu5Tiles.List
            style={{ paddingBottom: "3.5rem" }}
            columns={columns}
            height="auto"
            rowPadding="4px 16px"
            tileRowSpacing={8}
            tileListPadding="8px 16px"
            alternateRowBackground
          />
        </UU5.Bricks.Div>
      )
    } else {
      child = <UU5.Bricks.Div content={<UU5.Bricks.Lsi lsi={Lsi.noData} />} />
    }
    const exportData = props.dataSet.map(item => {
      const erasmus = item?.erasmus
      const destination = (number) => {
        const school = erasmus?.destionations[number]?.school !== undefined ? erasmus.destionations[number]?.school : null
        const approval = erasmus?.destionations[number]?.universityApprovalStatus === "alternative-final" ? "In process" : "final" ? "Approved" : "Denied"
        if (school === null) {
          return ""
        } else {
          return school + " /" + approval
        }
      }
      return {
        "fullName": item.firstName + " " + item.lastName,
        "currentSemester": erasmus?.currentSemester,
        "destination1": destination(0),
        "destination2": destination(1),
        "destination3": destination(2),
        "studyProgram": erasmus?.studyProgramme,
        "interviewDate": erasmus?.interviewDate,
        "requestForPrerecogn": erasmus?.requestForPrerecogn,
        "insurance": erasmus?.insurance,
        "learningAgreement": erasmus?.learningAgreement,
        "grant": erasmus?.grant,
        "erasmusState": props?.semester
      }
    })
    return (
      <>
        <ExcelFile element={<button id={props?.semester} style={{ display: "none" }} />}>
          <ExcelSheet data={exportData} name="Employees">
            <ExcelColumn label="Full name" value="fullName" />
            <ExcelColumn label="Current semester" value="currentSemester" />
            <ExcelColumn label="Destination 1/Approval" value="destination1" />
            <ExcelColumn label="Destination 2/Approval" value="destination2" />
            <ExcelColumn label="Destination 3/Approval" value="destination3" />
            <ExcelColumn label="Study program" value="studyProgram" />
            <ExcelColumn label="Interview date" value="interviewDate" />
            <ExcelColumn label="Prerecognition request" value="requestForPrerecogn" />
            <ExcelColumn label="Insurance" value="insurance" />
            <ExcelColumn label="Learning agreement" value="learningAgreement" />
            <ExcelColumn label="Grant" value="grant" />
            <ExcelColumn label="State" value="erasmusState" />
          </ExcelSheet>
        </ExcelFile>
        {data.length !== 0 && (
          <Uu5Tiles.Controller
            {...attrs}
            data={data}
            selectable={!["xs", "2rem"].includes(screenSize)}
            itemKey={(item) => (item ? item.code : undefined)}
          >
            <Uu5Tiles.ActionBar
              searchable={false} title={<UU5.Bricks.Div style={{ fontWeight: 500 }}>{props?.semester}</UU5.Bricks.Div>} actions={GET_ACTIONS} >
            </Uu5Tiles.ActionBar>
            {child}
          </Uu5Tiles.Controller>
        )}
      </>
    )
  }
});

export default ErasmusApplicantsTable;
