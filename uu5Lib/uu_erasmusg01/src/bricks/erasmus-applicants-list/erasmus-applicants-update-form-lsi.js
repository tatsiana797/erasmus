const Lsi = {
    erasmusInfo: {
        en: 'Erasmus info',
        cs: "Erasmus info"
    },
    subjects: {
        en: "Subjects",
        cs: "Předměty"
    },
    files: {
        en: "Attachments",
        cs: "Přílohy"
    },
    notes: {
        en: "Notes",
        cs: "Poznámky"
    },
    info: {
        en: "Here is some information related to Erasmus update form",
        cs: "Zde jsou informace týkající se aktualizačního formuláře programu Erasmus"
    },
    updateApplicantInfo: {
        en: "Update Erasmus application form",
        cs: "Aktualizace formuláře žádosti o Erasmus"
    },
    table: {
        en: "Table",
        cs: "Tabulka"
    },
    universityName: {
        en: "University name",
        cs: "Název univerzity"
    },
    approvalStatus: {
        en: "Approval status",
        cs: "Stav schválení"
    },
    prerocognRequest: {
        en: "Precognition request",
        cs: "Žádost o předvídání předmětu"
    },
    insurance: {
        en: "Insurance",
        cs: "Pojištění"
    },
    la: {
        en: "Learning agreement",
        cs: "Učební dohoda"
    },
    grant: {
        en: "Grant",
        cs: "Grant"
    },
    semester: {
        en: "Current semester",
        cs: "Stávající semestr"
    },
    mobSemester: {
        en: "Mobility semester",
        cs: "Semestr mobility"
    },
    mobYear: {
        en: "Mobility year",
        cs: "Rok mobility"
    },
    specialization: {
        en: "Study programme",
        cs: "Studijní program"
    },
    date: {
        en: "approximate date",
        cs: "přibližné datum"
    },
    school: {
        en: "School",
        cs: "Škola"
    },
    from: {
        en: "From",
        cs: "Od"
    },
    to: {
        en: "To",
        cs: "Do"
    },
    creadits: {
        en: "Credits",
        cs: "Kredity"
    },
    motivation: {
        en: "What are your main motivations to go on exchange:",
        cs: "Jaké jsou vaše hlavní motivy pro účast v programu Erasmus:"
    },
    english: {
        en: "English",
        cs: "Anglický"
    },
    german: {
        en: "German",
        cs: "Německý"
    },
    french: {
        en: "French",
        cs: "Francouzský"
    },
    russian: {
        en: "Russian",
        cs: "Ruský"
    },
    personalInfo: {
        en: "Personal info",
        cs: "Osobní údaje"
    },
    dateOfBirth: {
        en: "Date of birth",
        cs: "Datum narození"
    },
    nationality: {
        en: "Nationality",
        cs: "Státní příslušnost"
    },
    email: {
        en: "Email",
        cs: "E-mail"
    },
    phone: {
        en: "Phone number",
        cs: "Telefonní číslo"
    },
    invalidPhone: {
        en: "Please provide us with valid phone number",
        cs: "Uveďte prosím platné telefonní číslo"
    },
    invalidEmail: {
        en: "Please provide to us valid  email address",
        cs: "Uveďte nám prosím platnou e-mailovou adresu"
    },
    adress: {
        en: "Adress",
        cs: "Adresa"
    },
    bankAccount: {
        en: "Bank account",
        cs: "Bankovní účet"
    },
    bankName: {
        en: "Bank name",
        cs: "Název banky"
    },
    swift: {
        en: "Clearing/BIC/SWIFT number",
        cs: "Zúčtovací číslo/BIC/SWIFT"
    },
    uuSubjectName: {
        en: "Name of the subject at Unicorn University",
        cs: "Název předmětu na Unicorn University"
    },
    partnerUniName: {
        en: "Name of the subject at Partner University",
        cs: "Název předmětu na partnerské univerzitě"
    },
    recognitionNotCreated: {
        en: "Student have not yet created request for subjects recognition.",
        cs: "Student zatím nevytvořil žádost o uznání předmětů."
    },
    resume: {
        en: "Resume: ",
        cs: "Životopis: "
    },
    ml: {
        en: "Motivation letter: ",
        cs: "Motivační dopis: "
    },
    syllables: {
        en: "Syllables: ",
        cs: "Sylaby: "
    },
    extraAttachments: {
        en: "Additional documents: ",
        cs: "Další dokumenty: "
    },
    comment: {
        en: "Here you can add a comment for the student",
        cs: "Zde můžete přidat komentář pro studenta"
    },
    personalNote: {
        en: "Here you can add a note for yourself regarding the student",
        cs: "Zde můžete přidat vlastní poznámku týkající se studenta."
    },
    submit: {
        en: "Submit",
        cs: "Uložit"
    },
    initial: { en: "Initial ", cs: "Úvodní" },
    inprocess: { en: "In process", cs: "Zpracovává se" },
    approved: { en: "Approved", cs: "Schváleno" },
    denied: { en: "Denied", cs: "Odmítnuto" },
    ECONOM_MANAGEMENT: {
        en: "Economics and Management",
        cs: "Ekonomika a management"
    },
    IT: {
        en: "Information Technology",
        cs: "Informační technologie"
    },
    ICT_PROJECT_MANAGEMENT: {
        en: "ICT Project Management",
        cs: "Řízení projektů ICT"
    }
};


export default Lsi;
