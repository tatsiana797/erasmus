//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef, useLsiValues, useState, useEffect } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./erasmus-applicants-update-form-lsi"
import Css from "./erasmus-list-css"
import "uu_plus4u5g01-bricks";
import "uu_contentkitg01"
//@@viewOff:imports

const ErasmusApplicantsUpdateForm = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "ErasmusApplicantsUpdateForm",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        baseUri: UU5.PropTypes.string,
        studentData: UU5.PropTypes.object,
        onSaveDone: UU5.PropTypes.func,
        onSaveFail: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        onCancel: UU5.PropTypes.func,
        onSave: UU5.PropTypes.func,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        baseUri: undefined,
        studentData: undefined,
        onSaveDone: () => { },
        onSaveFail: () => { },
        shown: false,
        onCancel: () => { },
        onSave: () => { },
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {
        const [note, setNote] = useState()
        useEffect(() => {
            setNote(props?.studentData?.erasmus.personalNote)
        })
        const inputLsi = useLsiValues(Lsi);
        return (
            <UU5.Forms.ContextModal
                shown={props.shown}
                header={<Header />}
                footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                onClose={props.onCancel}
                size="l"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    note={note}
                    baseUri={props.baseUri}
                    inputLsi={inputLsi}
                    onSave={props.onSave}
                    onSaveDone={props.onSaveDone}
                    onSaveFail={props.onSaveFail}
                    onCancel={props.onCancel}
                    studentData={props.studentData}
                    studentDataObject={props.studentDataObject}
                    onChangeStudentFiles={props.onChangeStudentFiles}
                    colorSchema={props.colorSchema}
                    elevation={props.elevation}
                    borderRadius={props.borderRadius}

                />
            </UU5.Forms.ContextModal>
        );
        //@@viewOff:render
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return (
        <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} controlled={false} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.submit} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
    );
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.updateApplicantInfo} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.info} />}
        />
    );
}


function Form({ onSave, onCancel, studentData, onSaveDone, onSaveFail, baseUri, inputLsi, colorSchema, borderRadius, elevation, note }) {
    let approvalStates = Config.universityApprovalState.map(item => item.name)
    let approvalStatesCode = (index) => {
        let status = Config.universityApprovalState.find(item => item.code === studentData?.erasmus?.destionations[index]?.universityApprovalStatus)
        return status
    }


    const other = studentData?.erasmus?.language.filter(item => item.name !== 'english'
        && item.name !== 'russian' && item.name !== 'german' && item.name !== 'french')
    const french = studentData?.erasmus?.language.filter(item => item.name == 'french')
    const german = studentData?.erasmus?.language.filter(item => item.name == 'german')
    const russian = studentData?.erasmus?.language.filter(item => item.name == 'russian')
    const english = studentData?.erasmus?.language.filter(item => item.name == 'english')

    const destionation1 = studentData?.erasmus?.destionations[0].school !== undefined
    const destionation2 = studentData?.erasmus?.destionations[1].school !== undefined
    const destionation3 = studentData?.erasmus?.destionations[2].school !== undefined

    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onSaveDone={onSaveDone}
            onSaveFail={onSaveFail}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >


            <UU5.Bricks.Tabs type="tabs" fade={true} justified colorSchema={colorSchema} elevation={elevation}>
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.table} />}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className={Css.UpdateWrapper()}>
                            <UU5.Bricks.Div id='block'>
                                <UU5.Bricks.Div className={Css.table()} >
                                    <UU5.Bricks.Div className='theader'>
                                        <UU5.Bricks.Div className='table_header' ><UU5.Bricks.Lsi lsi={Lsi.universityName} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_header' id="statusHeader"> <UU5.Bricks.Lsi lsi={Lsi.approvalStatus} /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    {studentData.erasmus.destionations[0]["school"] !== undefined &&
                                        studentData.erasmus.destionations[0]["school"] !== "not selected" &&
                                        studentData.erasmus.destionations[0].from !== null &&
                                        studentData.erasmus.destionations[0].to !== null && (
                                            <UU5.Bricks.Div className='table_row'>
                                                <UU5.Bricks.Div className='table_small'>
                                                    <UU5.Bricks.Div className='table_cell' ><UU5.Bricks.Lsi lsi={Lsi.universityName} /></UU5.Bricks.Div>
                                                    <UU5.Bricks.Div className='table_cell'>
                                                        {studentData.erasmus.destionations[0].school}
                                                    </UU5.Bricks.Div>
                                                </UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_small' id="status">
                                                    <UU5.Bricks.Div className='table_cell' id="statusWidth"><UU5.Bricks.Lsi lsi={Lsi.approvalStatus} /></UU5.Bricks.Div>
                                                    <UU5.Bricks.Div className='table_cell' >
                                                        <UU5.Bricks.Div className="select" >
                                                            {studentData.erasmus.destionations[0].school && (<UU5.Forms.Select
                                                                style={{ margin: "0px" }}
                                                                colorSchema={colorSchema}
                                                                borderRadius={borderRadius}
                                                                name="universityApprovalStatus1"
                                                                value={approvalStatesCode(0)?.name}
                                                            >
                                                                {approvalStates.map((item, index) => {
                                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                                    />
                                                                })
                                                                }
                                                            </UU5.Forms.Select>)}
                                                        </UU5.Bricks.Div>
                                                    </UU5.Bricks.Div>
                                                </UU5.Bricks.Div>
                                            </UU5.Bricks.Div>)}
                                    {studentData.erasmus.destionations[1]["school"] !== undefined &&
                                        studentData.erasmus.destionations[1]["school"] !== "not selected" &&
                                        studentData.erasmus.destionations[1].from !== null &&
                                        studentData.erasmus.destionations[1].to !== null && (
                                            <UU5.Bricks.Div className='table_row'>
                                                <UU5.Bricks.Div className='table_small'>
                                                    <UU5.Bricks.Div className='table_cell' ><UU5.Bricks.Lsi lsi={Lsi.universityName} /></UU5.Bricks.Div>
                                                    <UU5.Bricks.Div className='table_cell'>
                                                        {studentData.erasmus.destionations[1].school}
                                                    </UU5.Bricks.Div>
                                                </UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_small' id="status">
                                                    <UU5.Bricks.Div className='table_cell' id="statusWidth"><UU5.Bricks.Lsi lsi={Lsi.approvalStatus} /></UU5.Bricks.Div>
                                                    <UU5.Bricks.Div className='table_cell'>
                                                        <UU5.Bricks.Div className="select" >
                                                            <UU5.Forms.Select
                                                                colorSchema={colorSchema}
                                                                borderRadius={borderRadius}
                                                                style={{ margin: "0px" }}
                                                                name="universityApprovalStatus2"
                                                                value={approvalStatesCode(1)?.name}
                                                            >
                                                                {approvalStates.map((item, index) => {
                                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                                    />
                                                                })
                                                                }
                                                            </UU5.Forms.Select>
                                                        </UU5.Bricks.Div>
                                                    </UU5.Bricks.Div>
                                                </UU5.Bricks.Div>
                                            </UU5.Bricks.Div>)}
                                    {studentData.erasmus.destionations[2]["school"] !== undefined &&
                                        studentData.erasmus.destionations[2]["school"] !== "not selected" &&
                                        studentData.erasmus.destionations[2].from !== null &&
                                        studentData.erasmus.destionations[2].to !== null && (
                                            <UU5.Bricks.Div className='table_row'>
                                                <UU5.Bricks.Div className='table_small'>
                                                    <UU5.Bricks.Div className='table_cell' ><UU5.Bricks.Lsi lsi={Lsi.universityName} /></UU5.Bricks.Div>
                                                    <UU5.Bricks.Div className='table_cell'>
                                                        {studentData.erasmus.destionations[2].school}
                                                    </UU5.Bricks.Div>
                                                </UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_small' id="status">
                                                    <UU5.Bricks.Div className='table_cell' id="statusWidth"><UU5.Bricks.Lsi lsi={Lsi.approvalStatus} /></UU5.Bricks.Div>
                                                    <UU5.Bricks.Div className='table_cell'>
                                                        <UU5.Bricks.Div className="select" >
                                                            <UU5.Forms.Select
                                                                colorSchema={colorSchema}
                                                                borderRadius={borderRadius}
                                                                style={{ margin: "0px" }}
                                                                name="universityApprovalStatus3"
                                                                value={approvalStatesCode(2)?.name}
                                                            >
                                                                {approvalStates.map((item, index) => {
                                                                    return <UU5.Forms.Select.Option key={index} value={item} readOnly={item === 'canceled'}
                                                                    />
                                                                })
                                                                }
                                                            </UU5.Forms.Select>
                                                        </UU5.Bricks.Div>
                                                    </UU5.Bricks.Div>
                                                </UU5.Bricks.Div>
                                            </UU5.Bricks.Div>)}
                                </UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div id='block'>
                            <UU5.Forms.Checkbox
                                colorSchema={colorSchema}
                                name="requestForPrerecogn"
                                label={<UU5.Bricks.Lsi lsi={Lsi.prerocognRequest} />}
                                value={studentData.erasmus.requestForPrerecogn == 'false' ? false : true}
                                labelPosition="right"
                                type={2}
                            />

                            <UU5.Forms.Checkbox
                                colorSchema={colorSchema}
                                name="insurance"
                                label={<UU5.Bricks.Lsi lsi={Lsi.insurance} />}
                                value={studentData.erasmus.insurance == 'false' ? false : true}
                                labelPosition="right"
                                type={2}
                            />

                            <UU5.Forms.Checkbox
                                colorSchema={colorSchema}
                                name="learningAgreement"
                                label={<UU5.Bricks.Lsi lsi={Lsi.la} />}
                                value={studentData.erasmus.learningAgreement == 'false' ? false : true}
                                labelPosition="right"
                                type={2}
                            />

                            <UU5.Forms.Checkbox
                                colorSchema={colorSchema}
                                name="grant"
                                label={<UU5.Bricks.Lsi lsi={Lsi.grant} />}
                                value={studentData.erasmus.grant == 'false' ? false : true}
                                labelPosition="right"
                                type={2}
                            />
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>

                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.erasmusInfo} />} className={Css.Erasmus()}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className="languages" >
                            <UU5.Forms.Select
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                                name="currentSemester"
                                label={<UU5.Bricks.Lsi lsi={Lsi.semester} />}
                                readOnly={true}
                                value={studentData?.erasmus.currentSemester}
                            >
                                {Object.keys(Config.SEMESTER).map(function (key, index) {
                                    return <UU5.Forms.Select.Option key={index} value={Config.SEMESTER[key]} />
                                })}
                            </UU5.Forms.Select>

                            <UU5.Forms.Select
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                                name="semester"
                                label={<UU5.Bricks.Lsi lsi={Lsi.mobSemester} />}
                                controlled={false}
                                readOnly={true}
                                value={studentData?.erasmus.semesterYear.match(/^(\S+)\s(.*)/).slice(1)}
                            >
                                {["Winter", "Summer"].map(function (item, index) {
                                    return <UU5.Forms.Select.Option key={index} value={item} />
                                })}
                            </UU5.Forms.Select>
                            <UU5.Forms.DatePicker
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                                label={<UU5.Bricks.Lsi lsi={Lsi.mobYear} />}
                                name="year"
                                readOnly={true}
                                value={studentData.erasmus.semesterYear.split(' ').pop()}
                                step="years"
                                controlled={false}
                            />
                            <UU5.Forms.Select
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                                name="specialization"
                                readOnly={true}
                                label={<UU5.Bricks.Lsi lsi={Lsi.specialization} />}
                                controlled={false}
                                value={studentData.erasmus?.studyProgramme}
                            >
                                <UU5.Forms.Select.Option key="ECONOM_MANAGEMENT" value="Economics and Management" content={inputLsi.ECONOM_MANAGEMENT} />
                                <UU5.Forms.Select.Option key="IT" value="Information Technology" content={inputLsi.IT} />
                                <UU5.Forms.Select.Option key="ICT_PROJECT_MANAGEMENT" value="ICT Project Management" content={inputLsi.ICT_PROJECT_MANAGEMENT} />

                            </UU5.Forms.Select>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div className={Css.table()} >
                            <UU5.Bricks.Div id="table">
                                <UU5.Bricks.Div className='theader'>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.creadits} /></UU5.Bricks.Div>
                                </UU5.Bricks.Div>
                                {destionation1 && <UU5.Bricks.Div className='table_row'>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Select className={Css.Select()}
                                                value={studentData?.erasmus?.destionations[0]?.school}
                                                name="school1"
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                controlled={false}
                                                readOnly={true}
                                            >
                                                {Config.UNIVERSITY.map((item, index) => {
                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                        content={item} />
                                                })
                                                }
                                            </UU5.Forms.Select></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                name="from1"
                                                valueType="iso"
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                value={studentData?.erasmus?.destionations[0]?.from}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={studentData.erasmus.destionations[0].to}
                                                name="to1"
                                                valueType="iso"
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.creadits} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                name='credits1'
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                value={studentData?.erasmus?.destionations[0]?.numberOfCreadits}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                                {destionation2 && <UU5.Bricks.Div className='table_row'>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Select className={Css.Select()}
                                                value={studentData?.erasmus?.destionations[1]?.school}
                                                name="school2"
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            >
                                                {Config.UNIVERSITY.map((item, index) => {
                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                        content={item} />
                                                })
                                                }
                                            </UU5.Forms.Select></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                name="from2"
                                                valueType="iso"
                                                value={studentData?.erasmus?.destionations[1]?.from}
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={studentData?.erasmus?.destionations[1]?.to}
                                                name="to2"
                                                readOnly={true}
                                                valueType="iso"
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.creadits} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                value={studentData?.erasmus?.destionations[1]?.numberOfCreadits}
                                                name='credits2'
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                                {destionation3 && <UU5.Bricks.Div className='table_row'>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Select className={Css.Select()}
                                                value={studentData?.erasmus?.destionations[2]?.school}
                                                name="school3"
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                readOnly={true}
                                            >
                                                {Config.UNIVERSITY.map((item, index) => {
                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                        content={item} />
                                                })
                                                }
                                            </UU5.Forms.Select></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={studentData?.erasmus?.destionations[2]?.from}
                                                name="from3"
                                                valueType="iso"
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                readOnly={true}
                                                controlled={false}
                                                placeholder="approximate date"
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={studentData?.erasmus?.destionations[2]?.to}
                                                name="to3"
                                                valueType="iso"
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                placeholder="approximate date"
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.creadits} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                value={studentData?.erasmus?.destionations[2]?.numberOfCreadits}
                                                name='credits3'
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                                controlled={false}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div style={{ margin: '1rem 0' }}>
                                <UU5.Forms.TextArea
                                    label={<UU5.Bricks.Lsi lsi={Lsi.motivation} />}
                                    placeholder='Insert text here.'
                                    message=""
                                    value={studentData?.erasmus?.motivationDescription}
                                    readOnly={true}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    name="motivationDescription"
                                    controlled={false}
                                />
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className="languages" >
                                {english[0]?.level !== undefined && <UU5.Bricks.Div >
                                    <UU5.Forms.Select
                                        style={{ margin: "0px" }}
                                        readOnly={true}
                                        name="english"
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        label={<UU5.Bricks.Lsi lsi={Lsi.english} />}
                                        value={english[0]?.level}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {german[0]?.level !== undefined && <UU5.Bricks.Div >
                                    <UU5.Forms.Select
                                        readOnly={true}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        style={{ margin: "0px" }}
                                        name="german"
                                        label={<UU5.Bricks.Lsi lsi={Lsi.german} />}
                                        value={german[0]?.level}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {french[0]?.level !== undefined && <UU5.Bricks.Div >
                                    <UU5.Forms.Select
                                        style={{ margin: "0px" }}
                                        name='french'
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        readOnly={true}
                                        label={<UU5.Bricks.Lsi lsi={Lsi.french} />}
                                        value={french[0]?.level}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {russian[0]?.level !== undefined && <UU5.Bricks.Div>
                                    <UU5.Forms.Select
                                        label={<UU5.Bricks.Lsi lsi={Lsi.russian} />}
                                        readOnly={true}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        style={{ margin: "0px" }}
                                        name="russian"
                                        value={russian[0]?.level}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {other.length !== 0 && other?.map((item, index) => {
                                    return (<UU5.Bricks.Div key={index}>
                                        <UU5.Forms.Select
                                            label={item.name}
                                            style={{ margin: "0px" }}
                                            name={"other" + `${index}`}
                                            readOnly={true}
                                            colorSchema={colorSchema}
                                            borderRadius={borderRadius}
                                            value={item.level}
                                        >
                                            {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                                return <UU5.Forms.Select.Option key={index} value={key}
                                                    content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                            })
                                            }
                                        </UU5.Forms.Select>
                                    </UU5.Bricks.Div>)
                                })}
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Edirt Erasmus information */}


                {/* Edit Personal info */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.personalInfo} />}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className={Css.PersonalInfo()}>
                            <UU5.Bricks.Div className="languages">
                                <UU5.Forms.DatePicker
                                    name="dateOfBirth"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.dateOfBirth} />}
                                    valueType="iso"
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    readOnly={true}
                                    placeholder={UU5.Common.Tools.getDateString("1990-11-21", { country: "cs-cz" })}
                                    value={studentData?.erasmus?.personalInfo?.dateOfBirth}
                                    controlled={false}
                                />
                                <UU5.Forms.Text
                                    name="nationality"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.nationality} />}
                                    readOnly={true}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    value={studentData?.erasmus?.personalInfo?.nationality}
                                    controlled={false}
                                />
                                <UU5.Forms.Text
                                    name="email"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.email} />}
                                    value={studentData?.erasmus?.personalInfo?.email}
                                    controlled={false}
                                    readOnly={true}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    onBlur={(opt) => {
                                        opt.component.setValue(opt.value);
                                        if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(opt.value) && opt.value.length > 0) {
                                            opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.invalidEmail} />);
                                        }
                                    }}
                                />
                                <UU5.Forms.Text
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    name="phone"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.phone} />}
                                    controlled={false}
                                    placeholder="+420xxxxxxxxx"
                                    value={studentData?.erasmus?.personalInfo?.phone}
                                    readOnly={true}
                                    onBlur={(opt) => {
                                        opt.component.setValue(opt.value);
                                        if (!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(opt.value) && opt.value.length > 0) {
                                            opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.invalidPhone} />);
                                        }
                                    }}
                                />
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className={Css.table()} id="enterLanguageDiv" style={{ margin: '1rem 0' }}>
                                <UU5.Forms.TextArea
                                    name="adress"
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    label={<UU5.Bricks.Lsi lsi={Lsi.adress} />}
                                    controlled={false}
                                    value={studentData?.erasmus?.personalInfo?.adress}
                                    readOnly={true}
                                />
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className="languages" >
                                <UU5.Forms.Text
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    name="banckAccount"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.bankAccount} />}
                                    controlled={false}
                                    value={studentData?.erasmus?.personalInfo?.banckAccount}
                                    readOnly={true}
                                />
                                <UU5.Forms.Text
                                    name="bankName"
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    label={<UU5.Bricks.Lsi lsi={Lsi.bankName} />}
                                    controlled={false}
                                    value={studentData?.erasmus?.personalInfo?.bankName}
                                    readOnly={true}
                                />
                                <UU5.Forms.Text
                                    name="swift"
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    label={<UU5.Bricks.Lsi lsi={Lsi.swift} />}
                                    controlled={false}
                                    value={studentData?.erasmus?.personalInfo?.swift}
                                    readOnly={true}
                                />
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Edit Personal info */}


                {/* Subjects recognition */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.subjects} />}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className={Css.table()}>
                            <UU5.Bricks.Div id='tableSubjects' >
                                {studentData?.erasmus?.subjectsRecognition && <UU5.Bricks.Div className='theader'>
                                    <UU5.Bricks.Div className='table_header'>{<UU5.Bricks.Lsi lsi={Lsi.uuSubjectName} />}</UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'>{<UU5.Bricks.Lsi lsi={Lsi.partnerUniName} />}</UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                                {studentData?.erasmus?.subjectsRecognition ? studentData?.erasmus?.subjectsRecognition?.map((item, index) => {
                                    return (
                                        <UU5.Bricks.Div className='table_row' key={index}>
                                            <UU5.Bricks.Div className='table_small'>
                                                <UU5.Bricks.Div className='table_cell'>{<UU5.Bricks.Lsi lsi={Lsi.uuSubjectName} />}</UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_cell'>
                                                    <UU5.Forms.Text style={{ marginTop: 0, marginBottom: "0.3rem" }}
                                                        name={"subjectUcl" + `${item}`}
                                                        controlled={false}
                                                        colorSchema={colorSchema}
                                                        borderRadius={borderRadius}
                                                        value={item?.subjectNameUcl || ""}
                                                        readOnly={true}
                                                    /></UU5.Bricks.Div>
                                            </UU5.Bricks.Div>
                                            <UU5.Bricks.Div className='table_small'>
                                                <UU5.Bricks.Div className='table_cell'>{<UU5.Bricks.Lsi lsi={Lsi.partnerUniName} />}</UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_cell'>
                                                    <UU5.Forms.Text style={{ marginTop: 0, marginBottom: "0.3rem" }}
                                                        name={"subject" + `${item}`}
                                                        readOnly={true}
                                                        colorSchema={colorSchema}
                                                        borderRadius={borderRadius}
                                                        value={item?.subjectNameForeignUni || ""}
                                                        controlled={false}
                                                    /></UU5.Bricks.Div>
                                            </UU5.Bricks.Div>

                                        </UU5.Bricks.Div>)
                                }) : <UU5.Bricks.Div><UU5.Bricks.Lsi lsi={Lsi.recognitionNotCreated} /></UU5.Bricks.Div>}
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Subjects recognition */}


                {/* Attachments */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.files} />} className={Css.Attachments()} >
                    <UU5.Bricks.Div id='attachments' style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div style={{ float: 'left', marginRight: '0.4rem' }}><UU5.Bricks.Lsi lsi={Lsi.resume} /></UU5.Bricks.Div>
                        <UU5.Bricks.Div >
                            <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${studentData.erasmus.attachments.cv.code}`}>
                                {studentData.erasmus.attachments.cv.name}</UU5.Bricks.Link>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div style={{ float: 'left', marginRight: '0.4rem' }}><UU5.Bricks.Lsi lsi={Lsi.ml} /></UU5.Bricks.Div>
                        <UU5.Bricks.Div >
                            <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${studentData.erasmus.attachments.cv.code}`}>
                                {studentData.erasmus.attachments.cv.name}</UU5.Bricks.Link>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div ><UU5.Bricks.Lsi lsi={Lsi.syllables} /></UU5.Bricks.Div>
                        {studentData.erasmus.attachments.syllabus.map((item, index) => {
                            return (
                                <UU5.Bricks.Div key={index}>
                                    <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${item.code}`}>
                                        {item.name}</UU5.Bricks.Link>
                                </UU5.Bricks.Div>
                            )
                        })}
                        <UU5.Bricks.Div ><UU5.Bricks.Lsi lsi={Lsi.extraAttachments} /></UU5.Bricks.Div>
                        {studentData.erasmus?.attachments.file.map((item, index) => {
                            return (
                                <UU5.Bricks.Div key={index}>
                                    <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${item.code}`}>
                                        {item.name}</UU5.Bricks.Link>
                                </UU5.Bricks.Div>
                            )
                        })}
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Attachments */}


                {/* Add notes */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.notes} />}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className={Css.Notes()}>
                            <UU5.Bricks.Section >
                                <UU5.RichText.EditorInput controlled={true} colorSchema={colorSchema} borderRadius={borderRadius} label={<UU5.Bricks.Lsi lsi={Lsi.comment} />} value={note ? note : ""} name="personalNote" />
                            </UU5.Bricks.Section>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div className={Css.Notes()} style={{ marginTop: '2rem' }}>
                            <UU5.Bricks.Section >
                                <UU5.RichText.EditorInput controlled={true} colorSchema={colorSchema} borderRadius={borderRadius} value={studentData?.erasmus?.noteForCoordinator ? studentData?.erasmus?.noteForCoordinator : ""} label={<UU5.Bricks.Lsi lsi={Lsi.personalNote} />} name="noteForCoordinator" />
                            </UU5.Bricks.Section>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Add notes */}
            </UU5.Bricks.Tabs>


        </UU5.Forms.ContextForm>
    )
};
export { ErasmusApplicantsUpdateForm }
export default ErasmusApplicantsUpdateForm;
