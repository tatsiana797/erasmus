import Config from "../config/config";

const table = () => Config.Css.css`
#tableSubjects {
  width:51rem;
  margin: 0 auto;
  .table_cell {
    width: 25.5rem;
    padding-right: 0.4rem;
  }

  @media screen and (max-width: 930px) {
    width:48rem;
    margin: 0 auto;
    .table_cell {
      width: 24rem;
    }
  }
  @media screen and (max-width: 890px) {
    width:47rem;
    margin: 0 auto;
    .table_cell {
      width: 23.5rem;
    }
  }
  @media screen and (max-width: 850px) {
    width:46rem;
    margin: 0 auto;
    .table_cell {
      width: 23rem;
    }
  }
  @media screen and (max-width: 830px) {
    width:44rem;
    margin: 0 auto;
    .table_cell {
      width: 22rem;
    }
  }
  @media screen and (max-width: 810px) {
    width:42rem;
    margin: 0 auto;
    .table_cell {
      width: 21rem;
    }
  }
  @media screen and (max-width: 755px) {
    width:40rem;
    margin: 0 auto;
    .table_cell {
      width: 20rem;
    }
  }
  @media screen and (max-width: 715px) {
    width:38rem;
    margin: 0 auto;
    .table_cell {
      width: 19rem;
    }
  }
  @media screen and (max-width: 685px) {
    width:36rem;
    margin: 0 auto;
    .table_cell {
      width: 18rem;
    }
  }
  @media screen and (max-width: 650px) {
    width:unset;
    .table_cell {
      width: unset;
    }
  }
}
.table {
    display: table;
    text-align: center;
    width: 100%;
    margin: 10% auto 0;
    border-collapse: separate;
    font-weight: 400;
  }
  
  .table_row {
    display: table-row;
     
  }
  
  .theader {
    display: table-row;
  }
  
  .table_header {
    display: table-cell;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #0000008a;
  }
 
  
  .table_small {
    display: table-cell;
  }
  
  .table_row > .table_small > .table_cell:nth-child(odd) {
    display: none;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  
  .table_row > .table_small > .table_cell {
    padding-top: 3px;
    padding-bottom: 3px;
    color: #5b5b5b;
  }
  
  @media screen and (max-width: 900px) {
    .table {
      width: 90%
    }
  }
  
  @media screen and (max-width: 650px) {
    .table {
      display: block;
    }
    .table_row:nth-child(2n+3) {
      background: none;
    }
    .theader {
      display: none;
    }
    .table_row > .table_small > .table_cell:nth-child(odd) {
      display: inherit;
      width: 50%;
    }
    .table_cell {
      display: table-cell;
      width: 50%;
      font-size: 12px;
    }
    .table_row {
      display: table;
      width: 100%;
      border-collapse: separate;
      padding-bottom: 20px;
      margin: 5% auto 0;
      text-align: center;
       border-bottom: 1px solid #e0e0e0;
    }
    .table_small {
      display: table-row;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-left: none;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-right: none;
    }
  }

`
const Select = () => Config.Css.css`
.uu5-forms-items-input{
  height: 32px;
  display: grid;
  overflow: auto;
}
margin-top: 0rem !important;
margin-bottom: 0rem !important;
max-width: 100%;
 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Number = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;;
  max-width: 100%;
  @media screen and (max-width: 650px) {
    max-width: 100%;
 }
  `
const Date = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
  max-width: 100%;

 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Language = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
  max-width: 100%;
 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Tile = () => Config.Css.css`
  min-width: fit-content;
 @media screen and (max-width: 650px) {
  .table_row:last-child {
    border-bottom: unset;
  }
}
  `

const UpdateWrapper = () => Config.Css.css`
.table_cell {
  width:25rem;
}
#block {
  width: 50rem !important;
  margin: 0 auto !important;
}
.table_row{
  width:50rem;
}
.table_small{
  vertical-align: middle;
}


@media screen and (max-width: 900px) {
  .table_cell {
    width:24rem;
  }
  #block {
    width: 48rem !important;
    margin: 0 auto !important;
  }
}

@media screen and (max-width: 870px) {
  .table_cell {
    width:22rem;
  }
  #block {
    width: 44rem !important;
    margin: 0 auto !important;
  }
}
@media screen and (max-width: 810px) {
  .table_cell {
    width:20rem;
  }
  #block {
    width: 40rem !important;
    margin: 0 auto !important;
  }
}
@media screen and (max-width: 716px) {
  .table_cell {
    width:18rem;
  }
  #block {
    width: 36rem !important;
    margin: 0 auto !important;
  }
}

@media screen and (max-width: 650px) {
  .background {
    background: transparent;
  }
  #block {
    width: unset !important;
  }
  .table_row {
    width:unset;
  }
  }

}`
const Zoom = () => Config.Css.css`
@media screen and (max-width: 1720px) {
  zoom: 90%;
   .uutiles-nurh0u {
     background: none;
   }
 
 }
 
 @media screen and (max-width: 1350px) {
  zoom: 80%;
 }
 
 @media screen and (max-width: 1147px) {
   zoom: 75%;
 }
 @media screen and (max-width: 1076px) {
   zoom: 70%;

 }
 
 @media screen and (max-width: 1015px) {
   zoom: 65%;
   font-size: 1rem!important;

   .uu5-bricks-panel-header-content, button{
    font-size: 1rem!important;
   }
   .uu5-forms-label {
    font-size: 1rem!important;
   }
 }
 @media screen and (max-width: 894px) {
   zoom: 60%;
   font-size: 1rem!important;

   .uu5-bricks-panel-header-content, button{
    font-size: 1rem!important;
   }
   .uu5-forms-label {
    font-size: 1rem!important;
   }
 }
 @media screen and (max-width: 840px) {
  zoom: 55%;

}

`

const Erasmus = () => Config.Css.css`

.uu5-forms-label, .uu5-col-xs12 {
  box-sizing: border-box;
  padding: 0;
  padding-right: 0.4rem;
  padding-bottom:0.2rem;
}
 .table_header {
   font-weight: inherit;
  color: #0000008a;
   padding-bottom:0.2rem;
 }
.languages {
  width: 20rem;
  margin: 0 auto;
}
 .uu5-forms-input, .uu5-forms-input-m, uu5-forms-input-initial {
margin:0!important;
margin-bottom: 0.2rem !important;
}

#table {
  width: 100%;
  margin: 0 auto;
}

@media screen and (max-width: 650px) {

  .languages {
    width: unset;
  }
}

`
const SelectErasmus = () => Config.Css.css`
margin-top: 0rem !important;
margin-bottom: 0rem !important;

`

const Notes = () => Config.Css.css`
.uu5-forms-label, .uu5-col-xs12 {
  padding: 0;
  padding-right: 0.4rem;
  padding-bottom:0.2rem;
}
`
const PersonalInfo = () => Config.Css.css`
.languages {
  width: 20rem;
  margin: 0 auto;
}
.uu5-forms-label, .uu5-col-xs12 {
  padding: 0;
  padding-right: 0.4rem;
  padding-bottom:0.2rem;
}
.uu5-forms-input, .uu5-forms-input-m, uu5-forms-input-initial {
  margin:0!important;
  margin-bottom: 0.2rem !important;
  }
@media screen and (max-width: 650px) {
  .languages {
    width: unset;
  }
}

`
const Attachments = () => Config.Css.css`
#attachments{
  width: 17rem;
  margin: 0 auto;
}
@media screen and (max-width: 650px) {
  #attachments {
    width: unset;
  }
}

`
const ZoomMain = () => Config.Css.css`
@media screen and (max-width: 1715px) {
zoom: 93%;
}
`
const ButtonPadding = () => Config.Css.css`
.uu5-bricks-modal-body {
  padding: 8px 40px 8px 32px;
}`

export default {
  table,
  Select,
  Number,
  Date,
  Language,
  Tile,
  UpdateWrapper,
  Zoom,
  Erasmus,
  SelectErasmus,
  PersonalInfo,
  Notes,
  Attachments,
  ZoomMain,
  ButtonPadding

}