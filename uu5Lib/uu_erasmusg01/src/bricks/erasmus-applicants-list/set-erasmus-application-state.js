//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./erasmus-applicants-list-lsi"
import "uu5richtextg01";
//@@viewOff:imports

const SetErasmusApplicationState = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "SetErasmusApplicationState",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        onCancel: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        studentData: UU5.PropTypes.object,
        onSaveDone: UU5.PropTypes.func,
        onSaveFail: UU5.PropTypes.func,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        studentData: undefined,
        onSaveDone: () => { },
        onSaveFail: () => { },
        shown: false,
        onCancel: () => { },
        onSave: () => { },
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {

        return (
            <UU5.Forms.ContextModal
                shown={props.shown}
                header={<Header />}
                footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                onClose={props.onCancel}
                size="l"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    onSave={props.onSave}
                    onSaveDone={props.onSaveDone}
                    onSaveFail={props.onSaveFail}
                    onCancel={props.onCancel}
                    studentData={props.studentData}
                    colorSchema={props.colorSchema}
                    elevation={props.elevation}
                />
            </UU5.Forms.ContextModal>
        );
        //@@viewOff:render
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return (
        <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} controlled={false} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.submit} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
    );
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.applStateHeader} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.infoSetApplState} />}
        />
    );
}


function Form({ onSave, onCancel, onSaveDone, onSaveFail, studentData, colorSchema, elevation }) {
    let items = [];
    Config.erasmusApplicationSetState.map(state => {
        items.push({
            value: state.code,
            content: (
                <UuP.Bricks.State
                    key={state.code}
                    stateName={state.code}
                    stateType={state.type}
                    type={"button"}
                />
            )
        });
    })
    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onSaveDone={onSaveDone}
            onSaveFail={onSaveFail}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >

            <UU5.Bricks.Div >
                <UU5.Forms.SwitchSelector
                    elevation={elevation}
                    colorSchema={colorSchema}
                    items={items}
                    label={<UU5.Bricks.Lsi lsi={Lsi.state} />}
                    value={studentData?.erasmus?.erasmusState}
                    name="state" />
            </UU5.Bricks.Div>


        </UU5.Forms.ContextForm>
    )
};
export { SetErasmusApplicationState }
export default SetErasmusApplicationState;
