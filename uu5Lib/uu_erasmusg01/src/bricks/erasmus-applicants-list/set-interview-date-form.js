//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef, useState, useEffect } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./erasmus-applicants-list-lsi"
import Css from "./erasmus-list-css"
//@@viewOff:imports

const SetInterviewDateForm = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "SetInterviewDateForm",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        onCancel: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        notesForSemester: UU5.PropTypes.string,
        commonList: UU5.PropTypes.array,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        notesForSemester: undefined,
        commonList: null,
        shown: false,
        onCancel: () => { },
        onSave: () => { },
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {

        return (
            <UU5.Forms.ContextModal
                className={Css.ButtonPadding()}
                shown={props.shown}
                header={<Header />}
                footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                onClose={props.onCancel}
                size="l"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    onSave={props.onSave}
                    onCancel={props.onCancel}
                    studentData={props.studentData}
                    commonList={props?.commonList}
                    notesForSemester={props?.notesForSemester}
                    colorSchema={props.colorSchema}
                    borderRadius={props.borderRadius}
                />
            </UU5.Forms.ContextModal>
        );
        //@@viewOff:render
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return (
        <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.submit} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
    );
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.setInterviewHeader} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.setInterviewInfo} />}
        />
    );
}

function Form({ onSave, onCancel, commonList, notesForSemester, colorSchema, borderRadius }) {

    const [interviewsList, setInterviewsList] = useState()
    const [interviews, setInterviews] = useState()
    useEffect(() => {
        const interviewDatesList = commonList?.filter(item => item.semesterId === notesForSemester)[0]?.interviewDates
        interviewDatesList?.length !== 0 ? setInterviews(interviewDatesList) : setInterviews([""])
    }, [commonList])
    function handleAddInterviewDate() {
        interviews ?
            setInterviews(interviews?.concat("")) : setInterviews([].concat(""))
    }
    function handleDeleteInterviewDate(item, i) {
        setInterviews(interviews.filter((el, index) => {
            return el !== item && i !== index

        }))
    }
    useEffect(() => {
        setInterviewsList(interviews?.map((item, index) => {
            return (<UU5.Bricks.Div key={index} style={{ display: "flex", margin: "1rem 0rem" }}><UU5.Forms.DateTimePicker
                colorSchema={colorSchema}
                borderRadius={borderRadius}
                style={{ marginTop: "0", width: '100%' }}
                name={`interviewDate-${index}`}
                value={interviews[index]}
                onChange={(opt) => setInterviews(interviews.map((item, i) => {
                    return index === i ? item = opt.value : item
                }))}
                key={index}
            /> <UU5.Bricks.Button
                borderRadius={borderRadius}
                style={{ display: "flex", marginLeft: "1rem" }}
                key={index + 2} colorSchema="grey"
                onClick={() => handleDeleteInterviewDate(item, index)}>
                    <UU5.Bricks.Icon icon="mdi-minus" style={{ paddingTop: "5px" }} /></UU5.Bricks.Button>
            </UU5.Bricks.Div>)
        }))
    }, [interviews])
    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            <UU5.Bricks.Div>
                {interviewsList}
                <UU5.Bricks.Div style={{ width: "10rem", margin: "0 auto" }} >
                    <UU5.Bricks.Button id='buttonStyle' bgStyle="transparent" size='l' onClick={() => handleAddInterviewDate()} borderRadius={borderRadius}>
                        <UU5.Bricks.Icon icon="mdi-shape-circle-plus" />
                        <UU5.Bricks.Div>
                            {<UU5.Bricks.Lsi lsi={Lsi.interviewDate} />}
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Button>
                </UU5.Bricks.Div>
            </UU5.Bricks.Div>
        </UU5.Forms.ContextForm>
    )
};
export { SetInterviewDateForm }
export default SetInterviewDateForm;
