const Lsi = {
  erasmusInfo: {
    en: 'Erasmus info',
    cs: "Erasmus info"
},
subjects: {
    en: "Subjects",
    cs: "Předměty"
},
files: {
    en: "Attachments",
    cs: "Přílohy"
},
  ok: {
    en: "OK",
    cs: "OK"
  },
  applDetail: {
    en: "Application details",
    cs: "Informace o žádosti"
  },
  startDate: {
    en: "Start date",
    cs: "Datum zahájení"
  },
  endDate: {
    en: "End date",
    cs: "Datum ukončení"
  },
  interview: {
    en: "Interview date",
    cs: "Datum pohovoru"
  },
  mobInfo: {
    en: "Mobility information",
    cs: "Informace o Erasmu"
  },
  important: {
    en: "Important!",
    cs: "Důležité!"
  },
  coordinator: {
    en: "Erasmus coordinator",
    cs: "Koordinátor programu Erasmus"
  },
  save: {
    en: "Save",
    cs: "Uložit"
  },
  header: {
    en: "Set interview date form",
    cs: "Formulář pro stanovení data pohovoru"
  },
  info: {
    en: "Here is some information related to Erasmus set interview form",
    cs: "Zde jsou informace týkající se formuláře pro pohovor v rámci programu Erasmus"
  },
  availableDates: {
    en: "Available interview dates",
    cs: "Dostupné termíny pohovorů"
  },
  noAvailableDates: {
    en: "No interview dates are available at the moment",
    cs: "V současné době nejsou k dispozici žádné termíny pohovorů"
  },
  headerUpload: {
    en: "Upload files form",
    cs: "Formulář pro nahrávání souborů"
  },
  infoUploadFiles: {
    en: "Here is some information related to Erasmus upload files form ",
    cs: "Zde jsou informace týkající se formuláře pro nahrávání souborů "
  },
  viewApplication: {
    en: "View Erasmus application",
    cs: "Zobrazit přihlášku na Erasmus"
  },
  infoViewApplication: {
    en: "Here is displayed a guiding information",
    cs: "Zde jsou zobrazeny návodné informace"
  },
  approval: {
    en: "Approval",
    cs: "Schválení"
  },
  uuSubjectName: {
    en: "Name of the subject at Unicorn University",
    cs: "Název předmětu na Unicorn University"
  },
  partnerUniName: {
    en: "Name of the subject at Partner University",
    cs: "Název předmětu na partnerské univerzitě"
  },
  semester: {
    en: "Current semester",
    cs: "Stávající semestr"
  },
  mobSemester: {
    en: "Mobility semester",
    cs: "Semestr mobility"
  },
  mobYear: {
    en: "Mobility year",
    cs: "Rok mobility"
  },
  specialization: {
    en: "Study programme",
    cs: "Studijní program"
  },
  date: {
    en: "approximate date",
    cs: "přibližné datum"
  },
  school: {
    en: "School",
    cs: "Škola"
  },
  from: {
    en: "From",
    cs: "Od"
  },
  to: {
    en: "To",
    cs: "Do"
  },
  creadits: {
    en: "Credits",
    cs: "Kredity"
  },
  motivation: {
    en: "What are your main motivations to go on exchange:",
    cs: "Jaké jsou vaše hlavní motivy pro účast v programu Erasmus:"
  },
  english: {
    en: "English",
    cs: "Anglický"
  },
  german: {
    en: "German",
    cs: "Německý"
  },
  french: {
    en: "French",
    cs: "Francouzský"
  },
  russian: {
    en: "Russian",
    cs: "Ruský"
  },
  personalInfo: {
    en: "Personal info",
    cs: "Osobní údaje"
  },
  dateOfBirth: {
    en: "Date of birth",
    cs: "Datum narození"
  },
  nationality: {
    en: "Nationality",
    cs: "Státní příslušnost"
  },
  email: {
    en: "Email",
    cs: "E-mail"
  },
  phone: {
    en: "Phone number",
    cs: "Telefonní číslo"
  },
  invalidPhone: {
    en: "Please provide us with valid phone number",
    cs: "Uveďte prosím platné telefonní číslo"
  },
  invalidEmail: {
    en: "Please provide to us valid  email address",
    cs: "Uveďte nám prosím platnou e-mailovou adresu"
  },
  adress: {
    en: "Adress",
    cs: "Adresa"
  },
  bankAccount: {
    en: "Bank account",
    cs: "Bankovní účet"
  },
  bankName: {
    en: "Bank name",
    cs: "Název banky"
  },
  swift: {
    en: "Clearing/BIC/SWIFT number",
    cs: "Zúčtovací číslo/BIC/SWIFT"
  },
  recognitionNotCreated: {
    en: "Student have not yet created request for subjects recognition.",
    cs: "Student zatím nevytvořil žádost o uznání předmětů."
  },
  resume: {
    en: "Resume: ",
    cs: "Životopis: "
  },
  ml: {
    en: "Motivation letter: ",
    cs: "Motivační dopis: "
  },
  syllables: {
    en: "Syllables: ",
    cs: "Sylaby: "
  },
  extraAttachments: {
    en: "Additional documents: ",
    cs: "Další dokumenty: "
  },
  ECONOM_MANAGEMENT: {
    en: "Economics and Management",
    cs: "Ekonomika a management"
  },
  IT: {
    en: "Information Technology",
    cs: "Informační technologie"
  },
  ICT_PROJECT_MANAGEMENT: {
    en: "ICT Project Management",
    cs: "Řízení projektů ICT"
  },
  uploadFile: {
    en: "Upload file",
    cs: "Nahrát soubor"
  },
  approvalStatus: {
    en: "University / Approval status",
    cs: "Univerzita / Stav schválení"
  }

}

export default Lsi
