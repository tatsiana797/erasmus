//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useState, useEffect } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "./config/config";
import DataObjectStateResolver from "../common/resolver/data-object-state-resolver";
import { useCommonData } from "../common/context/common-data/use-common-data";
import "uu5g04-block-layout";
import Css from "./erasmus-tracker.css"
import SetInterview from "./set-interview";
import Lsi from "./erasmus-application-status-tracker-lsi";
import ViewApplication from "./view-application";
import UploadFiles from "./upload-files";
import useApplication from "../common/context/application/use-application";
//@@viewOff:imports


//@@v
const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "ApplicationStatusTrackerView"
  //@@viewOff:statics
};

export const ApplicationStatusTrackerView = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  propTypes: {
    uuIdentity: UU5.PropTypes.string,
    baseUri: UU5.PropTypes.string,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
    trackerColoSchema: UU5.PropTypes.string,
    trackerBorderRadius: UU5.PropTypes.string,
    trackerElevation: UU5.PropTypes.string,
    erasmusCoordinator: UU5.PropTypes.object,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    uuIdentity: undefined,
    baseUri: undefined,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined,
    trackerColoSchema: undefined,
    trackerBorderRadius: undefined,
    trackerElevation: undefined,
    erasmusCoordinator: undefined

  },
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:hooks
    const commonDataList = useCommonData();
    const applicationDataObject = useApplication();
    const commonList = commonDataList?.data?.itemList
    const [schoolName, setSchoolName] = useState(false)
    const [startDate, setStartDate] = useState()
    const [endDate, setEndDate] = useState()
    const [approvalStatus, setApprovalStatus] = useState(false)
    const [modalShown, setModalshown] = useState(false)
    const [shownViewApplication, setShownViewApplication] = useState(false)
    const [showUploadFiles, setShowUploadFiles] = useState(false)
    const [popperShown, setPopperShown] = useState(true)
    const [status, setStatus] = useState()

    const application = applicationDataObject?.data
    const studentCommonData = commonList?.filter(item => {
      return item?.semesterId === application?.erasmus?.semesterYear
    })

    {/* School Name */ }
    useEffect(() => {
      let isNotAllDenied = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final' || item.universityApprovalStatus === 'initial'
      });
      let isNotInitial = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
      });

      let statusType;
      if (isNotInitial) {
        statusType = application?.erasmus?.destionations?.find(item => {
          return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
        }).school
      } else if (!isNotAllDenied) {
        let reversed = application?.erasmus?.destionations?.reverse()
        statusType = reversed?.find(item => {
          return item.universityApprovalStatus === 'canceled'
        })?.school
      }
      else {
        statusType = application?.erasmus?.destionations?.find(item => {
          return item.universityApprovalStatus === 'initial'
        })?.school
      }
      setSchoolName(statusType);

    }, [application?.erasmus?.destionations])
    {/* School Name */ }


    {/* StartDate */ }
    useEffect(() => {
      let isNotAllDenied = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final' || item.universityApprovalStatus === 'initial'
      });
      let isNotInitial = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
      });

      let statusType;
      if (isNotInitial) {
        statusType = application?.erasmus?.destionations?.find(item => {
          return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
        }).from
      } else if (!isNotAllDenied) {
        let reversed = application?.erasmus?.destionations?.reverse()
        statusType = reversed?.find(item => {
          return item.universityApprovalStatus === 'canceled'
        })?.from
      }
      else {
        statusType = application?.erasmus?.destionations?.find(item => {
          return item.universityApprovalStatus === 'initial'
        })?.from
      }
      setStartDate(statusType);

    }, [application?.erasmus?.destionations])
    {/* Start date */ }


    {/* End Date */ }
    useEffect(() => {
      let isNotAllDenied = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final' || item.universityApprovalStatus === 'initial'
      });
      let isNotInitial = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
      });

      let statusType;
      if (isNotInitial) {
        statusType = application?.erasmus?.destionations?.find(item => {
          return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
        })?.to
      } else if (!isNotAllDenied) {
        let reversed = application?.erasmus?.destionations?.reverse()
        statusType = reversed?.find(item => {
          return item.universityApprovalStatus === 'canceled'
        })?.to
      }
      else {
        statusType = application?.erasmus?.destionations?.find(item => {
          return item.universityApprovalStatus === 'initial'
        })?.to
      }
      setEndDate(statusType);

    }, [application?.erasmus?.destionations])
    {/* End date */ }

    {/* Approval status */ }
    useEffect(() => {
      let isNotAllDenied = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final' || item.universityApprovalStatus === 'initial'
      });
      let isNotInitial = application?.erasmus?.destionations?.some(item => {
        return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
      });
      let state = Config.universityApprovalState?.find(item => {
        let statusType;
        if (isNotInitial) {
          statusType = item.code === application?.erasmus?.destionations?.find(item => {
            return item.universityApprovalStatus === 'alternative-final' || item.universityApprovalStatus === 'final'
          })?.universityApprovalStatus
        } else if (!isNotAllDenied) {
          let reversed = application?.erasmus?.destionations?.reverse()
          statusType = item.code === reversed?.find(item => {
            return item.universityApprovalStatus === 'canceled'
          })?.universityApprovalStatus
        }
        else {
          statusType = item.code === application?.erasmus?.destionations?.find(item => {
            return item.universityApprovalStatus === 'initial'
          })?.universityApprovalStatus
        }
        return statusType
      }
      )
      setStatus(state?.name)
      setApprovalStatus(<UU5.Bricks.Label bgStyle={'outline'} colorSchema={state?.type}>{state?.name}</UU5.Bricks.Label>)
    }, [application?.erasmus?.destionations])
    {/* Approval status */ }

    function handleCancelSetInterviewDate() {
      setModalshown(false)
    }

    function handleOnClickViewApplication() {
      setPopperShown(true)
      setShownViewApplication(true)
    }

    function handleOnClickUploadFiles() {
      setShowUploadFiles(true)
      setPopperShown(popperShown)
    }
    function handleClickInterview() {
      setModalshown(true)
    }
    async function handleUploadFilesOnSave(opt) {

      for (const [key, value] of Object.entries(opt.values)) {
        const newData = {
          id: application.id,
          studentId: application.studentId,
          fileId: key,
          file: value,
        };
        try {
          await applicationDataObject.handlerMap.updateAttachment(newData);
        } catch (e) {
          opt.component.saveFail(e);
          return;
        }
      }
      opt.component.reset()
      setShowUploadFiles(false)
    }

    async function handleUplodFileOnCancel(opt) {
      setShowUploadFiles(false)
    }

    async function handleSaveSetInterviewDate(opt) {
      const studentErasmus = {
        studyProgramme: application?.erasmus?.studyProgramme,
        personalInfo: application?.erasmus?.personalInfo,
        interviewDate: opt?.values?.interviewDate,
        requestForPrerecogn: application?.erasmus?.requestForPrerecogn,
        insurance: application?.erasmus?.insurance,
        learningAgreement: application?.erasmus?.learningAgreement,
        grant: application?.erasmus?.grant,
        specialization: application?.erasmus?.specialization,
        currentSemester: application?.erasmus?.currentSemester,
        motivationDescription: application?.erasmus?.motivationDescription,
        attachments: application?.erasmus?.attachments,
        destionations: application?.erasmus?.destionations,
        semesterYear: application?.erasmus?.semesterYear,
        erasmusState: application?.erasmus?.erasmusState || "active",
        language: application.erasmus?.language,
        personalNote: application?.erasmus?.personalNote || "",
        noteForCoordinator: application?.erasmus?.noteForCoordinator || "",
      }
      const newData = {
        studentId: application.studentId,
        id: application.id,
        firstName: application.firstName,
        lastName: application.lastName,
        uuIdentity: application.uuIdentity,
        erasmus: studentErasmus
      }
      try {
        await applicationDataObject.handlerMap.update(newData);
        opt.component.reset();
        handleCancelSetInterviewDate(opt);
      } catch (e) {
        console.log(e);
        return;
      }
    }
    function handleClick(event) {
      event.stopPropagation()
    }

    //@@viewOn:render
    return (
      <>
       {JSON.stringify(application) !== '{}' &&  (<DataObjectStateResolver dataObject={applicationDataObject}>
          <DataObjectStateResolver dataObject={commonDataList}>
            {/* {application?.erasmus?.personalInfo?.banckAccount !== undefined && ( */}
            { status !== 'Initial' && (
              <UU5.Bricks.Div className={Css.Destination()}>
                <UU5.Bricks.Panel
                  colorSchema={props.trackerColoSchema}
                  elevation={props.trackerElevation}
                  borderRadius={props.trackerBorderRadius}
                  expanded={true}
                  className='panel accordion'
                  iconExpanded="mdi-chevron-up"
                  iconCollapsed="mdi-chevron-down"
                  header={

                    <UU5.Bricks.Div><UU5.Bricks.Div style={{ float: 'left', width: '18rem' }}><Plus4U5.Bricks.UserPhoto type="circle" width={20} uuIdentity={application?.uuIdentity} style={{ float: "left" }} />
                      <UU5.Bricks.Div className="nameLink"> <UU5.Bricks.Link style={{ float: 'left', margin: '0rem 0.4rem', fontSize: '16px' }}>{" " + application?.firstName + " " + application?.lastName}</UU5.Bricks.Link></UU5.Bricks.Div>
                      <UU5.Bricks.Div className="uuIdLink" style={{ fontSize: '16px' }}>{application?.uuIdentity}</UU5.Bricks.Div></UU5.Bricks.Div>
                      <UU5.Bricks.Div style={{
                        position: "absolute",
                        right: '30px'
                      }}>

                        <div onClick={(event) => handleClick(event)}> <UU5.Bricks.Dropdown
                          label={<UU5.Bricks.Icon icon="mdi-dots-horizontal" />}
                          bgStyle="transparent"
                          popoverLocation="local"
                          iconHidden
                          size='s'
                          colorSchema='white'
                          className='popover'
                        ><UU5.Bricks.Dropdown.Item
                            label={<UU5.Bricks.Text colorSchema='grey'><UU5.Bricks.Lsi lsi={Lsi.applDetail} /></UU5.Bricks.Text>}
                            onClick={handleOnClickViewApplication}
                          />
                          <UU5.Bricks.Dropdown.Item
                            label={<UU5.Bricks.Text colorSchema='grey'><UU5.Bricks.Lsi lsi={Lsi.uploadFile} /></UU5.Bricks.Text>}
                            onClick={(event) => handleOnClickUploadFiles(event)}
                          />
                        </UU5.Bricks.Dropdown>
                        </div>
                      </UU5.Bricks.Div>

                    </UU5.Bricks.Div>}
                >

                  {/* University status */}
                  <UU5.Bricks.Div className="table schoolDiv">
                    <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header"><UU5.Bricks.Lsi lsi={Lsi.approvalStatus} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div id='school'>
                        <UU5.Bricks.Div className="td name schoolName">{schoolName}</UU5.Bricks.Div>
                        <UU5.Bricks.Div className="td button">{approvalStatus}</UU5.Bricks.Div>
                      </UU5.Bricks.Div>

                    </UU5.Bricks.Div>

                  </UU5.Bricks.Div>
                  {/* University status */}

                  {/* Start date */}
                  <UU5.Bricks.Div className="table">
                    <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header "><UU5.Bricks.Lsi lsi={Lsi.startDate} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div className="td name">{startDate}</UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                  </UU5.Bricks.Div>
                  {/* Start date */}

                  {/* End date */}
                  <UU5.Bricks.Div className="table">
                    <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header"><UU5.Bricks.Lsi lsi={Lsi.endDate} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div className="td name">{endDate}</UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                  </UU5.Bricks.Div>
                  {/* End date */}

                  {/* Interview date */}
                  <UU5.Bricks.Div className="table edit">
                    <div onClick={(event) => handleClickInterview(event)}>   <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header date"><UU5.Bricks.Lsi lsi={Lsi.interview} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div className="td name interviewDate">{application?.erasmus?.interviewDate}</UU5.Bricks.Div>
                      <UU5.Bricks.Div className="td button interview">
                      </UU5.Bricks.Div>
                    </UU5.Bricks.Div></div>
                  </UU5.Bricks.Div>
                  {/* Interview date */}

                  {/*Mobility information*/}
                  <UU5.Bricks.Div className="table">
                    <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header"><UU5.Bricks.Lsi lsi={Lsi.mobInfo} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div className="td name"><UU5.Bricks.P style={{ margin: 0, fontSize: '16px' }} name="textEditor" readOnly content={studentCommonData && studentCommonData[0]?.commonNotes} /></UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                  </UU5.Bricks.Div>
                  {/*Mobility information*/}


                  {/*Important!*/}
                  <UU5.Bricks.Div className="table">
                    <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header" ><UU5.Bricks.Lsi lsi={Lsi.important} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div className="td name"> <UU5.Bricks.P style={{ margin: 0, fontSize: '16px' }} name="textEditor" readOnly content={application?.erasmus?.personalNote} /></UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                  </UU5.Bricks.Div>
                  {/*Important!*/}

                  {/* Erasmus coordinator */}
                  <UU5.Bricks.Div className="table" style={{ border: 'unset' }}>
                    <UU5.Bricks.Div className="tr">
                      <UU5.Bricks.Div className="td header"><UU5.Bricks.Lsi lsi={Lsi.coordinator} /></UU5.Bricks.Div>
                      <UU5.Bricks.Div id='coordinator'>
                        <UU5.Bricks.Div style={{ float: 'left', marginRight: '0.4rem' }}>{props.erasmusCoordinator.fullName}</UU5.Bricks.Div>
                        <UU5.Bricks.Div style={{ color: '#0000008a' }}>{props.erasmusCoordinator.uuIdentity}</UU5.Bricks.Div>
                      </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                  </UU5.Bricks.Div>
                  {/* Erasmus coordinator */}


                  <SetInterview
                    shown={modalShown}
                    application={application}
                    onSave={handleSaveSetInterviewDate}
                    onCancel={handleCancelSetInterviewDate}
                    bgStyle={props.bgStyle}
                    colorSchema={props.colorSchema}
                    elevation={props.elevation}
                    borderRadius={props.borderRadius}
                    studentCommonData={studentCommonData ? studentCommonData[0] : null}
                  />

                  <ViewApplication
                    shown={shownViewApplication}
                    application={application}
                    onSave={() => setShownViewApplication(false)}
                    baseUri={props.baseUri}
                    bgStyle={props.bgStyle}
                    colorSchema={props.colorSchema}
                    elevation={props.elevation}
                    borderRadius={props.borderRadius}
                  />

                  <UploadFiles
                    bgStyle={props.bgStyle}
                    colorSchema={props.colorSchema}
                    elevation={props.elevation}
                    borderRadius={props.borderRadius}
                    shown={showUploadFiles}
                    onSave={handleUploadFilesOnSave}
                    onCancel={handleUplodFileOnCancel}
                  />

                </UU5.Bricks.Panel>
              </UU5.Bricks.Div>
            )}
          </DataObjectStateResolver>
        </DataObjectStateResolver>)}
      </>
    )
  }
})
//viewOff:helpers
//viewOn:exports
export default ApplicationStatusTrackerView;
//viewOff:exports
