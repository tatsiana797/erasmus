//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "../config/config";
import ApplicationStatusTrackerView from "./erasmus-application-status-tracker-view";
import CommonDataLoader from "../common/loader/common-data-loader"
import ApplicationLoader from "../common/loader/application-loader"
//@@viewOff:imports
const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "ApplicationStatusTracker"
  //@@viewOff:statics
};
export const ApplicationStatusTracker = createVisualComponent({
  ...STATICS,
  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
    uuIdentity: UU5.PropTypes.string,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
    trackerColoSchema: UU5.PropTypes.string,
    trackerBorderRadius: UU5.PropTypes.string,
    trackerElevation: UU5.PropTypes.string,
    erasmusCoordinator: UU5.PropTypes.object,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
    uuIdentity: undefined,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined,
    trackerColoSchema: undefined,
    trackerBorderRadius: undefined,
    trackerElevation: undefined,
    erasmusCoordinator: undefined
  },
  //@@viewOff:defaultProps
  render(props) {

    //@@viewOn:render
    return (
      <CommonDataLoader baseUri={props.baseUri}>
        <ApplicationLoader uuIdentity={props.uuIdentity} baseUri={props.baseUri} >
          <UU5.Bricks.Div style={{ display: "block", padding: "5px" }}>
            <ApplicationStatusTrackerView
              trackerBorderRadius={props.trackerBorderRadius}
              trackerColoSchema={props.trackerColoSchema}
              trackerElevation={props.trackerElevation}
              uuIdentity={props.uuIdentity}
              baseUri={props.baseUri}
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
              erasmusCoordinator={props.erasmusCoordinator}
            />
          </UU5.Bricks.Div>
        </ApplicationLoader>
      </CommonDataLoader>
    )
  }
})

//viewOn:exports
export default ApplicationStatusTracker;
//viewOff:exports
