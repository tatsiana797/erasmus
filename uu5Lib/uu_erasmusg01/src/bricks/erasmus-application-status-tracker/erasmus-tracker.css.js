import Config from "../config/config";

const Link = () => Config.Css.css`
.uu5-bricks-link .uu5-forms-file-item-name {
  color: #1cb2c3 !important;
}

`
const Destination = () => Config.Css.css`
.nameLink{
  .uu5-bricks-link, .uu5-bricks-link:visited {
    color: white !important;
}

}
.uuIdLink{
    color: #f4f4f48c !important;
}

.accordion {
  max-width: 49rem;
  background: #f5f5f5;
   width: 100%;
}
.table {
  padding: 0.1rem 1rem;
  border-spacing: 0rem 0;
   border-bottom: 0.0001em solid #e4dcdc85;
   width: 100%;
}
.edit:hover {
background: #80808061!important;
cursor: pointer;
}
.popover {
  .uu5-bricks-popover-body {
    background: #f3f3f3!important
  }
  .uu5-bricks-popover {
   box-shadow: unset!important;
}
}
.uu5-bricks-button-transparent:active{
  background-color: #fafafa!important;
  color: black!important;
  border: 1px solid #BDBDBD!important;
  box-shadow: 0 2px 7px 0 rgb(0 0 0 / 34%)!important;
}
.uu5-bricks-button-transparent:focus{
  background-color: #fafafa!important;
  color: black!important;
}
.popperButton:hover{
  color: #0b2a70!important
}
.tr {
  display: table-row;
  line-height:2rem!important;
  font-size: 16px;
}
.td {
  display: table-cell;
}
#coordinator {
  min-width: 14rem;
}
#school{
   margin-left: -1rem;
  display: contents;
}
.header {
  padding-right: 1rem;
  min-width: 19rem;
  font-weight: 500;
}
.name {
  padding-right: 1rem;
  width: 100%
}
.button {
  padding-right: 0;
}
.uu5-forms-input {
  margin-top: 0px;
  margin-bottom: 0px;
}
.schoolName{
  width: 25rem;
}
#student {
  padding-bottom: 2rem!important;
}

@media (max-width: 650px) {
  .header {
    padding-right: 1rem;
    min-width: 10rem;
  }

}

@media (max-width: 710px) {
  #application{
    float: unset!important;
  }
  #student{
    padding-bottom: 2rem!important;
  }
  #file{
    float: unset!important;
  }
  .table{
    border: unset;
  }
  .table,
  .tr,
  .td {
    display: block!important;
  }
  .table{
    padding-bottom: 1rem;
  }
  .interviewDate{
     padding-bottom: 1rem;
  }
  #school{
    padding-bottom: 0.2rem;
  }
  .td {
    margin-left: auto;
    margin-right: 0;
  }
  .header {
    font-weight: 600;
  }
  .accordion {
     width: unset;
  }
  .schoolName {
    width: 75%;
    float: left;
    margin-left: 0;
  }
  #school {
    display: -webkit-box;
    margin-left: unset;
  }
  .date {
    float: left;
  }
  // .button {
  //   display: none!important;
  // }
  .interview {
    display: block!important
  }
  .interviewDate {
float: left;
width: 90%;
  }

  .editor {
    padding-right: unset;
  }
}
  `

  const Erasmus = () => Config.Css.css`

.uu5-forms-label, .uu5-col-xs12 {
  box-sizing: border-box;
  padding: 0;
  padding-right: 0.4rem;
  padding-bottom:0.2rem;
}
 .table_header {
   font-weight: inherit;
  color: #0000008a;
   padding-bottom:0.2rem;
 }
.languages {
  width: 20rem;
  margin: 0 auto;
}
 .uu5-forms-input, .uu5-forms-input-m, uu5-forms-input-initial {
margin:0!important;
margin-bottom: 0.2rem !important;
}

#table {
  width: 100%;
  margin: 0 auto;
}

@media screen and (max-width: 650px) {

  .languages {
    width: unset;
  }
}

`

const PersonalInfo = () => Config.Css.css`
.languages {
  width: 20rem;
  margin: 0 auto;
}
.uu5-forms-label, .uu5-col-xs12 {
  padding: 0;
  padding-right: 0.4rem;
  padding-bottom:0.2rem;
}
.uu5-forms-input, .uu5-forms-input-m, uu5-forms-input-initial {
  margin:0!important;
  margin-bottom: 0.2rem !important;
  }
@media screen and (max-width: 650px) {
  .languages {
    width: unset;
  }
}

`
const table = () => Config.Css.css`

#tableSubjects {
  width:51rem;
  margin: 0 auto;
  .table_cell {
    width: 25.5rem;
    padding-right: 0.4rem;
  }

  @media screen and (max-width: 930px) {
    width:48rem;
    margin: 0 auto;
    .table_cell {
      width: 24rem;
    }
  }
  @media screen and (max-width: 890px) {
    width:47rem;
    margin: 0 auto;
    .table_cell {
      width: 23.5rem;
    }
  }
  @media screen and (max-width: 850px) {
    width:46rem;
    margin: 0 auto;
    .table_cell {
      width: 23rem;
    }
  }
  @media screen and (max-width: 830px) {
    width:44rem;
    margin: 0 auto;
    .table_cell {
      width: 22rem;
    }
  }
  @media screen and (max-width: 810px) {
    width:42rem;
    margin: 0 auto;
    .table_cell {
      width: 21rem;
    }
  }
  @media screen and (max-width: 755px) {
    width:40rem;
    margin: 0 auto;
    .table_cell {
      width: 20rem;
    }
  }
  @media screen and (max-width: 715px) {
    width:38rem;
    margin: 0 auto;
    .table_cell {
      width: 19rem;
    }
  }
  @media screen and (max-width: 685px) {
    width:36rem;
    margin: 0 auto;
    .table_cell {
      width: 18rem;
    }
  }
  @media screen and (max-width: 650px) {
    width:unset;
    .table_cell {
      width: unset;
    }
  }
}
.table {
    display: table;
    text-align: center;
    width: 100%;
    margin: 10% auto 0;
    border-collapse: separate;
    font-weight: 400;
  }
  
  .table_row {
    display: table-row;
     
  }
  
  .theader {
    display: table-row;
  }
  
  .table_header {
    display: table-cell;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #0000008a;
  }
 
  
  .table_small {
    display: table-cell;
  }
  
  .table_row > .table_small > .table_cell:nth-child(odd) {
    display: none;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  
  .table_row > .table_small > .table_cell {
    padding-top: 3px;
    padding-bottom: 3px;
    color: #5b5b5b;
  }
  
  @media screen and (max-width: 900px) {
    .table {
      width: 90%
    }
  }
  
  @media screen and (max-width: 650px) {
    .table {
      display: block;
    }
    .table_row:nth-child(2n+3) {
      background: none;
    }
    .theader {
      display: none;
    }
    .table_row > .table_small > .table_cell:nth-child(odd) {
      display: inherit;
      width: 50%;
    }
    .table_cell {
      display: table-cell;
      width: 50%;
      font-size: 12px;
    }
    .table_row {
      display: table;
      width: 100%;
      border-collapse: separate;
      padding-bottom: 20px;
      margin: 5% auto 0;
      text-align: center;
       border-bottom: 1px solid #e0e0e0;
    }
    .table_small {
      display: table-row;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-left: none;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-right: none;
    }
  }

`
const Select = () => Config.Css.css`
.uu5-forms-items-input{
  height: 32px;
  display: grid;
  overflow: auto;
}
margin-top: 0rem !important;
margin-bottom: 0rem !important;
max-width: 100%;
 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Number = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;;
  max-width: 100%;
  @media screen and (max-width: 650px) {
    max-width: 100%;
 }
  `
const Date = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
  max-width: 100%;

 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `

const Attachments = () => Config.Css.css`
#attachments{
  width: 21rem;
  margin: 0 auto;
}
@media screen and (max-width: 650px) {
  #attachments {
    width: unset;
  }
}

`

const Footer = () => Config.Css.css`
button:first-child {
  display:none;
}

`


export default {
  Destination,
  Erasmus,
  Attachments,
  table,
  Select,
  Date,
  Number,
  PersonalInfo,
  Footer,
  Link

}