//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef } from "uu5g04-hooks";
import "uu5g04-forms";
import Lsi from "./erasmus-application-status-tracker-lsi"
import Config from "./config/config";
//@@viewOff:imports

const SetInterview = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "SetInterview",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        onCancel: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        application: UU5.PropTypes.object,
        studentCommonData: UU5.PropTypes.object,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        onSave: () => { },
        onCancel: () => { },
        shown: false,
        application: undefined,
        studentCommonData: undefined,
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {

        return (
            <UU5.Forms.ContextModal
                shown={props.shown}
                header={<Header />}
                footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                onClose={props.onCancel}
                size="s"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    application={props.application}
                    onSave={props.onSave}
                    onSaveDone={props.onSaveDone}
                    onSaveFail={props.onSaveFail}
                    onCancel={props.onCancel}
                    colorSchema={props.colorSchema}
                    studentCommonData={props.studentCommonData}
                />
            </UU5.Forms.ContextModal>
        );
        //@@viewOff:render
    }
});


function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return <UU5.Forms.ContextControls controlled={false} buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.save} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.header} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.info} />}
        />
    );
}

function Form({ onSave, onCancel, onSaveDone, onSaveFail, studentCommonData, application, colorSchema }) {

    const interviewDates = studentCommonData?.interviewDates?.map((item, index) => {
        return { label: item, name: item, value: application?.erasmus?.interviewDate === item ? true : index === 0 ? true : false }
    })
    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onSaveDone={onSaveDone}
            onSaveFail={onSaveFail}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            {studentCommonData?.interviewDates?.length !==0 ? <UU5.Forms.Radios
                colorSchema={colorSchema}
                name="interviewDate"
                label={<UU5.Bricks.Lsi lsi={Lsi.availableDates} />}
                value={interviewDates}
            /> : <UU5.Bricks.Div><UU5.Bricks.Lsi lsi={Lsi.noAvailableDates} /></UU5.Bricks.Div>}
        </UU5.Forms.ContextForm>
    )
};


export { SetInterview }
export default SetInterview;