import Config from "./config/config";

const top = () => Config.Css.css`
    margin-top: -0.9rem !important;
`
export default {
    top,
}
