//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef, useState } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./erasmus-application-status-tracker-lsi"
import Css from './erasmus-tracker.css';
import uniqid from 'uniqid';
//@@viewOff:imports

const UploadFiles = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "UploadFiles",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        onCancel: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        onSave: () => { },
        onCancel: () => { },
        shown: false,
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    render(props) {
        return (
            <UU5.Bricks.Div>
                <UU5.Forms.ContextModal
                    shown={props.shown}
                    header={<Header />}
                    footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                    onClose={props.onCancel}
                    size="l"
                    mountContent="onFirstOpen"
                    overflow
                    forceRender
                >
                    <Form
                        onSave={props.onSave}
                        onCancel={props.onCancel}
                        colorSchema={props.colorSchema}
                        borderRadius={props.borderRadius}
                    />
                </UU5.Forms.ContextModal>
            </UU5.Bricks.Div>);
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return (
        <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} controlled={false} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.save} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
    );
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.headerUpload} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.infoUploadFiles} />}
        />
    );
}

function Form({ onSave, onCancel, colorSchema, borderRadius }) {
    let generateFileId = UU5.Common.Tools.generateUUID(length = 3)

    const [uploaderList, setUploaderList] = useState([<UU5.Forms.File
        style={{ marginTop: 0 }}
        name={"file_" + generateFileId}
        label={<UU5.Bricks.Lsi lsi={Lsi.uploadFile} />}
        controlled={false}
        placeholder="Upload"
        key={"3"}
        colorSchema={colorSchema}
        borderRadius={borderRadius}
    />]);
    const handleAddFileUploader = () => {
        setUploaderList(uploaderList.concat(<UU5.Forms.File
            className={Css.Link()}
            name={"file_" + generateFileId}
            controlled={false}
            colorSchema={colorSchema}
            borderRadius={borderRadius}
            placeholder="Upload"
            key={uniqid()} />));
    }
    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            <UU5.Bricks.Div>
                <UU5.Bricks.Div style={{ marginTop: "1.2rem" }} className={Css.Link()}>
                    <UU5.Bricks.Div >
                        {uploaderList}
                    </UU5.Bricks.Div>
                    <UU5.Bricks.Div style={{ width: "10rem", margin: "0 auto" }} >
                        <UU5.Bricks.Button id='buttonStyle' bgStyle="transparent" size='l' onClick={handleAddFileUploader} borderRadius={borderRadius} >
                            <UU5.Bricks.Icon icon="mdi-shape-circle-plus" />
                            <UU5.Bricks.Div>
                                {<UU5.Bricks.Lsi lsi={Lsi.uploadFile} />}
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Button>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Div>
            </UU5.Bricks.Div>
        </UU5.Forms.ContextForm>
    )
};
export { UploadFiles }
export default UploadFiles;
