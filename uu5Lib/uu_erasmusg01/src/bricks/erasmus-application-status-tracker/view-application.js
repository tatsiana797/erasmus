//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./erasmus-application-status-tracker-lsi"
import Css from "./erasmus-tracker.css"
import "uu_plus4u5g01-bricks";
//@@viewOff:imports

const ViewApplication = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "ViewApplication",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        application: UU5.PropTypes.object,
        shown: UU5.PropTypes.bool,
        baseUri: UU5.PropTypes.string,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        onSave: () => { },
        shown: false,
        application: undefined,
        baseUri: undefined,
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    render(props) {
        return (
            <UU5.Forms.ContextModal
                shown={props.shown}
                header={<Header />}
                footer={<Controls elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} bgStyle={props.bgStyle} />}
                onClose={props.onSave}
                size="l"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    baseUri={props.baseUri}
                    onSave={props.onSave}
                    application={props.application}
                    colorSchema={props.colorSchema}
                    elevation={props.elevation}
                    borderRadius={props.borderRadius}
                />
            </UU5.Forms.ContextModal>
        );
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return <UU5.Forms.ContextControls controlled={false} className={Css.Footer()} buttonSubmitProps={{ bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.viewApplication} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.infoViewApplication} />}
        />
    );
}

function Form({ onSave, application, baseUri, colorSchema, borderRadius, elevation }) {

    const other = application?.erasmus?.language.filter(item => item.name !== 'english' && item.name !== 'russian' && item.name !== 'german' && item.name !== 'french')
    const french = application?.erasmus?.language.filter(item => item.name == 'french')
    const german = application?.erasmus?.language?.filter(item => item.name == 'german')
    const russian = application?.erasmus?.language.filter(item => item.name == 'russian')
    const english = application?.erasmus?.language.filter(item => item.name == 'english')
    const destionation1 = application?.erasmus?.destionations[0].school !== undefined
    const destionation2 = application?.erasmus?.destionations[1].school !== undefined
    const destionation3 = application?.erasmus?.destionations[2].school !== undefined
    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            <UU5.Bricks.Tabs type="tabs" fade={true} justified colorSchema={colorSchema} elevation={elevation}>
                {/* Edirt Erasmus information */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.erasmusInfo} />} className={Css.Erasmus()}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className="languages" >
                            <UU5.Forms.Select
                                name="currentSemester"
                                label={<UU5.Bricks.Lsi lsi={Lsi.semester} />}
                                readOnly={true}
                                value={application?.erasmus?.currentSemester}
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                            >
                                {Object.keys(Config.SEMESTER).map(function (key, index) {
                                    return <UU5.Forms.Select.Option key={index} value={Config.SEMESTER[key]} />
                                })}
                            </UU5.Forms.Select>

                            <UU5.Forms.Select
                                name="semester"
                                label={<UU5.Bricks.Lsi lsi={Lsi.mobSemester} />}
                                controlled={false}
                                readOnly={true}
                                value={application?.erasmus?.semesterYear.match(/^(\S+)\s(.*)/).slice(1)}
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                            >
                                {["Winter", "Summer"].map(function (item, index) {
                                    return <UU5.Forms.Select.Option key={index} value={item} />
                                })}
                            </UU5.Forms.Select>
                            <UU5.Forms.DatePicker
                                label={<UU5.Bricks.Lsi lsi={Lsi.mobYear} />}
                                name="year"
                                readOnly={true}
                                value={application.erasmus?.semesterYear.split(' ').pop()}
                                step="years"
                                controlled={false}
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                            />
                            <UU5.Forms.Select
                                name="specialization"
                                readOnly={true}
                                label={<UU5.Bricks.Lsi lsi={Lsi.specialization} />}
                                controlled={false}
                                value={application.erasmus?.studyProgramme}
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                            >
                                <UU5.Forms.Select.Option key="ECONOM_MANAGEMENT" value="Economics and Management" content={<UU5.Bricks.Lsi lsi={Lsi.ECONOM_MANAGEMENT} />} />
                                <UU5.Forms.Select.Option key="IT" value="Information Technology" content={<UU5.Bricks.Lsi lsi={Lsi.IT} />} />
                                <UU5.Forms.Select.Option key="ICT_PROJECT_MANAGEMENT" value="ICT Project Management" content={<UU5.Bricks.Lsi lsi={Lsi.ICT_PROJECT_MANAGEMENT} />} />
                            </UU5.Forms.Select>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div className={Css.table()} >
                            <UU5.Bricks.Div id="table">
                                <UU5.Bricks.Div className='theader'>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.creadits} /></UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.approval} /></UU5.Bricks.Div>
                                </UU5.Bricks.Div>
                                {destionation1 && <UU5.Bricks.Div className='table_row'>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Select className={Css.Select()}
                                                value={application?.erasmus?.destionations[0]?.school}
                                                name="school1"
                                                controlled={false}
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            >
                                                {Config.UNIVERSITY.map((item, index) => {
                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                        content={item} />
                                                })
                                                }
                                            </UU5.Forms.Select></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                name="from1"
                                                valueType="iso"
                                                readOnly={true}
                                                value={application?.erasmus?.destionations[0]?.from}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={application.erasmus?.destionations[0].to}
                                                name="to1"
                                                valueType="iso"
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                name='credits1'
                                                readOnly={true}
                                                value={application?.erasmus?.destionations[0]?.numberOfCreadits}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.approval} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                readOnly={true}
                                                value={application?.erasmus?.destionations[0]?.universityApprovalStatus}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                                {destionation2 && <UU5.Bricks.Div className='table_row'>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Select className={Css.Select()}
                                                value={application?.erasmus?.destionations[1]?.school}
                                                name="school2"
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            >
                                                {Config.UNIVERSITY.map((item, index) => {
                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                        content={item} />
                                                })
                                                }
                                            </UU5.Forms.Select></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                name="from2"
                                                valueType="iso"
                                                value={application?.erasmus?.destionations[1]?.from}
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={application?.erasmus?.destionations[1]?.to}
                                                name="to2"
                                                readOnly={true}
                                                valueType="iso"
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                value={application?.erasmus?.destionations[1]?.numberOfCreadits}
                                                name='credits2'
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.approval} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                readOnly={true}
                                                value={application?.erasmus?.destionations[1]?.universityApprovalStatus}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                                {destionation3 && <UU5.Bricks.Div className='table_row'>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Select className={Css.Select()}
                                                value={application?.erasmus?.destionations[2]?.school}
                                                name="school3"
                                                controlled={false}
                                                readOnly={true}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            >
                                                {Config.UNIVERSITY.map((item, index) => {
                                                    return <UU5.Forms.Select.Option key={index} value={item}
                                                        content={item} />
                                                })
                                                }
                                            </UU5.Forms.Select></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={application?.erasmus?.destionations[2]?.from}
                                                name="from3"
                                                valueType="iso"
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.DatePicker className={Css.Date()}
                                                value={application?.erasmus?.destionations[2]?.to}
                                                name="to3"
                                                valueType="iso"
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                value={application?.erasmus?.destionations[2]?.numberOfCreadits}
                                                name='credits3'
                                                readOnly={true}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_small'>
                                        <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.approval} /></UU5.Bricks.Div>
                                        <UU5.Bricks.Div className='table_cell'>
                                            <UU5.Forms.Text className={Css.Number()}
                                                readOnly={true}
                                                value={application?.erasmus?.destionations[2]?.universityApprovalStatus}
                                                controlled={false}
                                                colorSchema={colorSchema}
                                                borderRadius={borderRadius}
                                            /></UU5.Bricks.Div>
                                    </UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div style={{ margin: '1rem 0' }}>
                                <UU5.Forms.TextArea
                                    label={<UU5.Bricks.Lsi lsi={Lsi.motivation} />}
                                    placeholder='Insert text here.'
                                    message=""
                                    value={application?.erasmus?.motivationDescription}
                                    readOnly={true}
                                    name="motivationDescription"
                                    controlled={false}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                />
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className="languages" >
                                {english[0]?.level !== undefined && <UU5.Bricks.Div >
                                    <UU5.Forms.Select
                                        style={{ margin: "0px" }}
                                        readOnly={true}
                                        name="english"
                                        label={<UU5.Bricks.Lsi lsi={Lsi.english} />}
                                        value={english[0]?.level}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {german[0]?.level !== undefined && <UU5.Bricks.Div >
                                    <UU5.Forms.Select
                                        readOnly={true}
                                        style={{ margin: "0px" }}
                                        name="german"
                                        label={<UU5.Bricks.Lsi lsi={Lsi.german} />}
                                        value={german[0]?.level}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {french[0]?.level !== undefined && <UU5.Bricks.Div >
                                    <UU5.Forms.Select
                                        style={{ margin: "0px" }}
                                        name='french'
                                        readOnly={true}
                                        label={<UU5.Bricks.Lsi lsi={Lsi.french} />}
                                        value={french[0]?.level}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {russian[0]?.level !== undefined && <UU5.Bricks.Div>
                                    <UU5.Forms.Select
                                        label={<UU5.Bricks.Lsi lsi={Lsi.russian} />}
                                        readOnly={true}
                                        style={{ margin: "0px" }}
                                        name="russian"
                                        value={russian[0]?.level}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                    >
                                        {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return <UU5.Forms.Select.Option key={index} value={key}
                                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                        })
                                        }
                                    </UU5.Forms.Select>
                                </UU5.Bricks.Div>}
                                {other.length !== 0 && other?.map((item, index) => {
                                    return (<UU5.Bricks.Div key={index}>
                                        <UU5.Forms.Select
                                            label={item.name}
                                            style={{ margin: "0px" }}
                                            name={"other" + `${index}`}
                                            readOnly={true}
                                            value={item.level}
                                            colorSchema={colorSchema}
                                            borderRadius={borderRadius}
                                        >
                                            {Object.keys(Config.LANGUAGE).map(function (key, index) {
                                                return <UU5.Forms.Select.Option key={index} value={key}
                                                    content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />} />
                                            })
                                            }
                                        </UU5.Forms.Select>
                                    </UU5.Bricks.Div>)
                                })}
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Edirt Erasmus information */}


                {/* Edit Personal info */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.personalInfo} />}>
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className={Css.PersonalInfo()}>
                            <UU5.Bricks.Div className="languages">
                                <UU5.Forms.DatePicker
                                    name="dateOfBirth"
                                    label="Date of birth"
                                    valueType="iso"
                                    readOnly={true}
                                    placeholder={UU5.Common.Tools.getDateString("1990-11-21", { country: "cs-cz" })}
                                    value={application?.erasmus?.personalInfo?.dateOfBirth}
                                    controlled={false}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                />
                                <UU5.Forms.Text
                                    name="nationality"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.nationality} />}
                                    readOnly={true}
                                    value={application?.erasmus?.personalInfo?.nationality}
                                    controlled={false}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                />
                                <UU5.Forms.Text
                                    name="email"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.email} />}
                                    value={application?.erasmus?.personalInfo?.email}
                                    controlled={false}
                                    readOnly={true}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    onBlur={(opt) => {
                                        opt.component.setValue(opt.value);
                                        if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(opt.value) && opt.value.length > 0) {
                                            opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.invalidEmail} />);
                                        }
                                    }}
                                />
                                <UU5.Forms.Text
                                    name="phone"
                                    label="Phone number"
                                    controlled={false}
                                    placeholder="+420xxxxxxxxx"
                                    value={application?.erasmus?.personalInfo?.phone}
                                    readOnly={true}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    onBlur={(opt) => {
                                        opt.component.setValue(opt.value);
                                        if (!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(opt.value) && opt.value.length > 0) {
                                            opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.invalidPhone} />);
                                        }
                                    }}
                                />
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className={Css.table()} id="enterLanguageDiv" style={{ margin: '1rem 0' }}>
                                <UU5.Forms.TextArea
                                    name="adress"
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    label={<UU5.Bricks.Lsi lsi={Lsi.adress} />}
                                    controlled={false}
                                    value={application?.erasmus?.personalInfo?.adress}
                                    readOnly={true}
                                />
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className="languages" >
                                <UU5.Forms.Text
                                    name="banckAccount"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.bankAccount} />}
                                    controlled={false}
                                    value={application?.erasmus?.personalInfo?.banckAccount}
                                    readOnly={true}
                                />
                                <UU5.Forms.Text
                                    name="bankName"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.bankName} />}
                                    controlled={false}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                    value={application?.erasmus?.personalInfo?.bankName}
                                    readOnly={true}
                                />
                                <UU5.Forms.Text
                                    name="swift"
                                    label={<UU5.Bricks.Lsi lsi={Lsi.swift} />}
                                    controlled={false}
                                    value={application?.erasmus?.personalInfo?.swift}
                                    readOnly={true}
                                    colorSchema={colorSchema}
                                    borderRadius={borderRadius}
                                />
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Edit Personal info */}


                {/* Subjects recognition */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.subjects} />} >
                    <UU5.Bricks.Div style={{ marginTop: '2rem' }}>
                        <UU5.Bricks.Div className={Css.table()}>
                            <UU5.Bricks.Div id='tableSubjects' >
                                {application?.erasmus?.subjectsRecognition && <UU5.Bricks.Div className='theader'>
                                    <UU5.Bricks.Div className='table_header'>{<UU5.Bricks.Lsi lsi={Lsi.uuSubjectName} />}</UU5.Bricks.Div>
                                    <UU5.Bricks.Div className='table_header'>{<UU5.Bricks.Lsi lsi={Lsi.partnerUniName} />}</UU5.Bricks.Div>
                                </UU5.Bricks.Div>}
                                {application?.erasmus?.subjectsRecognition ? application?.erasmus?.subjectsRecognition?.map((item, index) => {
                                    return (
                                        <UU5.Bricks.Div className='table_row' key={index}>
                                            <UU5.Bricks.Div className='table_small'>
                                                <UU5.Bricks.Div className='table_cell'>{<UU5.Bricks.Lsi lsi={Lsi.uuSubjectName} />}</UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_cell'>
                                                    <UU5.Forms.Text style={{ marginTop: 0, marginBottom: "0.3rem" }}
                                                        name={"subjectUcl" + `${item}`}
                                                        controlled={false}
                                                        value={item?.subjectNameUcl || ""}
                                                        readOnly={true}
                                                        colorSchema={colorSchema}
                                                        borderRadius={borderRadius}
                                                    /></UU5.Bricks.Div>
                                            </UU5.Bricks.Div>
                                            <UU5.Bricks.Div className='table_small'>
                                                <UU5.Bricks.Div className='table_cell'>{<UU5.Bricks.Lsi lsi={Lsi.partnerUniName} />}</UU5.Bricks.Div>
                                                <UU5.Bricks.Div className='table_cell'>
                                                    <UU5.Forms.Text style={{ marginTop: 0, marginBottom: "0.3rem" }}
                                                        name={"subject" + `${item}`}
                                                        readOnly={true}
                                                        value={item?.subjectNameForeignUni || ""}
                                                        controlled={false}
                                                        colorSchema={colorSchema}
                                                        borderRadius={borderRadius}
                                                    /></UU5.Bricks.Div>
                                            </UU5.Bricks.Div>

                                        </UU5.Bricks.Div>)
                                }) : <UU5.Bricks.Div><UU5.Bricks.Lsi lsi={Lsi.recognitionNotCreated} /></UU5.Bricks.Div>}
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Subjects recognition */}


                {/* Attachments */}
                <UU5.Bricks.Tabs.Item header={<UU5.Bricks.Lsi lsi={Lsi.files} />} className={Css.Attachments()} >
                    <UU5.Bricks.Div id='attachments'>
                        <UU5.Bricks.Div style={{ float: 'left', marginRight: '0.4rem' }}><UU5.Bricks.Lsi lsi={Lsi.resume} /></UU5.Bricks.Div>
                        <UU5.Bricks.Div >
                            <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${application.erasmus?.attachments.cv.code}`}>
                                {application.erasmus?.attachments.cv.name}</UU5.Bricks.Link>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div style={{ float: 'left', marginRight: '0.4rem' }}><UU5.Bricks.Lsi lsi={Lsi.ml} /></UU5.Bricks.Div>
                        <UU5.Bricks.Div >
                            <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${application.erasmus?.attachments.cv.code}`}>
                                {application.erasmus?.attachments.cv.name}</UU5.Bricks.Link>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div ><UU5.Bricks.Lsi lsi={Lsi.syllables} /></UU5.Bricks.Div>
                        {application.erasmus?.attachments.syllabus.map((item, index) => {
                            return (
                                <UU5.Bricks.Div key={index * 0.9} >
                                    <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${item.code}`}>
                                        {item.name}</UU5.Bricks.Link>
                                </UU5.Bricks.Div>
                            )
                        })}
                        <UU5.Bricks.Div ><UU5.Bricks.Lsi lsi={Lsi.extraAttachments} /></UU5.Bricks.Div>
                        <UU5.Bricks.Div id="unique">
                            {application.erasmus?.attachments.file.map((item, index) => {
                                return (
                                    <UU5.Bricks.Div key={index}>
                                        <UU5.Bricks.Link target="_blank" colorSchema={colorSchema} href={baseUri + 'application/getAttachment?code=' + `${item.code}`}>
                                            {item.name}</UU5.Bricks.Link>
                                    </UU5.Bricks.Div>
                                )
                            })}
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Tabs.Item>
                {/* Attachments */}
            </UU5.Bricks.Tabs>


        </UU5.Forms.ContextForm>
    )
};
export { ViewApplication }
export default ViewApplication;
