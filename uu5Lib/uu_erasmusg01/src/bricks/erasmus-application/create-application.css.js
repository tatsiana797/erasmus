import Config from "../config/config";

const table = () => Config.Css.css`
#tableCenter {
margin:0 auto;
 width: fit-content;
}
.table {
    display: table;
    text-align: center;
    width: 100%;
    margin: 10% auto 0;
    border-collapse: separate;
    font-weight: 400;
  }
  
  .table_row {
    display: table-row;
     
  }
  
  .theader {
    display: table-row;
  }
  
  .table_header {
    display: table-cell;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #0000008a;
  }
 
  
  .table_small {
    display: table-cell;
  }
  
  .table_row > .table_small > .table_cell:nth-child(odd) {
    display: none;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  
  .table_row > .table_small > .table_cell {
    padding-top: 3px;
    padding-bottom: 3px;
    color: #5b5b5b;
  }
  
  @media screen and (max-width: 900px) {
    .table {
      width: 90%
    }
  }
  
  @media screen and (max-width: 650px) {
    .table {
      display: block;
    }
    .table_row:nth-child(2n+3) {
      background: none;
    }
    .theader {
      display: none;
    }
    .table_row > .table_small > .table_cell:nth-child(odd) {
      display: inherit;
      width: 50%;
    }
    .table_cell {
      display: table-cell;
      width: 50%;
      font-size: 12px;
    }
    .table_row {
      display: table;
      width: 100%;
      border-collapse: separate;
      padding-bottom: 20px;
      margin: 5% auto 0;
      text-align: center;
       border-bottom: 1px solid #e0e0e0;
    }
    .table_small {
      display: table-row;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-left: none;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-right: none;
    }
  }

`
const Select = () => Config.Css.css`
box-sizing-border-box;
.uu5-forms-items-input{
  height: 32px;
  display: grid;
  overflow: hidden;
}
margin-top: 0rem !important;
margin-bottom: 0rem !important;
max-width: 100%;
padding-right: 0.5rem;
 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Number = () => Config.Css.css`
box-sizing-border-box;
padding-right: 0.5rem;
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
  max-width: 100%;
  width: 4rem;
  @media screen and (max-width: 650px) {
    max-width: 100%;
 }
  `
const Date = () => Config.Css.css`
box-sizing-border-box;
padding-right: 0.5rem;
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
  max-width: 100%;
 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Language = () => Config.Css.css`
box-sizing-border-box;
padding-right: 0.5rem;
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
  max-width: 100%;
 @media screen and (max-width: 650px) {
  max-width: 100%;
 }
  `
const Modal = () => Config.Css.css`
  .uu5-bricks-modal-layout-wrapper {
     display: block;
     width: 98.4%;
   }

   .input {
     height: 3.4rem;
   }
    `
const StepOne = () => Config.Css.css`
    width: 100%;
    #centerInput {
       width: 12rem ;
       margin: 0 auto ;
       margin-top: 2rem ;
    }
    #centerInputStep3 {
      width: 12rem ;
      margin: 0 auto ;
      margin-top: 2rem ;
    }
    #enterLanguageDiv {
      width: 35rem;
       margin: 0 auto;
    }
    .buttonStyle {
      float: left;
       margin-right: 0.3rem;
    }
#otheLangDiv {
  float: left;
  min-width: 46.5%;
}
#language0 {
  float: left;
   display: none;
}
#centerFilesDiv {
  width: 12rem;
   margin: 0 auto;
}


@media screen and (max-width: 653px) {
  .table {
    width: 90%
  }
  .buttonStyle {
    float: unset;
     margin-right: 0.3rem;
  }
#otheLangDiv {
float: unset;
.uu5-forms-select{
  display:contents;
}
}
#language0 {
float: unset;
}

#centerInput {
  width: auto ;
}
#centerInputStep3 {
 width: auto ;
}
#centerFilesDiv{
  width: auto ;
}

#enterLanguageDiv {
  width: auto ;
  margin: auto;

  .uu5-forms-select {
    width: auto !important;
  }
}
}
      `
const StepTwo = () => Config.Css.css`
 #item{
  margin-left: 5.5rem;
 }

#selectDiv {
  float:  left;
}

.other {
  float: left;
  display:none;
}
      @media screen and (max-width: 653px) {
      #language {
        float: unset;
        display:none;
      }
      #selectDiv{
        float:  unset;
      }
      #item{
        margin-left: unset;
      }
      .other {
        float: unset;
      }
      }

    `
const ProgressIndicator = () => Config.Css.css`
    .bx--progress-line {
      height: 3px;
    }
      .bx--progress-label:focus {
        box-shadow: 0 0.1875rem  0 0 #161616!important;
        color: #161616!important;
  }
  .bx--progress-step-button--unclickable .bx--tooltip__label:hover {
    box-shadow: 0 0.0625rem 0 0 #161616!important;
    color: #161616!important;
  }
  .bx--progress-step-button:not(.bx--progress-step-button--unclickable) .bx--progress-label:active {
    box-shadow: 0 0.05 0 0 #161616!important;
    color: #161616!important;
}
.bx--progress-label:hover {
  box-shadow: 0 0.0625rem 0 0 #161616!important;
  color: #161616!important;
}
.bx--progress-step--complete {
  svg{
    fill: #161616!important;
  }

}
    `

export default {
  table,
  Select,
  Number,
  Date,
  Language,
  Modal,
  StepOne,
  StepTwo,
  ProgressIndicator
}