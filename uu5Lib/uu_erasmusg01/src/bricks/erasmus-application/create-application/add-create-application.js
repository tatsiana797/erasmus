//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef, useState, useCallback, useEffect } from "uu5g04-hooks";
import "uu5g04-forms";
import Lsi from "./create-application-lsi"
import Css from "../create-application.css"
import { ProgressIndicator, ProgressStep, Tooltip } from 'carbon-components-react';
import Config from "./config/config";
import { useLocalStorage } from './use-local-storage';
import 'carbon-components/css/carbon-components.min.css';
//@@viewOff:imports

const AddCreateApplication = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "AddCreateApplication",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        shown: UU5.PropTypes.bool,
        stepIndex: UU5.PropTypes.number,
        onCancel: UU5.PropTypes.func,
        onSave: UU5.PropTypes.func,
        onSaveDone: UU5.PropTypes.func,
        onSaveFail: UU5.PropTypes.func,
        onChangeStudentFiles: UU5.PropTypes.func,
        application: UU5.PropTypes.object,
        handleOnChangeStep: UU5.PropTypes.func,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        shown: false,
        stepIndex: 0,
        onCancel: () => { },
        onSave: () => { },
        onSaveDone: () => { },
        onSaveFail: () => { },
        onChangeStudentFiles: () => { },
        application: undefined,
        handleOnChangeStep: () => { },
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {

        return (
            <UU5.Forms.ContextModal
                shown={props.shown}
                header={<Header />}
                footer={props.stepIndex == 2 ? <Controls bgStyle={props.bgStyle} elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} /> :
                    <ControlsStepper bgStyle={props.bgStyle} borderRadius={props.borderRadius} colorSchema={props.colorSchema} elevation={props.elevation} />}
                onClose={props.onCancel && localStorage.clear()}
                size="l"
                mountContent="onFirstOpen"
                overflow
                forceRender
            >
                <Form
                    borderRadius={props.borderRadius}
                    colorSchema={props.colorSchema}
                    stepIndex={props.stepIndex}
                    onSave={props.onSave}
                    onSaveDone={props.onSaveDone}
                    onSaveFail={props.onSaveFail}
                    onCancel={props.onCancel}
                    application={props.application}
                    onChangeStudentFiles={props.onChangeStudentFiles}
                    handleOnChangeStep={props.handleOnChangeStep}
                />
            </UU5.Forms.ContextModal>
        );
    }
});



function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return <UU5.Forms.ContextControls controlled={false} buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.submit} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
}

function ControlsStepper({ bgStyle, colorSchema, elevation, borderRadius }) {
    return <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }} controlled={false} buttonSubmitProps={{ content: <UU5.Bricks.Lsi lsi={Lsi.save} />, bgStyle: bgStyle, colorSchema: colorSchema, elevation: elevation, borderRadius: borderRadius }} />
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.headerForm} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.info} />}
        />
    );
}

function Form({ onSave, onCancel, application, onSaveDone, onSaveFail, stepIndex, onChangeStudentFiles, handleOnChangeStep, colorSchema, borderRadius }) {

    const currentYear = new Date().getFullYear().toString()
    const today = new Date();
    let [languageList, setLanguageList] = useState([])
    let [languageCount, setLanguageCount] = useState(0)
    const [currentSemester, setCurrentSemester] = useLocalStorage('currentSemester', currentSemester)
    const [semester, setSemester] = useLocalStorage("semester", semester)
    const [year, setYear] = useLocalStorage("year", year)
    const [cv, setCv] = useLocalStorage("cv", cv)
    const [ml, setMl] = useLocalStorage("ml", ml)
    const [language0, setLanguage0] = useLocalStorage("language0", language0)
    const [otherLanguageName0, setOtherLanguageName0] = useLocalStorage("otherLanguageName0", otherLanguageName0)
    const [level0, setLevel0] = useLocalStorage("level0", level0)
    const [school1, setSchool1] = useLocalStorage("school1", school1)
    const [school2, setSchool2] = useLocalStorage("school2", school2)
    const [school3, setSchool3] = useLocalStorage("school3", school3)
    const [motivationDescription, setMotivationDescription] = useLocalStorage("motivationDescription", motivationDescription)
    const [from1, setFrom1] = useLocalStorage("from1", from1)
    const [from2, setFrom2] = useLocalStorage("from2", from2)
    const [from3, setFrom3] = useLocalStorage("from3", from3)
    const [to1, setTo1] = useLocalStorage("to1", to1)
    const [to2, setTo2] = useLocalStorage("to2", to2)
    const [to3, setTo3] = useLocalStorage("to3", to3)
    const [credits1, setCredits1] = useLocalStorage("credits1", credits1)
    const [credits2, setCredits2] = useLocalStorage("credits2", credits2)
    const [credits3, setCredits3] = useLocalStorage("credits3", credits3)
    const [swift, setSwift] = useLocalStorage("swift", swift)
    const [bankName, setBankName] = useLocalStorage("bankName", bankName)
    const [adress, setAdress] = useLocalStorage("adress", adress)
    const [banckAccount, setBanckAccount] = useLocalStorage("banckAccount", banckAccount)
    const [phone, setPhone] = useLocalStorage("phone", phone)
    const [dateOfBirth, setDateOfBirth] = useLocalStorage("dateOfBirth", dateOfBirth)
    const [nationality, setNationality] = useLocalStorage("nationality", nationality)
    const [email, setEmail] = useLocalStorage("email", email)
    const [studyProgram, setStudyProgram] = useLocalStorage("specialization", studyProgram)
    const [disabledStep2, setDisabledStep2] = useState(true)
    const [disabledStep3, setDisabledStep3] = useState(true)

    const handleClickRemove = () => {
        languageList = languageList.filter((item, i) => {
            return languageList.length - 1 !== i
        })
        setLanguageList(languageList)
    }

    useEffect(() => {
        const color = Config.COLORS.find(color => {
            if (color.color === colorSchema) {
                return color
            } else {
                return color.color === 'default'
            }
        }).code
        const indicator = document.getElementsByClassName('bx--progress-step--current')[0]
        const indicatorLine = indicator.getElementsByClassName('bx--progress-line ')[0]
        indicatorLine.style.backgroundColor = color
        const indicatorSvg = indicator.querySelector('svg')
        indicatorSvg.style.fill = color
    })

    useEffect(() => {
        typeof application?.erasmus?.semesterYear == "undefined" ?
            setDisabledStep2(true) : typeof application?.erasmus?.semesterYear != "undefined" ? setDisabledStep2(false) : false
        application.erasmus?.destionations?.length ? setDisabledStep3(false) : true

    }, [application?.erasmus])

    const handleClickAdd = useCallback(() => {
        setLanguageCount(languageCount += 1)
        setLanguageList(languageList?.concat(<UU5.Bricks.Div className={Css.StepTwo()} id={"item" + `${languageCount}`}  >
            <UU5.Bricks.Div id='item'>
                <UU5.Bricks.Div id='selectDiv' >
                    <UU5.Forms.Select className={Css.Language()}
                        colorSchema={colorSchema}
                        borderRadius={borderRadius}
                        style={{ margin: "0px" }}
                        name={"language" + `${languageCount}`}
                        label={<UU5.Bricks.Lsi lsi={Lsi.headerForm} />}
                        placeholder={<UU5.Bricks.Lsi lsi={Lsi.placehLanguage} />}
                        controlled={false}
                        onChange={(opt) => {
                            if (opt.value == "other") {
                                const element = document.getElementById("language" + `${languageCount}`)
                                element.style.display = "block";
                                opt.component.setValue(opt.value);
                            } else {
                                const element = document.getElementById("language" + `${languageCount}`)
                                element.style.display = "none";
                                opt.component.setValue(opt.value);
                            }
                        }}
                    >
                        {Object.keys(Config.LANGUAGE_LIST).map(function (key, index) {
                            return <UU5.Forms.Select.Option key={index} value={key}
                                content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE_LIST[key].cs, en: Config.LANGUAGE_LIST[key].en }} />} />
                        })
                        }
                    </UU5.Forms.Select>
                </UU5.Bricks.Div>
                <UU5.Bricks.Div id={"language" + `${languageCount}`} className='other' >
                    <UU5.Forms.Text className={Css.Date()}
                        colorSchema={colorSchema}
                        borderRadius={borderRadius}
                        controlled={false}
                        name={"otherLanguageName" + `${languageCount}`}
                        label={<UU5.Bricks.Lsi lsi={Lsi.langName} />}
                        style={{ width: "7rem", marginTop: 0, marginRight: "0.4rem" }}
                    >
                    </UU5.Forms.Text>
                </UU5.Bricks.Div>
                <UU5.Forms.Radios
                    colorSchema={colorSchema}
                    controlled={false}
                    label={<UU5.Bricks.Lsi lsi={Lsi.pickLangLevel} />}
                    name={"level" + `${languageCount}`}
                    value={Object.keys(Config.LANGUAGE).map(function (key, index) {
                        return { name: key, label: <UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} /> }
                    })
                    }
                />
            </UU5.Bricks.Div>
        </UU5.Bricks.Div>)
        )
    }, [languageList])

    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onSaveDone={onSaveDone}
            onSaveFail={onSaveFail}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            <UU5.Bricks.Div className={Css.ProgressIndicator("yellow")}>
                <ProgressIndicator currentIndex={stepIndex}
                    spaceEqually={true}
                    onChange={(el) => handleOnChangeStep(el)}
                >
                    <ProgressStep
                        label=""
                        renderLabel={() => (
                            <Tooltip
                                direction="bottom"
                                showIcon={false}
                                triggerText={'First step'}
                                triggerClassName="bx--progress-label"
                                tooltipId="tooltipId-0">
                                <p>
                                    {<UU5.Bricks.Lsi lsi={Lsi.helper1} />}
                                </p>
                            </Tooltip>
                        )}
                    />
                    <ProgressStep
                        label=""
                        renderLabel={() => (
                            <Tooltip
                                direction="bottom"
                                showIcon={false}
                                triggerText={'Second step'}
                                triggerClassName="bx--progress-label"
                                tooltipId="tooltipId-1">
                                <p>
                                    {<UU5.Bricks.Lsi lsi={Lsi.helper1} />}
                                </p>
                            </Tooltip>
                        )}
                    />
                    <ProgressStep
                        label=""
                        renderLabel={() => (
                            <Tooltip
                                direction="bottom"
                                showIcon={false}
                                triggerText={'Third step'}
                                triggerClassName="bx--progress-label"
                                tooltipId="tooltipId-2">
                                <p>
                                    {<UU5.Bricks.Lsi lsi={Lsi.helper1} />}
                                </p>
                            </Tooltip>
                        )}
                    />
                </ProgressIndicator>
            </UU5.Bricks.Div>
            {/* Step One */}
            {stepIndex == 0 && (
                <UU5.Bricks.Div className={Css.StepOne()} >
                    <UU5.Bricks.Div id='centerInput' >
                        <UU5.Forms.Select
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            name="currentSemester"
                            controlled={false}
                            value={currentSemester}
                            required
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setCurrentSemester(opt.value)
                            }}
                            label={<UU5.Bricks.Lsi lsi={Lsi.semester} />}
                        >
                            {Object.keys(Config.SEMESTER).map(function (key, index) {
                                return <UU5.Forms.Select.Option key={index} value={Config.SEMESTER[key]} />
                            })}
                        </UU5.Forms.Select>

                        <UU5.Forms.Select
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            name="semester"
                            value={semester}
                            required
                            label={<UU5.Bricks.Lsi lsi={Lsi.mobSemester} />}
                            controlled={false}
                            onChange={(opt) => {
                                setSemester(opt.value)
                                opt.component.setValue(opt.value)
                            }}
                        >
                            {["Winter", "Summer"].map(function (item, index) {
                                return <UU5.Forms.Select.Option key={index} value={item} />
                            })}
                        </UU5.Forms.Select>

                        <UU5.Forms.DatePicker
                            label={<UU5.Bricks.Lsi lsi={Lsi.mobYear} />}
                            dateFrom={currentYear}
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            name="year"
                            required
                            value={year}
                            step="years"
                            controlled={false}
                            onChange={(opt) => {
                                setYear(opt.value)
                                opt.component.setValue(opt.value)
                            }}
                        />
                    </UU5.Bricks.Div>
                    <UU5.Bricks.Div id='enterLanguageDiv'  >
                        <UU5.Bricks.Div >
                            <UU5.Bricks.Button className='buttonStyle' onClick={handleClickAdd} bgStyle="transparent" size='l' >
                                <UU5.Bricks.Icon icon="mdi-shape-circle-plus" />
                            </UU5.Bricks.Button>
                            <UU5.Bricks.Button className='buttonStyle' onClick={handleClickRemove} bgStyle="transparent" size='l'  >
                                <UU5.Bricks.Icon icon="mdi-minus" />
                            </UU5.Bricks.Button>
                            <UU5.Bricks.Div >
                                <UU5.Bricks.Div >
                                    <UU5.Bricks.Div id='otheLangDiv' className={Css.StepTwo()} >
                                        <UU5.Forms.Select className={Css.Language()}
                                            style={{ margin: "0px" }}
                                            name="language0"
                                            colorSchema={colorSchema}

                                            borderRadius={borderRadius}
                                            required
                                            controlled={false}
                                            label={<UU5.Bricks.Lsi lsi={Lsi.pickLanguage} />}
                                            value={language0}
                                            placeholder={<UU5.Bricks.Lsi lsi={Lsi.placehLanguage} />}
                                            onChange={(opt) => {
                                                setLanguage0(opt.value)
                                                if (opt.value == "other") {
                                                    const element = document.getElementById("language0")
                                                    element.style.display = "block";
                                                    opt.component.setValue(opt.value);

                                                } else {
                                                    const element = document.getElementById("language0")
                                                    element.style.display = "none";
                                                    opt.component.setValue(opt.value);

                                                }
                                            }}
                                        >
                                            {Object.keys(Config.LANGUAGE_LIST).map(function (key, index) {
                                                return <UU5.Forms.Select.Option key={index} value={key}
                                                    content={<UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE_LIST[key].cs, en: Config.LANGUAGE_LIST[key].en }} />} />
                                            })
                                            }
                                        </UU5.Forms.Select>
                                    </UU5.Bricks.Div>
                                    <UU5.Bricks.Div id="language0" style={{ display: language0 === 'other' ? 'block' : "none", marginRight: "0.4rem" }}>
                                        <UU5.Forms.Text
                                            colorSchema={colorSchema}

                                            borderRadius={borderRadius}
                                            name="otherLanguageName0"
                                            label={<UU5.Bricks.Lsi lsi={Lsi.langName} />}
                                            value={otherLanguageName0}
                                            controlled={false}
                                            style={{ width: "7rem", marginTop: 0 }}
                                            onChange={(opt) => {
                                                opt.component.setValue(opt.value)
                                                setOtherLanguageName0(opt.value)
                                            }}
                                        >
                                        </UU5.Forms.Text>
                                    </UU5.Bricks.Div>
                                    <UU5.Forms.Radios
                                        colorSchema={colorSchema}
                                        controlled={false}
                                        label={<UU5.Bricks.Lsi lsi={Lsi.pickLangLevel} />}
                                        required
                                        onChange={(opt) => {
                                            setLevel0(opt.value)
                                            opt.component.setValue(opt.value)
                                        }}
                                        name="level0"
                                        value={Object.keys(Config.LANGUAGE).map(function (key, index) {
                                            return { name: key, label: <UU5.Bricks.Lsi lsi={{ cs: Config.LANGUAGE[key].cs, en: Config.LANGUAGE[key].en }} />, value: level0 }
                                        })
                                        }
                                    />
                                </UU5.Bricks.Div>
                                {languageList}
                            </UU5.Bricks.Div>

                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div id='centerFilesDiv'>
                            <UU5.Forms.Select
                                colorSchema={colorSchema}

                                borderRadius={borderRadius}
                                name="specialization"
                                required
                                label={<UU5.Bricks.Lsi lsi={Lsi.specialization} />}
                                controlled={false}
                                onChange={(opt) => {
                                    setStudyProgram(opt.value)
                                    opt.component.setValue(opt.value)
                                }}
                                value={studyProgram}
                            >
                                {Object.keys(Config.SPECIALIZATION).map(function (key, index) {
                                    return <UU5.Forms.Select.Option key={index} value={Config.SPECIALIZATION[key]} />
                                })}
                            </UU5.Forms.Select>
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>

                    <UU5.Bricks.Div id='centerFilesDiv'>
                        <UU5.Forms.File
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            name="cv"
                            label={<UU5.Bricks.Lsi lsi={Lsi.cv} />}
                            controlled={false}
                            required
                            value={cv}
                            onChange={(opt) => {
                                setCv(opt.value)
                                opt.component.setValue(opt.value)
                                onChangeStudentFiles(opt, "cv")
                            }}
                        />
                        <UU5.Forms.File
                            name="ml"
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            label={<UU5.Bricks.Lsi lsi={Lsi.ml} />}
                            controlled={false}
                            value={ml}
                            required
                            onChange={(opt) => {
                                setMl(opt.value)
                                opt.component.setValue(opt.value)
                                onChangeStudentFiles(opt, "ml")
                            }} />
                    </UU5.Bricks.Div>
                </UU5.Bricks.Div>
            )}
            {/* Step One */}

            {/* Step Two */}
            {stepIndex === 1 && (
                <UU5.Bricks.Div className={Css.table()} style={{ marginTop: "2rem" }} >
                    <UU5.Bricks.Div id='tableCenter' >
                        <UU5.Bricks.Div className='theader'>
                            <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.credits} /></UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div className='table_row'>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Select className={Css.Select()}
                                        name="school1"
                                        controlled={false}
                                        required
                                        colorSchema={colorSchema}

                                        borderRadius={borderRadius}
                                        placeholder='Name of the partner school'
                                        value={school1}
                                        disabled={disabledStep2}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            if (opt.value !== "not selected" && (opt.value === school2 || opt.value === school3)) {
                                                opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.schoolExist} />)
                                            } else {
                                                setSchool1(opt.value)
                                            }
                                        }}
                                    >
                                        {Config.UNIVERSITY.map((item, index) => {
                                            return <UU5.Forms.Select.Option key={index} value={item}
                                                content={item} />
                                        })}
                                    </UU5.Forms.Select></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.DatePicker className={Css.Date()}
                                        name="from1"
                                        dateFrom={today}
                                        valueType="iso"
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        required
                                        disabled={disabledStep2}
                                        value={from1}
                                        controlled={false}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            setFrom1(opt.value)
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.DatePicker className={Css.Date()}
                                        name="to1"
                                        disabled={disabledStep2}
                                        valueType="iso"
                                        value={to1}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        required
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            if (opt.value <= from1) {
                                                opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.dateInvalid} />)
                                            } else {
                                                setTo1(opt.value)
                                            }
                                        }} /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.credits} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Text className={Css.Number()}
                                        name='credits1'
                                        disabled={disabledStep2}
                                        value={credits1}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        required
                                        controlled={false}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            setCredits1(opt.value)
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div className='table_row'>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Select className={Css.Select()}
                                        name="school2"
                                        disabled={disabledStep2}
                                        controlled={false}
                                        placeholder='Name of the partner school'
                                        value={school2}
                                        colorSchema={colorSchema}

                                        borderRadius={borderRadius}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            if (opt.value !== "not selected" && (opt.value === school1 || opt.value === school3)) {
                                                opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.schoolExist} />)
                                            } else {
                                                setSchool2(opt.value)
                                            }
                                        }}
                                    >
                                        {Config.UNIVERSITY.map((item, index) => {
                                            return <UU5.Forms.Select.Option key={index} value={item}
                                                content={item} />
                                        })}
                                    </UU5.Forms.Select></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.DatePicker className={Css.Date()}
                                        name="from2"
                                        dateFrom={today}
                                        valueType="iso"
                                        disabled={disabledStep2}
                                        value={from2}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            setFrom2(opt.value)
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.DatePicker className={Css.Date()}
                                        name="to2"
                                        valueType="iso"
                                        disabled={disabledStep2}
                                        value={to2}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            if (opt.value <= from2) {
                                                opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.dateInvalid} />)
                                            } else {
                                                setTo2(opt.value)
                                            }
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.credits} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Text className={Css.Number()}
                                        name='credits2'
                                        value={credits2}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        disabled={disabledStep2}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            setCredits2(opt.value)
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div className='table_row'>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.school} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Select className={Css.Select()}
                                        name="school3"
                                        controlled={false}
                                        placeholder='Name of the partner school'
                                        disabled={disabledStep2}
                                        value={school3}
                                        colorSchema={colorSchema}

                                        borderRadius={borderRadius}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            if (opt.value !== "not selected" && (opt.value === school1 || opt.value === school2)) {
                                                opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.schoolExist} />)
                                            } else {
                                                setSchool3(opt.value)
                                            }
                                        }}
                                    >
                                        {Config.UNIVERSITY.map((item, index) => {
                                            return <UU5.Forms.Select.Option key={index} value={item}
                                                content={item} />
                                        })}
                                    </UU5.Forms.Select></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.from} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.DatePicker className={Css.Date()}
                                        name="from3"
                                        dateFrom={today}
                                        disabled={disabledStep2}
                                        valueType="iso"
                                        value={from3}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            setFrom3(opt.value)
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.to} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.DatePicker className={Css.Date()}
                                        name="to3"
                                        valueType="iso"
                                        disabled={disabledStep2}
                                        value={to3}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            if (opt.value <= from2) {
                                                opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.dateInvalid} />)
                                            } else {
                                                setTo3(opt.value)
                                            }
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.credits} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Text className={Css.Number()}
                                        name='credits3'
                                        value={credits3}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        controlled={false}
                                        disabled={disabledStep2}
                                        onChange={(opt) => {
                                            opt.component.setValue(opt.value)
                                            setCredits3(opt.value)
                                        }}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>
                        <UU5.Bricks.Div style={{ marginTop: '1rem' }}>
                            <UU5.Forms.TextArea
                                style={{ paddingRight: '0.4rem' }}
                                label={<UU5.Bricks.Lsi lsi={Lsi.motivation} />}
                                placeholder='Insert text here.'
                                message=""
                                colorSchema={colorSchema}
                                borderRadius={borderRadius}
                                required
                                name="motivationDescription"
                                controlled={false}
                                disabled={disabledStep2}
                                value={motivationDescription}
                                onChange={(opt) => {
                                    opt.component.setValue(opt.value)
                                    setMotivationDescription(opt.value)
                                }}
                            />
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                </UU5.Bricks.Div>)}
            {/* Step Two */}

            {/* Step Three */}
            {stepIndex === 2 && (
                <UU5.Bricks.Div className={Css.StepOne()}>
                    <UU5.Bricks.Div id='centerInputStep3'>
                        <UU5.Forms.DatePicker
                            name="dateOfBirth"
                            label={<UU5.Bricks.Lsi lsi={Lsi.dateOfBirth} />}
                            valueType="iso"
                            value={dateOfBirth}
                            required
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            disabled={disabledStep3}
                            controlled={false}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setDateOfBirth(opt.value)
                            }}

                        />
                        <UU5.Forms.Text
                            name="nationality"
                            label={<UU5.Bricks.Lsi lsi={Lsi.nationality} />}
                            disabled={disabledStep3}
                            value={nationality}
                            required
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            controlled={false}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setNationality(opt.value)
                            }}

                        />
                        <UU5.Forms.Text
                            name="email"
                            label={<UU5.Bricks.Lsi lsi={Lsi.email} />}
                            disabled={disabledStep3}
                            placeholder="School/personal"
                            value={email}
                            required
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            controlled={false}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setEmail(opt.value)
                            }}
                            onBlur={(opt) => {
                                opt.component.setValue(opt.value);
                                if (!/^(([^<>()[\]\\.,;:\s@"]+(\.[^<>()[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(opt.value) && opt.value.length > 0) {
                                    opt.component.setError("Please enter valid email adress");
                                }
                            }}
                        />
                        <UU5.Forms.Text
                            disabled={disabledStep3}
                            name="phone"
                            required
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            label={<UU5.Bricks.Lsi lsi={Lsi.phone} />}
                            controlled={false}
                            placeholder="+420xxxxxxxxx"
                            value={phone}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setPhone(opt.value)
                            }}
                            onBlur={(opt) => {
                                opt.component.setValue(opt.value);
                                if (!/^[\+]?[(]?[0-9]{3}[)]?[-\s\.]?[0-9]{3}[-\s\.]?[0-9]{4,6}$/im.test(opt.value) && opt.value.length > 0) {
                                    opt.component.setError(<UU5.Bricks.Lsi lsi={Lsi.invalidPhone} />);
                                }
                            }}
                        />
                    </UU5.Bricks.Div>
                    <UU5.Bricks.Div className={Css.table()} id="enterLanguageDiv">
                        <UU5.Forms.TextArea
                            name="adress"
                            required
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            disabled={disabledStep3}
                            label={<UU5.Bricks.Lsi lsi={Lsi.address} />}
                            placeholder="Country, city, street, house number, region, ZIP index,"
                            controlled={false}
                            value={adress}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setAdress(opt.value)
                            }}
                        />
                    </UU5.Bricks.Div>
                    <UU5.Bricks.Div id='centerInput'>
                        <UU5.Forms.Text
                            name="banckAccount"
                            disabled={disabledStep3}
                            label={<UU5.Bricks.Lsi lsi={Lsi.bankAccount} />}
                            controlled={false}
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            value={banckAccount}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setBanckAccount(opt.value)
                            }}
                            placeholder="Financial support will be transferred on this account"
                        />
                        <UU5.Forms.Text
                            name="bankName"
                            label={<UU5.Bricks.Lsi lsi={Lsi.bankName} />}
                            disabled={disabledStep3}
                            controlled={false}
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            value={bankName}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setBankName(opt.value)
                            }}
                        />
                        <UU5.Forms.Text
                            name="swift"
                            label={<UU5.Bricks.Lsi lsi={Lsi.swift} />}
                            controlled={false}
                            disabled={disabledStep3}
                            colorSchema={colorSchema}
                            borderRadius={borderRadius}
                            value={swift || ""}
                            onChange={(opt) => {
                                opt.component.setValue(opt.value)
                                setSwift(opt.value)
                            }}
                        />
                    </UU5.Bricks.Div>
                </UU5.Bricks.Div>
            )}
            {/* Step Three */}

        </UU5.Forms.ContextForm>

    )
};


export { AddCreateApplication }
export default AddCreateApplication;