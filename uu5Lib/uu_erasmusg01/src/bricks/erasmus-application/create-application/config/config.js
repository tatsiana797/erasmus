import UU5 from "uu5g04";
import Config from "../../../config/config";

const TAG = Config.TAG + "CreateApplication.";

export default {
  ...Config,
  SEMESTER: {
    FISRT: "1",
    SECOND: "2",
    THIRD: "3",
    FOURTH: "4",
    FIFTH: "5",
    SIXTH: "6",
    SEVENTH: "7",
    EIGHTH: "8",
    NINTH: "9",
    TENTH: "10"
  },

  UNIVERSITY: [
    "not selected",
    "Haute Ecole EPHEC, Belgium",
    "Business Academy Aarhus, Denmark",
    "Escuela Superior Politécnica del Litoral, Ecuador",
    "Laurea University of Applied Sciences, Finland",
    "EPITA School of Engineering and Computer Science, France",
    "Kedge business school, France",
    "University Niccolò Cusano, Italy",
    "PA College, Cyprus",
    "The University of Economics and Culture, Latvia",
    "University of Applied Sciences Würzburg-Schweinfurt, Germany"
  ],

  SPECIALIZATION: {
    ECONOM_MANAGEMENT: "Economics and Management",
    IT: "Information Technology",
    ICT_PROJECT_MANAGEMENT: "ICT Project Management",
  },
  
  COLORS: [
    { color: "my-blue1", code: "#0b2a70"},
    { color: "blue", code: '#2196F3' },
    { color: "cyan", code: '#00BCD4' },
    { color: "green", code: "#4CAF50" },
    { color: "yellow", code: "#FFEB3B" },
    { color: "brown", code: "#795548" },
    { color: "grey", code: "#9E9E9E" },
    { color: "blue-grey", code: "#607D8B" },
    { color: "red", code: "#F44336" },
    { color: "pink", code: "#E91E63" },
    { color: "purple", code: "#9C27B0" },
    { coloe: "deep-purple", code: "#673AB7" },
    { color: "amber", code: "#FFC107" },
    { color: "orange", code: "#FF9800" },
    { color: "deep-orange", code: "#FF5722" },
    { color: "teal", code: "#009688" },
    { color: "light-green", code: "#8BC34A" },
    { coloe: "lime", code: "#CDDC39" },
    { color: "indigo", code: "#3F51B5" },
    { color: "light-blue", code: "#03A9F4" },
    { color: "default", code: '#2196F3' },
  ],

  LANGUAGE: {
    WELL: { en: "Very well", cs: "Velmi dobře" },
    OK: { en: "More or less OK", cs: "Víceméně OK" },
    SURVIVAL: { en: "Survival level", cs: "Úroveň přežití" }
  },
  LANGUAGE_LIST: {
    russian: { en: "Russian", cs: "Russian" },
    english: { en: "English", cs: "English" },
    german: { en: "German", cs: "German" },
    french: { en: "French", cs: "French" },
    spanish: { en: "Spanish", cs: "Spanish" },
    other: { en: "Other", cs: "Other" },
  },

  TAG,
  Css: UU5.Common.Css.createCssModule(
    TAG.replace(/\.$/, "")
      .toLowerCase()
      .replace(/\./g, "-")
      .replace(/[^a-z-]/g, ""),
    process.env.NAME + "/" + process.env.OUTPUT_NAME + "@" + process.env.VERSION // this helps preserve proper order of styles among loaded libraries
  ),
};
