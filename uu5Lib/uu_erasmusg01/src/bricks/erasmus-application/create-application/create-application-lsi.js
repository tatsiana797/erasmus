const Lsi = {

  confirm: {
    en: "Confirmation window",
    cs: "Potvrzovací okénko"
  },
  confirmMessage: {
    en: "Are you sure all the data you have entered is valid? After confirmation you will not be able to edit this step.",
    cs: "Jste si jisti, že všechny zadané údaje jsou platné? Po potvrzení nebudete moci tento krok upravovat."
  },
  cancel: {
    en: "Cancel",
    cs: "Zrušit"
  },
  ok: {
    en: "Ok",
    cs: "Ok"
  },
  createAppl: {
    en: "Create Erasmus application",
    cs: "Vytvoření žádosti o Erasmus"
  },
  headerForm: {
    en: "Create Erasmus application form",
    cs: "Vytvořit přihlášku Erasmus"
  },
  info: {
    en: "Here is some information related to Erasmus create application form",
    cs: "Zde jsou informace týkající se formuláře pro vytvoření přihlášky Erasmus"
  },
  pickLanguage: {
    en: "Choose language",
    cs: "Zvolte jazyk"
  },
  placehLanguage: {
    en: "Language",
    cs: "Jazyk"
  },
  langName: {
    en: "Language name",
    cs: "Název jazyka"
  },
  pickLangLevel: {
    en: "Choose language level",
    cs: "Zvolte úroveň jazyka"
  },
  helper1: {
    en: "Here will be some notes which will help applicant to fill in form fields in the current step.",
    cs: "Zde bude uvedeno několik poznámek, které žadateli pomohou vyplnit pole formuláře v tomto kroku."
  },
  schoolExist: {
    en: "This school was already chosen",
    cs: "Tato škola již byla vybrána"
  },
  dateInvalid: {
    en: "Please enter valid date",
    cs: "Zadejte prosím platné datum"
  },
  required: {
    en: "This field is required",
    cs: "Toto pole je povinné"
  },
  cv: {
    en: "Insert resume",
    cs: "Nahrát životopis"
  },
  ml: {
    en: "Insert motivation letter",
    cs: "Vložte motivační dopis"
  },
  semester: {
    en: "Current semester",
    cs: "Stávající semestr"
  },
  mobSemester: {
    en: "Mobility semester",
    cs: "Semestr mobility"
  },
  mobYear: {
    en: "Mobility year",
    cs: "Rok mobility"
  },
  specialization: {
    en: "Study programme",
    cs: "Studijní program"
  },
  school: {
    en: "School",
    cs: "Škola"
  },
  from: {
    en: "From",
    cs: "Od"
  },
  to: {
    en: "To",
    cs: "Do"
  },
  credits: {
    en: "Credits",
    cs: "Kredity"
  },
  motivation: {
    en: "What are your main motivations to go on exchange:",
    cs: "Jaké jsou vaše hlavní motivy pro účast v programu Erasmus:"
  },
  dateOfBirth: {
    en: "Date of birth",
    cs: "Datum narození"
  },
  nationality: {
    en: "Nationality",
    cs: "Státní příslušnost"
  },
  email: {
    en: "Email",
    cs: "E-mail"
  },
  phone: {
    en: "Phone number",
    cs: "Telefonní číslo"
  },
  invalidPhone: {
    en: "Please provide us with valid phone number",
    cs: "Uveďte prosím platné telefonní číslo"
  },
  address: {
    en: "Address",
    cs: "Adresa"
  },
  bankAccount: {
    en: "Bank account",
    cs: "Bankovní účet"
  },
  bankName: {
    en: "Bank name",
    cs: "Název banky"
  },
  swift: {
    en: "Clearing/BIC/SWIFT number",
    cs: "Zúčtovací číslo/BIC/SWIFT"
  },
  submit: {
    en: "Submit",
    cs: "Uložit"
  },
  save: {
    en: "Save",
    cs: "Uložit"
  },
}

export default Lsi
