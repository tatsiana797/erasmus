//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useState, useCallback, useRef } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "./config/config";
import AddCreateApplication from "./add-create-application"
import useStudent from "../../common/context/student/use-student";
import useApplication from "../../common/context/application/use-application";
import DataObjectStateResolver from "../../common/resolver/data-object-state-resolver";
import Lsi from "./create-application-lsi";
//@@viewOff:imports/

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "CreateApplicationView"
  //@@viewOff:statics
};

export const CreateApplicationView = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  propTypes: {
    isStudent: UU5.PropTypes.bool,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
    buttonColoSchema: UU5.PropTypes.string,
    buttonBorderRadius: UU5.PropTypes.string,
    buttonElevation: UU5.PropTypes.string,
    buttonBgStyle: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    isStudent: false,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined,
    buttonColoSchema: undefined,
    buttonBorderRadius: undefined,
    buttonElevation: undefined,
    buttonBgStyle: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {

    const confirm = useRef()
    const [stepIndex, setStepIndex] = useState(0);
    const [showAdd, setShowAddModal] = useState(false);
    const studentDataObject = useStudent();
    const student = studentDataObject?.data
    const applicationDataObject = useApplication();
    const application = applicationDataObject?.data
    const handleOnChangeStep = useCallback((el) => {
      setStepIndex(el)
    }, [stepIndex])

    function handleOpenAddCreateApplicationForm() {
      setShowAddModal(true)
    }

    const isEnabled = props.isStudent && (application === undefined || application === null || JSON.stringify(application) === JSON.stringify({}))

    function handleConfirmRequest(options) {
      confirm.current.open({
        onRefuse: () => options.component.setReady(),
        onConfirm: async () => {
          const studentErasmus = {
            personalInfo: {
              dateOfBirth: options.values.dateOfBirth?.toString(),
              nationality: options.values.nationality?.toString(),
              email: options.values.email?.toString(),
              phone: options.values.phone?.toString(),
              adress: options.values.adress?.toString(),
              banckAccount: options.values.banckAccount?.toString(),
              bankName: options.values.bankName?.toString(),
              swift: options.values.swift?.toString()
            },
            studyProgramme: application?.erasmus?.studyProgramme,
            interviewDate: "",
            requestForPrerecogn: application?.erasmus?.requestForPrerecogn || "false",
            insurance: "false",
            learningAgreement: "false",
            grant: "false",
            currentSemester: application.erasmus.currentSemester,
            semesterYear: application.erasmus.semesterYear,
            motivationDescription: application.erasmus.motivationDescription,
            erasmusState: application.erasmus.erasmusState ? application.erasmus.erasmusState : "active",
            attachments: application.erasmus.attachments,
            language: application.erasmus.language,
            destionations: application.erasmus.destionations
          }
          const newData = {
            studentId: application.studentId,
            id: application.id,
            firstName: application.firstName,
            lastName: application.lastName,
            uuIdentity: application.uuIdentity,
            erasmus: studentErasmus
          }
          try {
            await applicationDataObject.handlerMap.update(newData);
            options.component.saveDone();
            options.component.reset();
          } catch (e) {
            console.log(e);
            return;
          }
        },
        header: <UU5.Bricks.Lsi lsi={Lsi.confirm} />,
        content: <UU5.Bricks.P>
          <UU5.Bricks.Lsi lsi={Lsi.confirmMessage} />
        </UU5.Bricks.P>,
        refuseButtonProps: { content: <UU5.Bricks.Lsi lsi={Lsi.cancel} />, elevation: props.elevation, borderRadius: props.borderRadius, bgStyle: props.bgStyle },
        confirmButtonProps: { content: <UU5.Bricks.Lsi lsi={Lsi.ok} />, elevation: props.elevation, borderRadius: props.borderRadius, bgStyle: props.bgStyle, colorSchema: props.colorSchema },
        confirmButtonLeft: false
      })

    }

    async function handleChangeStudentFiles(opt, nameVal) {
      let newData = {};
      newData['id'] = application?.id
      newData[nameVal] = opt.value
      newData["studentId"] = student.id
      try {
        await applicationDataObject.handlerMap.updateAttachment(newData);
      } catch (e) {
        console.log(e);
      }
    }

    async function handleAddApplicationSave(opt) {
      let language = []
      for (let index = 0; index < 6; index++) {
        if (opt.values["language" + `${index}`] && opt.values["language" + `${index}`] !== "other") {
          language.push({ name: opt.values["language" + `${index}`], level: opt.values["level" + `${index}`] })
        }
        if (opt.values["otherLanguageName" + `${index}`] && opt.values["language" + `${index}`] == "other") {
          language.push({ name: opt.values["otherLanguageName" + `${index}`], level: opt.values["level" + `${index}`] })
        }
      }

      if (stepIndex == 0) {
        const studentErasmus = {
          currentSemester: opt.values.currentSemester,
          semesterYear: opt.values.semester + " " + opt.values.year,
          attachments: application.erasmus.attachments,
          language: language,
          erasmusState: "active",
          studyProgramme: opt.values.specialization
        }

        const newData = {
          studentId: application.studentId,
          id: application.id,
          firstName: student.firstName,
          lastName: student.lastName,
          uuIdentity: student.uuIdentity,
          erasmus: studentErasmus
        }
        try {
          await applicationDataObject.handlerMap.update(newData);
        } catch (e) {
          console.log(e);
          return;
        }
        setStepIndex(stepIndex + 1)
        opt.component.setReady()
        opt.component.reset()
      }
      if (stepIndex == 1) {
        const studentErasmus = {
          currentSemester: application.erasmus.currentSemester,
          semesterYear: application.erasmus.semesterYear,
          attachments: application.erasmus.attachments,
          language: application.erasmus.language,
          studyProgramme: application.erasmus.studyProgramme,
          destionations: [{
            order: "1",
            school: opt.values.school1 !== "not selected" ? opt.values.school1 : null,
            from: opt.values.from1,
            to: opt.values.to1,
            numberOfCreadits: opt.values.credits1,
            universityApprovalStatus: "initial",
          },
          {
            order: "2",
            school: opt.values.school2,
            from: opt.values.from2,
            to: opt.values.to2,
            numberOfCreadits: opt.values.credits2,
            universityApprovalStatus: "initial"
          },
          {
            order: "3",
            school: opt.values.school3,
            from: opt.values.from3,
            to: opt.values.to3,
            numberOfCreadits: opt.values.credits3,
            universityApprovalStatus: "initial"
          },
          ],
          motivationDescription: opt.values.motivationDescription,
        }

        const newData = {
          studentId: application.studentId,
          id: application.id,
          firstName: student.firstName,
          lastName: student.lastName,
          uuIdentity: student.uuIdentity,
          erasmus: studentErasmus
        }
        try {
          await applicationDataObject.handlerMap.update(newData);
          opt.component.reset();
        } catch (e) {
          console.log(e);
          return;
        }
        setStepIndex(stepIndex + 1)
        opt.component.setReady()
        opt.component.reset()
      }
      if (stepIndex == 2) {
        handleConfirmRequest(opt)

      }
    }

    async function handleAddApplicationCancel(opt) {
      const codeArr = []
      if (application?.erasmus?.attachments?.cv.code !== undefined) {
        codeArr.push(application?.erasmus?.attachments?.cv.code)
      }
      if (application?.erasmus?.attachments?.ml?.code !== undefined) {
        codeArr.push(application?.erasmus?.attachments?.ml?.code)
      }
      if (codeArr.length !== 0) {
        const newData = {
          studentId: application.studentId,
          id: application.id,
          code: codeArr
        }
        try {
          await applicationDataObject.handlerMap.removeAttachments(newData);
          setShowAddModal(false)
          setStepIndex(0)
          opt.component.reset();
        } catch (e) {
          opt.component.saveFail(e);
          return;
        }
        opt.component.saveDone();
        opt.component.reset();

      } else {
        setShowAddModal(false)
      }

    }

    function handleAddFail({ component, dtoOut: e }) {
      console.error(e);
      component.getAlertBus().addAlert({
        content: <Error errorData={e} />,
        colorSchema: "danger",
      });
    }

    function handleAddDone({ component }) {
      clearForm(component);
      setShowAddModal(false);
    }

    function clearForm(form) {
      if (form.reset) form.reset();
      if (form.getAlertBus) form.getAlertBus().clearAlerts();
    }

    return (
      <>
        <UU5.Bricks.ConfirmModal ref={confirm} />
        <UU5.Bricks.Button
          borderRadius={props.buttonBorderRadius}
          bgStyle={props.buttonBgStyle}
          elevation={props.buttonElevation}
          colorSchema={props.buttonColoSchema}
          onClick={handleOpenAddCreateApplicationForm}
          disabled={!isEnabled}
          style={{ height: '2.5rem', display: 'block', marginTop: '0.2rem' }}>
          <UU5.Bricks.Text colorSchema='white' style={{ fontSize: '16px' }}>
            <UU5.Bricks.Lsi lsi={Lsi.createAppl} />
          </UU5.Bricks.Text>
        </UU5.Bricks.Button>
        <DataObjectStateResolver dataObject={studentDataObject} >
          <AddCreateApplication
            shown={showAdd}
            stepIndex={stepIndex}
            onCancel={handleAddApplicationCancel}
            onSave={handleAddApplicationSave}
            onSaveDone={handleAddDone}
            onSaveFail={handleAddFail}
            onChangeStudentFiles={handleChangeStudentFiles}
            application={application || {}}
            handleOnChangeStep={handleOnChangeStep}
            colorSchema={props.colorSchema}
            borderRadius={props.borderRadius}
            elevation={props.elevation}
            bgStyle={props.bgStyle}
          />
        </DataObjectStateResolver>
      </>
    )
  }
})

//viewOn:exports
export default CreateApplicationView;
//viewOff:exports
