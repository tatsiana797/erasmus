//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "../config/config";
import StudentLoader from "../common/loader/student-loader";
import CreateApplicationView from "./create-application/create-application-view";
import ApplicationLoader from "../common/loader/application-loader"

//@@viewOff:imports
const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "ErasmusApplication"
  //@@viewOff:statics
};
export const ErasmusApplication = createVisualComponent({
  ...STATICS,
  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
    uuIdentity: UU5.PropTypes.string,
    role: UU5.PropTypes.string,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
    isStudent: UU5.PropTypes.bool,
    isStudyDepOrCoordinator: UU5.PropTypes.bool,
    buttonColoSchema: UU5.PropTypes.string,
    buttonBorderRadius: UU5.PropTypes.string,
    buttonElevation: UU5.PropTypes.string,
    buttonBgStyle: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
    uuIdentity: undefined,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined,
    isStudent: false,
    buttonColoSchema: undefined,
    buttonBorderRadius: undefined,
    buttonElevation: undefined,
    buttonBgStyle: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {
    const errorsToWarn = ["Warning:"]
    const oldConsError = console.error;
    console.error = function (...args) {
      let toWarn = false;
      if (typeof args[0] === 'string') {
        errorsToWarn.map(function (_s) {
          if (args[0].startsWith(_s)) {
            toWarn = true;
          }
        })
      }
      toWarn ? console.warn(...args) : oldConsError(...args);
    }
    //@@viewOn:render
    return (
      <StudentLoader uuIdentity={props.uuIdentity} baseUri={props.baseUri}>
        <ApplicationLoader baseUri={props.baseUri} uuIdentity={props.uuIdentity} >
          <UU5.Bricks.Div style={{ display: "block", padding: "5px" }}>
            <CreateApplicationView
              isStudent={props.isStudent}
              colorSchema={props.colorSchema}
              borderRadius={props.borderRadius}
              elevation={props.elevation}
              bgStyle={props.bgStyle}
              buttonBorderRadius={props.buttonBorderRadius}
              buttonColoSchema={props.buttonColoSchema}
              buttonElevation={props.buttonElevation}
              buttonBgStyle={props.buttonBgStyle}
            />
          </UU5.Bricks.Div>
        </ApplicationLoader>
      </StudentLoader>
    )
  }
})

//viewOn:exports
export default ErasmusApplication;
//viewOff:exports
