//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponentWithRef, useState, useEffect } from "uu5g04-hooks";
import Config from "./config/config";
import "uu5g04-forms";
import Lsi from "./request-subjects-recognition-lsi"
import { ProgressIndicator, ProgressStep, Tooltip } from 'carbon-components-react';
import Css from './request-subjects-recogn.scss'
//@@viewOff:imports

const AddRequestSubjectsRecognition = createVisualComponentWithRef({
    //@@viewOn:statics
    displayName: Config.TAG + "AddRequestSubjectsRecognition",
    //@@viewOff:statics

    //@@viewOn:propTypes
    propTypes: {
        onSave: UU5.PropTypes.func,
        onCancel: UU5.PropTypes.func,
        shown: UU5.PropTypes.bool,
        onSaveDone: UU5.PropTypes.func,
        onSaveFail: UU5.PropTypes.func,
        stepIndex: UU5.PropTypes.number,
        confirm: UU5.PropTypes.any,
        handleOnChangeStep: UU5.PropTypes.func,
        colorSchema: UU5.PropTypes.string,
        borderRadius: UU5.PropTypes.string,
        elevation: UU5.PropTypes.string,
        bgStyle: UU5.PropTypes.string,
    },
    //@@viewOff:propTypes

    //@@viewOn:defaultProps
    defaultProps: {
        onSave: () => { },
        onCancel: () => { },
        shown: false,
        onSaveDone: () => { },
        onSaveFail: () => { },
        stepIndex: 0,
        confirm: undefined,
        handleOnChangeStep: () => { },
        colorSchema: undefined,
        borderRadius: undefined,
        elevation: undefined,
        bgStyle: undefined
    },
    //@@viewOff:defaultProps

    //@@viewOn:render
    render(props) {
        return (
            <UU5.Bricks.Div>
                <UU5.Forms.ContextModal
                    shown={props.shown}
                    header={<Header />}
                    footer={props.stepIndex == 1 ? <Controls bgStyle={props.bgStyle} elevation={props.elevation} borderRadius={props.borderRadius} colorSchema={props.colorSchema} /> :
                        <ControlsStepper bgStyle={props.bgStyle} borderRadius={props.borderRadius} colorSchema={props.colorSchema} elevation={props.elevation} />}
                    onClose={props.onCancel}
                    size="l"
                    mountContent="onFirstOpen"
                    overflow
                    forceRender
                >
                    <Form
                        borderRadius={props.borderRadius}
                        colorSchema={props.colorSchema}
                        handleOnChangeStep={props.handleOnChangeStep}
                        onSave={props.onSave}
                        onSaveDone={props.onSaveDone}
                        onSaveFail={props.onSaveFail}
                        onCancel={props.onCancel}
                        stepIndex={props.stepIndex}
                    />
                </UU5.Forms.ContextModal>

            </UU5.Bricks.Div>
        );
        //@@viewOff:render
    }
});

function Controls({ bgStyle, colorSchema, elevation, borderRadius }) {
    return (
        <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }}
            controlled={false} buttonSubmitProps={{
                content: <UU5.Bricks.Lsi lsi={Lsi.submit} />, bgStyle: bgStyle, colorSchema: colorSchema,
                elevation: elevation, borderRadius: borderRadius
            }} />
    );
}

function ControlsStepper({ bgStyle, colorSchema, elevation, borderRadius }) {
    return <UU5.Forms.ContextControls buttonCancelProps={{ elevation: elevation, borderRadius: borderRadius, bgStyle: bgStyle }}
        controlled={false} buttonSubmitProps={{
            content: <UU5.Bricks.Lsi lsi={Lsi.save} />, bgStyle: bgStyle, colorSchema: colorSchema,
            elevation: elevation, borderRadius: borderRadius
        }} />
}

function Header() {
    return (
        <UU5.Forms.ContextHeader
            content={<UU5.Bricks.Lsi lsi={Lsi.header} />}
            info={<UU5.Bricks.Lsi lsi={Lsi.info} />}
        />
    );
}

function Form({ onSave, onCancel, onSaveDone, onSaveFail, stepIndex, colorSchema, borderRadius }) {

    useEffect(() => {
        const color = Config.COLORS.find(color => {
            if (color.color === colorSchema) {
                return color
            } else {
                return color.color === 'default'
            }
        }).code
        const indicator = document.getElementsByClassName('bx--progress-step--current')[0]
        const indicatorLine = indicator.getElementsByClassName('bx--progress-line ')[0]
        indicatorLine.style.backgroundColor = color
        const indicatorSvg = indicator.querySelector('svg')
        indicatorSvg.style.fill = color
    })

    let generateSyllablesId = UU5.Common.Tools.generateUUID(length = 3)

    const [uploaderList, setUploaderList] = useState([<UU5.Forms.File
        required
        colorSchema={colorSchema}
        style={{ marginTop: 0 }}
        name={"syllable_" + generateSyllablesId}
        label={<UU5.Bricks.Lsi lsi={Lsi.syllables} />}
        controlled={false}
        placeholder="Upload"
        onChange={(opt) => {
            opt.component.setValue(opt.value)
        }}
        key={"syllable_" + generateSyllablesId}
    />]);
    const handleAddFileUploader = () => {
        setUploaderList(uploaderList.concat(<UU5.Forms.File
            required
            colorSchema={colorSchema}
            borderRadius={borderRadius}
            name={"syllable_" + generateSyllablesId}
            controlled={false}
            placeholder="Upload"
            onChange={(opt) => {
                opt.component.setValue(opt.value)
            }}
            key={uploaderList?.length} />));
    }
    return (
        <UU5.Forms.ContextForm
            onSave={onSave}
            onSaveDone={onSaveDone}
            onSaveFail={onSaveFail}
            onCancel={onCancel}
            progressIndicator={<UU5.Bricks.Loading />}
        >
            <UU5.Bricks.Div className={Css.ProgressIndicator()}>
                <ProgressIndicator
                    spaceEqually={true}
                    currentIndex={stepIndex}
                >
                    <ProgressStep
                        label=""
                        disabled={stepIndex === 1}
                        renderLabel={() => (
                            <Tooltip
                                direction="bottom"
                                showIcon={false}
                                triggerText={'Subject names '}
                                triggerClassName="bx--progress-label"
                                tooltipId="tooltipId-0">
                                <p>
                                    <UU5.Bricks.Lsi lsi={Lsi.helper1} />
                                </p>
                            </Tooltip>
                        )}
                    />
                    <ProgressStep
                        label=""
                        disabled={stepIndex === 0}
                        renderLabel={() => (
                            <Tooltip
                                direction="bottom"
                                showIcon={false}
                                triggerText={'Upload syllables'}
                                triggerClassName="bx--progress-label"
                                tooltipId="tooltipId-1">
                                <p>
                                    <UU5.Bricks.Lsi lsi={Lsi.helper1} />
                                </p>
                            </Tooltip>
                        )}
                    />
                </ProgressIndicator>
            </UU5.Bricks.Div>


            {stepIndex == 0 && <UU5.Bricks.Div className={Css.table()}>
                <UU5.Bricks.Div id='table' >
                    <UU5.Bricks.Div className='theader'>
                        <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.uuSubjectName} /></UU5.Bricks.Div>
                        <UU5.Bricks.Div className='table_header'><UU5.Bricks.Lsi lsi={Lsi.partnerUniSubjectName} /> </UU5.Bricks.Div>
                    </UU5.Bricks.Div>
                    {[1, 2, 3, 4, 5, 6, 7, 8, 9, 10].map((item, index) => {
                        return (<UU5.Bricks.Div className='table_row' key={index}>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.uuSubjectName} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Text style={{ marginTop: 0, marginBottom: "0.3rem" }}
                                        required={item === 1 ? true : false}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        name={"subjectUcl" + `${item}`}
                                        controlled={false}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                            <UU5.Bricks.Div className='table_small'>
                                <UU5.Bricks.Div className='table_cell'><UU5.Bricks.Lsi lsi={Lsi.partnerUniSubjectName} /></UU5.Bricks.Div>
                                <UU5.Bricks.Div className='table_cell'>
                                    <UU5.Forms.Text style={{ marginTop: 0, marginBottom: "0.3rem" }}
                                        required={item === 1 ? true : false}
                                        colorSchema={colorSchema}
                                        borderRadius={borderRadius}
                                        name={"subject" + `${item}`}
                                        controlled={false}
                                    /></UU5.Bricks.Div>
                            </UU5.Bricks.Div>
                        </UU5.Bricks.Div>)
                    })}
                </UU5.Bricks.Div>
            </UU5.Bricks.Div>}

            {stepIndex == 1 && <UU5.Bricks.Div style={{ marginTop: "1.2rem" }} >

                <UU5.Bricks.Div >
                    {uploaderList}
                </UU5.Bricks.Div>
                <UU5.Bricks.Div style={{ width: "10rem", margin: "0 auto" }} >
                    <UU5.Bricks.Button id='buttonStyle' bgStyle="transparent" size='l' onClick={handleAddFileUploader} borderRadius={borderRadius} >
                        <UU5.Bricks.Icon icon="mdi-shape-circle-plus" />
                        <UU5.Bricks.Div>
                            <UU5.Bricks.Lsi lsi={Lsi.uploadFile} />
                        </UU5.Bricks.Div>
                    </UU5.Bricks.Button>
                </UU5.Bricks.Div>
            </UU5.Bricks.Div>}
        </UU5.Forms.ContextForm>
    )
};
export { AddRequestSubjectsRecognition }
export default AddRequestSubjectsRecognition;
