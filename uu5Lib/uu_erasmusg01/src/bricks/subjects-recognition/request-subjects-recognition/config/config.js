import UU5 from "uu5g04";
import Config from "../../../config/config";

const TAG = Config.TAG + "RequestSubjectsRecognition.";

export default {
  ...Config,

  COLORS: [
    { color: "my-blue1", code: "#0b2a70"},
    { color: "blue", code: '#2196F3' },
    { color: "cyan", code: '#00BCD4' },
    { color: "green", code: "#4CAF50" },
    { color: "yellow", code: "#FFEB3B" },
    { color: "brown", code: "#795548" },
    { color: "grey", code: "#9E9E9E" },
    { color: "blue-grey", code: "#607D8B" },
    { color: "red", code: "#F44336" },
    { color: "pink", code: "#E91E63" },
    { color: "purple", code: "#9C27B0" },
    { coloe: "deep-purple", code: "#673AB7" },
    { color: "amber", code: "#FFC107" },
    { color: "orange", code: "#FF9800" },
    { color: "deep-orange", code: "#FF5722" },
    { color: "teal", code: "#009688" },
    { color: "light-green", code: "#8BC34A" },
    { coloe: "lime", code: "#CDDC39" },
    { color: "indigo", code: "#3F51B5" },
    { color: "light-blue", code: "#03A9F4" },
    { color: "default", code: '#2196F3' },
  ],

  TAG,
  Css: UU5.Common.Css.createCssModule(
    TAG.replace(/\.$/, "")
      .toLowerCase()
      .replace(/\./g, "-")
      .replace(/[^a-z-]/g, ""),
    process.env.NAME + "/" + process.env.OUTPUT_NAME + "@" + process.env.VERSION // this helps preserve proper order of styles among loaded libraries
  ),
};
