import Config from "./config/config";

const Subjects = () => Config.Css.css`

#text {
  float: left;
   width: 47%;
  }

  #button {
    float: left;
     margin-top: 1.7rem;
  }
  @media screen and (max-width: 718px) {
    #text {
       width: 45%;

      }
      #button {
        margin-top: 1.7rem;
      }
    }
    @media screen and (max-width: 595px) {
      #table{
        width: initial;
      }
      #text {
         width: 45%;
  
        }
        #button {
          margin-top: 3rem;
        }
      }

`
const table = () => Config.Css.css`
#table {
  padding-top: 1rem;
  width: 50rem;
  margin: 0 auto;

}
.table {
    display: table;
    text-align: center;
    width: 100%;
    margin: 10% auto 0;
    border-collapse: separate;
    font-weight: 400;
  }

  .table_row {
    display: table-row;
     
  }
  
  .theader {
    display: table-row;
  }
  
  .table_header {
    display: table-cell;
    padding-top: 10px;
    padding-bottom: 10px;
    color: #0000008a;
    padding-right: 0.5rem;
  }
 
  .table_cell {
    padding-right: 0.5rem;
    width: 25rem;
    box-sizing: content-box;
  }
  .table_small {
    display: table-cell;
  }
  
  .table_row > .table_small > .table_cell:nth-child(odd) {
    display: none;
    padding-top: 10px;
    padding-bottom: 10px;
  }
  
  .table_row > .table_small > .table_cell {
    padding-top: 3px;
    padding-bottom: 3px;
    color: #5b5b5b;
  }
  
  @media screen and (max-width: 900px) {
    .table {
      width: 90%
    }
  }
  @media screen and (max-width: 925px) {
    #table {
      padding-top: 1rem;
      width: 46rem;
      margin: 0 auto;
    }
    .table_cell {
      padding-right: 0.5rem;
      width: 23rem
    }
  }
  @media screen and (max-width: 850px) {
    #table {
      padding-top: 1rem;
      width: 42rem;
      margin: 0 auto;
    }
    .table_cell {
      padding-right: 0.5rem;
      width: 21rem
    }
  }
  @media screen and (max-width: 745px) {
    #table {
      padding-top: 1rem;
      width: 38rem;
      margin: 0 auto;
    }
    .table_cell {
      padding-right: 0.5rem;
      width: 19rem
    }
  }
  @media screen and (max-width: 677px) {
    #table {
      padding-top: 1rem;
      width: 35rem;
      margin: 0 auto;
    }
    .table_cell {
      padding-right: 0.5rem;
      width: 17rem
    }
  }
  
  @media screen and (max-width: 650px) {
    #table {
      padding-top: unset;
      width: unset;
      margin: unset;
    }
    .table_cell {
      padding-right: 0.5rem;
      width: unset
    }
    .table {
      display: block;
    }
    .table_row:nth-child(2n+3) {
      background: none;
    }
    .theader {
      display: none;
    }
    .table_row > .table_small > .table_cell:nth-child(odd) {
      display: inherit;
      width: 50%;
    }
    .table_cell {
      display: table-cell;
      width: 50%;
      font-size: 12px;
    }
    .table_row {
      display: table;
      width: 100%;
      border-collapse: separate;
      padding-bottom: 20px;
      margin: 5% auto 0;
      text-align: center;
       border-bottom: 1px solid #e0e0e0;
    }
    .table_small {
      display: table-row;
    }
    .table_row > .table_small:first-child > .table_cell:last-child {
      border-left: none;
    }
    .table_row > .table_small:last-child > .table_cell:first-child {
      border-right: none;
    }

    @media screen and (max-width: 560px) {
    #table{
      width: unset;
      margin: unset;
    }
    }
  }

`
const Select = () => Config.Css.css`
margin-top: 0rem !important;
margin-bottom: 0rem !important;
width: 17rem;
 margin-right: 0.7rem;
 @media screen and (max-width: 650px) {
   width: 16rem;
 }
  `
const Number = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;;
  width: 10rem;
  @media screen and (max-width: 650px) {
    width: 16rem;
 }
  `
const Date = () => Config.Css.css`
  margin-top: 0rem !important;
  margin-bottom: 0rem !important;
width: 11rem;
 margin-right: 0.7rem;
 @media screen and (max-width: 650px) {
    width: 16rem;
 }
  `

const ProgressIndicator = () => Config.Css.css`
.bx--progress-line {
  height: 3px;
}
  .bx--progress-label:focus {
    box-shadow: 0 0.1875rem  0 0 #161616!important;
    color: #161616!important;
}
.bx--progress-step-button--unclickable .bx--tooltip__label:hover {
box-shadow: 0 0.0625rem 0 0 #161616!important;
color: #161616!important;
}
.bx--progress-step-button:not(.bx--progress-step-button--unclickable) .bx--progress-label:active {
box-shadow: 0 0.05 0 0 #161616!important;
color: #161616!important;
}
.bx--progress-label:hover {
box-shadow: 0 0.0625rem 0 0 #161616!important;
color: #161616!important;
}
.bx--progress-step--complete {
svg{
fill: #161616!important;
}
}
`


export default {
  Subjects,
  Date,
  Select,
  table,
  Number,
  ProgressIndicator,
}