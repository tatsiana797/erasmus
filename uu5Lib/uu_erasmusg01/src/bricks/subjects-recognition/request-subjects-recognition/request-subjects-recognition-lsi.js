const Lsi = {
  save: {
    en: "Save",
    cs: "Uložit"
  },
  header: {
    en: "Request subjects recognition form",
    cs: "Žádost o uznání subjektu"
  },
  info: {
    en: "<uu5string/>Request subjects recognition. For more information see <UU5.Bricks.Link href='%s' target='_blank' content='documentation'/>.",
    cs: "<uu5string/>Žádost o uznání subjektů. Další informace naleznete na adrese <UU5.Bricks.Link href='%s' target='_blank' content='dokumentace'/>."
  },
  submit: {
    en: "Submit form",
    cs: "Odeslat formulář"
  },
  error: {
    en: "Failed to save",
    cs: "Nepodařilo se uložit"
  },
  confirm: {
    en: "Confirmation window",
    cs: "Potvrzovací okénko"
  },
  confirmMessage: {
    en: "Are you sure all the data you have entered is valid? After confirmation you will not be able to edit this step.",
    cs: "Jste si jisti, že všechny zadané údaje jsou platné? Po potvrzení nebudete moci tento krok upravovat."
  },
  cancel: {
    en: "Cancel",
    cs: "Zrušit"
  },
  ok: {
    en: "Ok",
    cs: "Ok"
  },
  requestForm: {
    en: "Request subject recognition",
    cs: "Žádost o uznání předmětů"
  },
  uploadFile: {
    en: "Upload file",
    cs: "Nahrát soubor"
  },
  syllables: {
    en: "Syllabuses of subjects",
    cs: "Sylaby předmětů"
  },
  helper1: {
    en: "Here will be some notes which will help applicant to fill in form fields in the current step.",
    cs: "Zde bude uvedeno několik poznámek, které žadateli pomohou vyplnit pole formuláře v tomto kroku."
  },
  uuSubjectName: {
    en: "Name of the subject at Unicorn University",
    cs: "Název předmětu na Unicorn University"
  },
  partnerUniSubjectName: {
    en: "Name of the subject at Partner University",
    cs: "Název předmětu na partnerské univerzitě"
  }
}

export default Lsi
