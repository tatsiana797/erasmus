//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useState, useCallback, useRef } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "../../config/config";
import AddRequestSubjectsRecognition from "./add-request-subjects-recognition";
import useApplication from "../../common/context/application/use-application";
import Lsi from "./request-subjects-recognition-lsi";
import { DataObjectStateResolver } from "../../common/resolver/data-object-state-resolver";
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "RequestSubjectsRecognitionView"
  //@@viewOff:statics
};

export const RequestSubjectsRecognitionView = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  propTypes: {
    isStudent: UU5.PropTypes.bool,
    uuIdentity: UU5.PropTypes.string,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
    buttonColoSchema: UU5.PropTypes.string,
    buttonBorderRadius: UU5.PropTypes.string,
    buttonElevation: UU5.PropTypes.string,
    buttonBgStyle: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    isStudent: false,
    uuIdentity: undefined,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined,
    buttonColoSchema: undefined,
    buttonBorderRadius: undefined,
    buttonElevation: undefined,
    buttonBgStyle: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {
    const [showAdd, setShowAddModal] = useState(false);
    const [stepIndex, setStepIndex] = useState(0);
    const applicationDataObject = useApplication();

    const application = applicationDataObject?.data
    const isEnabled = (application !== undefined || application !== null || JSON.stringify(application) !== JSON.stringify({})) && application?.erasmus?.requestForPrerecogn !== "true" && application?.erasmus?.destionations?.some(item => item.universityApprovalStatus === 'final')

    function handleOpenAddRequestSubjectsRecognitionForm() {
      setShowAddModal(true)
    }

    function handleCloseAddRequestSubjectsRecognitionForm() {
      setShowAddModal(false)
    }

    const handleOnChangeStep = useCallback((el) => {
      setStepIndex(el)
    }, [stepIndex])
    const confirm = useRef()

    function handleConfirmRequest(options) {
      confirm.current.open({
        onRefuse: () => options.component.setReady(),
        onConfirm: async () => {
          for (const [key, value] of Object.entries(options.values)) {
            const newData = {
              id: application?.id,
              studentId: application?.studentId,
              syllabusId: key,
              syllabus: value,
            };
            try {
              await applicationDataObject.handlerMap.updateAttachment(newData);
            } catch (e) {
              opt.component.saveFail(e);
              return;
            }
          }
          options.component.reset()
          handleCloseAddRequestSubjectsRecognitionForm()
          setStepIndex(0)
        },
        header: <UU5.Bricks.Lsi lsi={Lsi.confirm} />,
        content: <UU5.Bricks.P>
          <UU5.Bricks.Lsi lsi={Lsi.confirmMessage} />
        </UU5.Bricks.P>,
        refuseButtonProps: { content: <UU5.Bricks.Lsi lsi={Lsi.cancel} />, elevation: props.elevation, borderRadius: props.borderRadius, bgStyle: props.bgStyle },
        confirmButtonProps: { content: <UU5.Bricks.Lsi lsi={Lsi.ok} />, elevation: props.elevation, borderRadius: props.borderRadius, bgStyle: props.bgStyle, colorSchema: props.colorSchema },
        confirmButtonLeft: false
      })
    }

    function handleConfirmStep1(options, newSubjectRecogn) {
      confirm.current.open({
        onRefuse: () => options.component.setReady(),
        onConfirm: async () => {
          const studentErasmus = {
            studyProgramme: application?.erasmus?.studyProgramme,
            subjectsRecognition: newSubjectRecogn,
            attachments: application?.erasmus.attachments,
            currentSemester: application?.erasmus.currentSemester,
            motivationDescription: application?.erasmus.motivationDescription,
            destionations: application?.erasmus.destionations,
            language: application?.erasmus.language,
            personalInfo: application?.erasmus.personalInfo,
            interviewDate: application?.erasmus.interviewDate,
            requestForPrerecogn: "true",
            insurance: application?.erasmus.insurance,
            learningAgreement: application?.erasmus.learningAgreement,
            grant: application?.erasmus.grant,
            semesterYear: application?.erasmus.semesterYear,
            erasmusState: application?.erasmus.erasmusState,
            personalNote: application?.erasmus?.personalNote || "",
            noteForCoordinator: application?.erasmus?.noteForCoordinator || "",
          }
          const newData = {
            studentId: application?.studentId,
            id: application?.id,
            firstName: application?.firstName,
            lastName: application?.lastName,
            uuIdentity: application?.uuIdentity,
            erasmus: studentErasmus
          }

          try {
            await applicationDataObject.handlerMap.update(newData);
          } catch (e) {
            console.log(e);
            return;
          }
          setStepIndex(stepIndex + 1)
          options.component.setReady()
          options.component.reset()
        },
        header: <UU5.Bricks.Lsi lsi={Lsi.confirm} />,
        content: <UU5.Bricks.P>
          <UU5.Bricks.Lsi lsi={Lsi.confirmMessage} />
        </UU5.Bricks.P>,
        refuseButtonProps: { content: <UU5.Bricks.Lsi lsi={Lsi.cancel} />, elevation: props.elevation, borderRadius: props.borderRadius, bgStyle: props.bgStyle },
        confirmButtonProps: { content: <UU5.Bricks.Lsi lsi={Lsi.ok} />, elevation: props.elevation, borderRadius: props.borderRadius, bgStyle: props.bgStyle, colorSchema: props.colorSchema },
        confirmButtonLeft: false
      })
    }

    async function handleAddRequestRecognitionSave(opt) {
      const subjectsRecognition = []
      for (let index = 0; index <= 10; index++) {
        let subjectForeignUni = 'subject' + `${index}`
        let subjectUclUni = "subjectUcl" + `${index}`
        if (opt.values[`${subjectForeignUni}`] !== undefined) {
          subjectsRecognition.push({
            subjectNameForeignUni: opt.values[`${subjectForeignUni}`],
            subjectNameUcl: opt.values[`${subjectUclUni}`]
          })
        }
      }

      const newSubjectRecogn = subjectsRecognition.filter(item => item.subjectNameForeignUni !== "")
      if (stepIndex == 0) {
        handleConfirmStep1(opt, newSubjectRecogn)
      } else {
        handleConfirmRequest(opt)
      }
    }

    async function handleAddRequestRecognitionCancel(opt) {
      const newData = {
        studentId: application?.studentId,
        id: application?.id,
        firstName: application?.firstName,
        lastName: application?.lastName,
        uuIdentity: application?.uuIdentity,
        erasmus: {
          studyProgramme: application?.erasmus?.studyProgramme,
          attachments: application?.erasmus.attachments,
          currentSemester: application?.erasmus.currentSemester,
          motivationDescription: application?.erasmus.motivationDescription,
          destionations: application?.erasmus.destionations,
          language: application?.erasmus.language,
          personalInfo: application?.erasmus.personalInfo,
          interviewDate: application?.erasmus.interviewDate,
          requestForPrerecogn: "false",
          insurance: application?.erasmus.insurance,
          learningAgreement: application?.erasmus.learningAgreement,
          grant: application?.erasmus.grant,
          semesterYear: application?.erasmus.semesterYear,
          erasmusState: application?.erasmus.erasmusState,
          personalNote: application?.erasmus?.personalNote || "",
          noteForCoordinator: application?.erasmus?.noteForCoordinator || "",
        }
      }
      try {
        await applicationDataObject.handlerMap.update(newData);
        setShowAddModal(false)
      } catch (e) {
        opt.component.saveFail(e);
        return;
      }
      opt.component.saveDone();
      setStepIndex(0)
      setShowAddModal(false)

    }

    function handleAddFail({ component, dtoOut: e }) {
      console.error(e);
      component.getAlertBus().addAlert({
        content: <Error errorData={e} />,
        colorSchema: "danger",
      });
    }

    function handleAddDone({ component }) {
      clearForm(component);
      setShowAddModal(false);
    }

    function clearForm(form) {
      if (form.reset) form.reset();
      if (form.getAlertBus) form.getAlertBus().clearAlerts();
    }

    return (
      <>
        <DataObjectStateResolver dataObject={applicationDataObject} >
          <UU5.Bricks.ConfirmModal ref={confirm} />
          <UU5.Bricks.Button
            borderRadius={props.buttonBorderRadius}
            bgStyle={props.buttonBgStyle}
            elevation={props.buttonElevation}
            colorSchema={props.buttonColoSchema}
            onClick={handleOpenAddRequestSubjectsRecognitionForm}
            disabled={!isEnabled}
            style={{ height: '2.5rem', display: 'block', marginTop: '0.2rem' }}>
            <UU5.Bricks.Text colorSchema='white' style={{ fontSize: '16px' }}>
              <UU5.Bricks.Lsi lsi={Lsi.requestForm} />
            </UU5.Bricks.Text>
          </UU5.Bricks.Button>
          <AddRequestSubjectsRecognition
            shown={showAdd}
            onCancel={handleAddRequestRecognitionCancel}
            onSave={handleAddRequestRecognitionSave}
            onSaveDone={handleAddDone}
            onSaveFail={handleAddFail}
            stepIndex={stepIndex}
            confirm={confirm}
            handleOnChangeStep={handleOnChangeStep}
            colorSchema={props.colorSchema}
            borderRadius={props.borderRadius}
            elevation={props.elevation}
            bgStyle={props.bgStyle}
          />
        </DataObjectStateResolver>
      </>
    )
  }
})
//viewOff:helpers

//viewOn:exports
export default RequestSubjectsRecognitionView;
//viewOff:exports
