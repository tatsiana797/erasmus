//@@viewOn:imports
import UU5 from "uu5g04";
import { createVisualComponent } from "uu5g04-hooks";
import "uu5g04-forms";
import Config from "../config/config";
import RequestSubjectsRecognitionView from "./request-subjects-recognition/request-subjects-recognition-view";
import ApplicationLoader from "../common/loader/application-loader"

//@@viewOff:imports
const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "SubjectsRecognition"
  //@@viewOff:statics
};
export const SubjectsRecognition = createVisualComponent({
  ...STATICS,
  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
    uuIdentity: UU5.PropTypes.string,
    role: UU5.PropTypes.string,
    colorSchema: UU5.PropTypes.string,
    borderRadius: UU5.PropTypes.string,
    elevation: UU5.PropTypes.string,
    bgStyle: UU5.PropTypes.string,
    isStudent: UU5.PropTypes.bool,
    isStudyDepOrCoordinator: UU5.PropTypes.bool,
    buttonColoSchema: UU5.PropTypes.string,
    buttonBorderRadius: UU5.PropTypes.string,
    buttonElevation: UU5.PropTypes.string,
    buttonBgStyle: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
    uuIdentity: undefined,
    colorSchema: undefined,
    borderRadius: undefined,
    elevation: undefined,
    bgStyle: undefined,
    isStudent: false,
    isStudyDepOrCoordinator: false,
    buttonColoSchema: undefined,
    buttonBorderRadius: undefined,
    buttonElevation: undefined,
    buttonBgStyle: undefined,
  },
  //@@viewOff:defaultProps
  render(props) {

    //@@viewOn:render
    return (
      <ApplicationLoader baseUri={props.baseUri} uuIdentity={props.uuIdentity} skipInitialLoad={false}>
        <UU5.Bricks.Div style={{ display: "block", padding: "5px" }}>
          <RequestSubjectsRecognitionView
            isStudent={props.isStudent}
            isStudyDepOrCoordinator={props.isStudyDepOrCoordinator}
            uuIdentity={props.uuIdentity}
            colorSchema={props.colorSchema}
            borderRadius={props.borderRadius}
            elevation={props.elevation}
            bgStyle={props.bgStyle}
            buttonBorderRadius={props.buttonBorderRadius}
            buttonColoSchema={props.buttonColoSchema}
            buttonElevation={props.buttonElevation}
            buttonBgStyle={props.buttonBgStyle}
          />
        </UU5.Bricks.Div>
      </ApplicationLoader>)
  }
})
//viewOn:exports
export default SubjectsRecognition;
//viewOff:exports
