/**
 * Server calls of application client.
 */
import UU5 from "uu5g04";
import Plus4U5 from "uu_plus4u5g01";

let Calls = {
  /** URL containing app base, e.g. "https://uuapp.plus4u.net/vendor-app-subapp/awid/". */
  APP_BASE_URI: location.protocol + "//" + location.host + UU5.Environment.getAppBasePath(),

  async call(method, url, dtoIn, clientOptions) {
    let response = await Plus4U5.Common.Calls.call(method, url, dtoIn, clientOptions);
    return response.data;
  },

  loadDemoContent(dtoIn) {
    let commandUri = Calls.getCommandUri("loadDemoContent");
    return Calls.call("get", commandUri, dtoIn);
  },

  loadIdentityProfiles() {
    let commandUri = Calls.getCommandUri("sys/uuAppWorkspace/initUve");
    return Calls.call("get", commandUri, {});
  },

  initWorkspace(dtoInData) {
    let commandUri = Calls.getCommandUri("sys/uuAppWorkspace/init");
    return Calls.call("post", commandUri, dtoInData);
  },

  getWorkspace() {
    let commandUri = Calls.getCommandUri("sys/uuAppWorkspace/get");
    return Calls.call("get", commandUri, {});
  },

  async initAndGetWorkspace(dtoInData) {
    await Calls.initWorkspace(dtoInData);
    return await Calls.getWorkspace();
  },

  applicationUpdate(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("application/update", baseUri);
    return Calls.call("post", commandUri, dtoIn);
  },

  createApplication(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("application/create", baseUri);
    return Calls.call("post", commandUri, dtoIn);
  },

  applicationList(dtoIn) {
    const commandUri = Calls.getCommandUri("application/list", dtoIn.baseUri);
    return Calls.call("get", commandUri, dtoIn);
  },

  applicationUpdateList(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("application/updateList", baseUri);
    return Calls.call("post", commandUri, dtoIn);
  },

  getApplicationByUuIdentity(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("application/getByUuIdentity", baseUri);
    return Calls.call("get", commandUri, dtoIn);
  },

  updateAttachment(baseUri, dtoIn) {
    const formData = new FormData();
    for (const key in dtoIn) {
      const element = dtoIn[key];
      formData.append(key, element)
    }
    const commandUri = Calls.getCommandUri("application/addAttachment", baseUri);
    return Calls.call("post", commandUri, formData);
  },

  removeAttachments(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("application/deleteAttachments", baseUri);
    return Calls.call("post", commandUri, dtoIn);
  },

  getStudent(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("student/get", baseUri);
    return Calls.call("get", commandUri, dtoIn);
  },

  getErasmusInfo(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("erasmusCommon/getErasmusInfo", baseUri);
    return Calls.call("get", commandUri, dtoIn);
  },

  addErasmusInfo(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("erasmusCommon/addErasmusInfo", baseUri);
    return Calls.call("post", commandUri, dtoIn);
  },

  listErasmusInfo(dtoIn) {
    const commandUri = Calls.getCommandUri("erasmusCommon/listErasmusInfo", dtoIn.baseUri);
    return Calls.call("get", commandUri, dtoIn);
  },

  updateErasmusInfo(baseUri, dtoIn) {
    const commandUri = Calls.getCommandUri("erasmusCommon/updateErasmusInfo", baseUri);
    return Calls.call("post", commandUri, dtoIn);
  },

  /*
  For calling command on specific server, in case of developing client site with already deployed
  server in uuCloud etc. You can specify url of this application (or part of url) in development
  configuration in *-client/env/development.json, for example:
   {
     ...
     "uu5Environment": {
       "gatewayUri": "https://uuapp.plus4u.net",
       "awid": "b9164294f78e4cd51590010882445ae5",
       "vendor": "uu",
       "app": "demoappg01",
       "subApp": "main"
     }
   }
   */
  getCommandUri(aUseCase, baseUri) {
    // useCase <=> e.g. "getSomething" or "sys/getSomething"
    // add useCase to the application base URI
    // let targetUriStr = Calls.APP_BASE_URI + aUseCase.replace(/^\/+/, "");

    let properBaseUri = Calls.APP_BASE_URI;
    if (baseUri) properBaseUri = !baseUri.endsWith("/") ? baseUri.concat("/") : baseUri;

    let targetUriStr = properBaseUri + aUseCase.replace(/^\/+/, "");

    // override tid / awid if it's present in environment (use also its gateway in such case)
    if (process.env.NODE_ENV !== "production") {
      let env = UU5.Environment;
      if (env.tid || env.awid || env.vendor || env.app) {
        let url = Plus4U5.Common.Url.parse(targetUriStr);
        if (env.tid || env.awid) {
          if (env.gatewayUri) {
            let match = env.gatewayUri.match(/^([^:]*):\/\/([^/]+?)(?::(\d+))?(\/|$)/);
            if (match) {
              url.protocol = match[1];
              url.hostName = match[2];
              url.port = match[3];
            }
          }
          if (env.tid) url.tid = env.tid;
          if (env.awid) url.awid = env.awid;
        }
        if (env.vendor || env.app) {
          if (env.vendor) url.vendor = env.vendor;
          if (env.app) url.app = env.app;
          if (env.subApp) url.subApp = env.subApp;
        }
        targetUriStr = url.toString();
      }
    }
    return targetUriStr;
  },
};

export default Calls;
