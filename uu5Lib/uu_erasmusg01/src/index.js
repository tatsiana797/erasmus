import UU5 from "uu5g04";
import * as Bricks from "./bricks/bricks.js";
export { Bricks };
export default { Bricks };
// src/index.js (if in library)
import mod from "module";

let uri = ((mod ? mod.uri : (document.currentScript || Array.prototype.slice.call(document.getElementsByTagName("script"), -1)[0] || {}).src) || "").toString();
let libraryBaseUrl = uri ? uri.replace(/^(.*\/).*/, "$1") : "./";
UU5.Environment.changeColorSchema("my-blue1", "my-blue1", libraryBaseUrl + (process.env.NODE_ENV === "production" ? "my-blue1.min.css" : "my-blue1.css"));
if (process.env.NODE_ENV !== "test") {
  console.log(
    `${process.env.NAME}-${process.env.VERSION} © Unicorn\nTerms of Use: https://unicorn.com/tou/${process.env.NAME}`
  );
}
UU5.Environment.addRuntimeLibrary({
  name: process.env.NAME,
  version: process.env.VERSION,
  namespace: process.env.NAMESPACE,
});
