import UU5 from "uu5g04";
import UuErasmus from "uu_erasmusg01";

const { shallow } = UU5.Test.Tools;

describe(`UuErasmus.Bricks.Tracker`, () => {
  it(`default props`, () => {
    const wrapper = shallow(<UuErasmus.Bricks.Tracker />);
    expect(wrapper).toMatchSnapshot();
  });
});
