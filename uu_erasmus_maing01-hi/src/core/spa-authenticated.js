//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useState, SessionProvider } from "uu5g04-hooks";
import Plus4U5 from "uu_plus4u5g01";
import "uu_plus4u5g01-app";
import Config from "./config/config";
import Bottom from "./bottom";
import ErasmusPortal from "../routes/erasmus-portal";
import ApplicationList from "../routes/application-list.js";
import Calls from 'calls';
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "SpaAuthenticated",
  //@@viewOff:statics
};
const CLASS_NAMES = {
  top: () => Config.Css.css`
  .demo-top {
    display: flex;
    justify-content: space-between;
    align-items: center;
    padding: 32px 112px 32px 32px;
  };
  .demo-top-right {
    margin: 0 8px;
  };

  span.uu5-bricks-button-text{
    color: white;
  };
  .plus4u5-app-button-fixed {

    padding-right: 17px!important;
}
.plus4u5-app-button-notification-badge {
  right: 22px!important;
}
`
}

const About = UU5.Common.Component.lazy(() => import("../routes/about"));
const InitAppWorkspace = UU5.Common.Component.lazy(() => import("../routes/init-app-workspace"));
const ControlPanel = UU5.Common.Component.lazy(() => import("../routes/control-panel"));
const baseUri = Calls.APP_BASE_URI;

const DEFAULT_USE_CASE = "erasmus";
const ROUTES = {
  "": DEFAULT_USE_CASE,
  erasmus: { component: <ErasmusPortal baseUri={baseUri} /> },
  'application-list': { component: <ApplicationList baseUri={baseUri} /> },
  about: { component: <About /> },
  "sys/uuAppWorkspace/initUve": { component: <InitAppWorkspace /> },
  controlPanel: { component: <ControlPanel /> },
};

export const SpaAuthenticated = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  //@@viewOff:defaultProps

  render() {

    let [initialActiveItemId] = useState(() => {
      let url = UU5.Common.Url.parse(window.location.href);
      return url.useCase || DEFAULT_USE_CASE;
    });
    const [partnerUniRoute, setPartnerUniRoute] = useState(undefined)

    //@@viewOn:render
    return (
      <Plus4U5.App.MenuProvider activeItemId={initialActiveItemId}>
        <SessionProvider session={UU5.Environment.getSession()}>
          <Plus4U5.App.Page
            topFixed="always"
            className={CLASS_NAMES.top()}
            style={{ background: "#ebebeb" }}
            top={
              <UU5.Bricks.Section colorSchema="white" className='demo-top' style={{ backgroundColor: 'rgb(11, 42, 112)', height: '64px' }}>
                <UU5.Bricks.Div className="demo-top-left">
                  <div style={{ cursor: 'pointer' }} onClick={() => UU5.Environment.getRouter().setRoute("erasmus")}>
                    <UU5.Bricks.Image
                      src="https://uuapp.plus4u.net/uu-appbinarystore-maing02/eb5f2c8c7a06433b9188316ea37cdb1d/binary/getData?accessKey=0cf4cccd803b0f4ca717714009b0bccb.27436102.9b7ea4cbd6663d5f2118ee6436ac0166975183d9&clientAwid=9bbeae2d272b4b28b0f0b3772ce4642d&dataKey=prod2-small_UUN_logo_zahlavi"
                      width={123}
                      height={40}
                    />
                  </div>
                </UU5.Bricks.Div>
                <UU5.Bricks.Div className='demo-top-righ'>
                  <UU5.Bricks.Button onClick={() => UU5.Environment.getRouter().setRoute("")} bgStyle="transparent">
                    <UU5.Bricks.Icon icon='mdi-home' style={{ color: 'white' }} />
                  </UU5.Bricks.Button>
                  <UU5.Bricks.Dropdown label={<UU5.Bricks.Lsi lsi={{ en: "ABOUT US", cs: "O NÁS" }} />} bgStyle="transparent" >

                    <UU5.Bricks.Dropdown.Item label={<UU5.Bricks.Lsi lsi={{ en: "PARTNER UNIVERSITIES", cs: "PARTNERSKÉ UNIVERZITY" }} />} onClick={() => setPartnerUniRoute({ mountedTarget: "_blank" })} />
                  </UU5.Bricks.Dropdown>
                  <UU5.Bricks.Dropdown label="ERASMUS" bgStyle="transparent">
                    <UU5.Bricks.Dropdown.Item label={<UU5.Bricks.Lsi lsi={{ en: "APPLICATION LIST", cs: "SEZNAM ŽÁDOSTÍ" }} />} onClick={() => UU5.Environment.getRouter().setRoute("application-list")} />
                  </UU5.Bricks.Dropdown>
                  <UU5.Bricks.LanguageSelector displayedLanguages={["cs", "en"]} headerMode="code" bgStyle="transparent" />
                  {partnerUniRoute !== undefined ? <UU5.Common.Redirect uri="https://unicornuniversity.net/cs/partners" target={partnerUniRoute} /> : null}
                </UU5.Bricks.Div>
              </UU5.Bricks.Section >}
            bottom={<Bottom />}
            type={3}
            displayedLanguages={["cs", "en"]}
            leftWidth="!xs-300px !s-300px !m-288px !l-288px !xl-288px"
            leftFixed
            leftRelative="m l xl"
            leftResizable="m l xl"
            leftResizableMinWidth={220}
            leftResizableMaxWidth={500}
            isLeftOpen="m l xl"
            showLeftToggleButton
            fullPage
          >
            <Plus4U5.App.MenuConsumer>
              {({ setActiveItemId }) => {
                let handleRouteChanged = ({ useCase, parameters }) => setActiveItemId(useCase || DEFAULT_USE_CASE);
                return <UU5.Common.Router routes={ROUTES} controlled={false} onRouteChanged={handleRouteChanged} />;
              }}
            </Plus4U5.App.MenuConsumer>
          </Plus4U5.App.Page>
        </SessionProvider>
      </Plus4U5.App.MenuProvider>
    );
    //@@viewOff:render
  },
});

export default SpaAuthenticated;
