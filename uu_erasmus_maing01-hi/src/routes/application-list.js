//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useSession } from "uu5g04-hooks";
import "uu_plus4u5g01-bricks";
import Config from "./config/config.js";
import UuErasmus from 'uu_erasmusg01';
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "ApplicationList",
  //@@viewOff:statics
};


export const ApplicationList = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {
    const { identity } = useSession();
    const role = Object.keys(Config.ROLES_LIST).filter(function (key) {
      if (Config.ROLES_LIST[key].some(item => item === identity?.uuIdentity)) {
        return key;
      }
    }).toString()
    const roleHasAccess = Config.ROLE["studyDepartment"] === 'STUDY_DEPARTMENT' || Config.ROLE[role] === 'ERASMUS_COORDINATOR'
    //@@viewOn:render
    return (
      roleHasAccess ? (<UuErasmus.Bricks.ErasmusApplicantsList
        baseUri={props.baseUri}
        colorSchema={"blue"}
        bgStyle={"filled"}
        borderRadius={"5px"}
        elevation={"2"}
      />) : <UU5.Common.Error content={<UU5.Bricks.Lsi lsi={{
        en: "Current address is not valid or you don't have permission to view this page.",
        cs: "Aktuální adresa není platná nebo nemáte oprávnění k zobrazení této stránky."
      }} />} />
    );
    //@@viewOff:render
  },
});

export default ApplicationList;
