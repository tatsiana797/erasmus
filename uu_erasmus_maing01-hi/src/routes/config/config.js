import UU5 from "uu5g04";
import Config from "../../config/config.js";

const TAG = Config.TAG + "Routes.";

export default {
  ...Config,
  ROLES_LIST: {
    studyDepartment: [
      '11-8641-1'
    ],
    erasmusCoordinator: [
      "8193-1",
      "22-8883-1"
    ],
    student: [
      '24-9525-1',
      '1-6384-1',
      '2439-9619-1',
      '6073-576-1',
      '24-9931-1',
      '11-1384-1'
    ]
  },
  ROLE: {
    studyDepartment: "STUDY_DEPARTMENT",
    erasmusCoordinator: "ERASMUS_COORDINATOR",
    student: "STUDENT"
  },
  TAG,
  Css: UU5.Common.Css.createCssModule(
    TAG.replace(/\.$/, "")
      .toLowerCase()
      .replace(/\./g, "-")
      .replace(/[^a-z-]/g, ""),
    process.env.NAME + "/" + process.env.OUTPUT_NAME + "@" + process.env.VERSION // this helps preserve proper order of styles among loaded libraries
  ),
};
