//@@viewOn:imports
import UU5 from "uu5g04";
import "uu5g04-bricks";
import { createVisualComponent, useSession } from "uu5g04-hooks";
import "uu_plus4u5g01-bricks";
import Config from "./config/config.js";
import UuErasmus from 'uu_erasmusg01';
//@@viewOff:imports

const STATICS = {
  //@@viewOn:statics
  displayName: Config.TAG + "ErasmusPortal",
  //@@viewOff:statics
};

export const ErasmusPortal = createVisualComponent({
  ...STATICS,

  //@@viewOn:propTypes
  propTypes: {
    baseUri: UU5.PropTypes.string,
  },
  //@@viewOff:propTypes

  //@@viewOn:defaultProps
  defaultProps: {
    baseUri: undefined,
  },
  //@@viewOff:defaultProps

  render(props) {
    //@@viewOn:render
    const attrs = UU5.Common.VisualComponent.getAttrs(props);
    const { identity } = useSession();
    const role = Object.keys(Config.ROLES_LIST).filter(function (key) {
      if (Config.ROLES_LIST[key].some(item => item === identity.uuIdentity)) {
        return key;
      }
    }).toString()

    const roleHasAccess = Config.ROLE[role] === 'STUDY_DEPARTMENT' || Config.ROLE[role] === 'ERASMUS_COORDINATOR' || Config.ROLE[role] === 'STUDENT'
    const isStudent = Config.ROLE[role] === 'STUDENT'

    return (
      <UU5.Bricks.Div {...attrs} style={{ background: "#ebebeb" }}>
        <UU5.Bricks.Div >
          <UU5.Bricks.Div>
            <UU5.Bricks.Carousel
              interval={5000}
              hideIndicators
              colorSchema="white"
              displayedItems={1}
              type='circular'
            >
              <UU5.Bricks.Carousel.Item>
                <UU5.Bricks.Div
                  style={{
                    backgroundImage: 'url(assets/uni1.jpg)',
                    backgroundSize: "cover",
                    backgroundPosition: "center center",
                    height: 425,
                  }}
                />
              </UU5.Bricks.Carousel.Item>
              <UU5.Bricks.Carousel.Item>
                <UU5.Bricks.Div
                  style={{
                    backgroundImage: 'url(assets/uni3.jpg)',
                    backgroundSize: "cover",
                    backgroundPosition: "center bottom",
                    height: 425,
                  }}
                />
              </UU5.Bricks.Carousel.Item>
              <UU5.Bricks.Carousel.Item>
                <UU5.Bricks.Div
                  style={{
                    backgroundImage: 'url(assets/uni2.jpg)',
                    backgroundSize: "cover",
                    backgroundPosition: "center bottom",
                    height: 425,
                  }}
                />
              </UU5.Bricks.Carousel.Item>
              <UU5.Bricks.Carousel.Item>
                <UU5.Bricks.Div
                  style={{
                    backgroundImage: 'url(assets/uni4.jpg)',
                    backgroundSize: "cover",
                    backgroundPosition: "center center",
                    height: 425,
                  }}
                />
              </UU5.Bricks.Carousel.Item>
            </UU5.Bricks.Carousel>
            {isStudent && (<UU5.Bricks.Div style={{ width: '49rem', margin: '0 auto', paddingTop: '3.2rem', paddingBottom: '3.2rem' }}>
              <UuErasmus.Bricks.ApplicationStatusTracker
                uuIdentity={identity.uuIdentity}
                colorSchema={"indigo"}
                bgStyle={"outline"}
                borderRadius={"3px"}
                elevation={"2"}
                baseUri={props.baseUri}
                trackerColoSchema="my-blue1"
                trackerBorderRadius="5px"
                trackerElevation="3"
                erasmusCoordinator={{
                  fullName: "Marina Purina",
                  uuIdentity: "22-8883-1"
                }}
              />
            </UU5.Bricks.Div>)}
          </UU5.Bricks.Div>
          <UU5.Bricks.Div style={{ height: '20rem', background: '#1CB2C3'}}>
            {roleHasAccess && (<UU5.Bricks.Div style={{ width: '76rem', margin: '0 auto', padding: '2.5rem 2rem 0 0 ' }}>
              <UU5.Bricks.Div style={{ marginRight: '5rem', float: 'left' }}>
                <UU5.Bricks.Card style={{ width: "21rem", height: '15rem', padding: '2rem' }}>
                  <UU5.Bricks.Text style={{ color: '#0b2a70', fontSize: '23px', paddingBottom: '0.8rem', fontWeight: 500 }}>
                    <UU5.Bricks.Lsi lsi={{ en: "Erasmus program", cs: "Erasmus" }} />
                  </UU5.Bricks.Text>
                  <UU5.Bricks.Text style={{ color: 'black', fontSize: '16px', paddingBottom: '0.8rem' }}>
                    <UU5.Bricks.Lsi lsi={{ en: " Are you at least second year student at Unicorn University? Then there is nothing to wait, apply now.", cs: "Jste studentem alespoň druhého ročníku Unicorn University? Pak není na co čekat, přihlaste se hned." }} />
                  </UU5.Bricks.Text>
                  <UuErasmus.Bricks.ErasmusApplication
                    uuIdentity={identity.uuIdentity}
                    isStudent={isStudent}
                    colorSchema={"indigo"}
                    bgStyle={"outline"}
                    borderRadius={"3px"}
                    elevation={"2"}
                    baseUri={props.baseUri}
                    buttonColoSchema="my-blue1"
                    buttonBorderRadius="30px"
                    buttonElevation="3"
                    buttonBgStyle="filled" />
                </UU5.Bricks.Card>
              </UU5.Bricks.Div>
              <UU5.Bricks.Div style={{ marginRight: '5rem', float: 'left' }}>
                <UU5.Bricks.Card style={{ width: "21rem", height: '15rem', padding: '2rem' }}>
                  <UU5.Bricks.Text style={{ color: '#0b2a70', fontSize: '23px', paddingBottom: '0.8rem', fontWeight: 500 }}>
                    <UU5.Bricks.Lsi lsi={{ en: "Subjects recognition", cs: "Uznání předmětů" }} />
                  </UU5.Bricks.Text>
                  <UU5.Bricks.Text style={{ color: 'black', fontSize: '16px', paddingBottom: '0.8rem' }}>
                    <UU5.Bricks.Lsi lsi={{ en: "You can create subjects recognition request after successful submission of Erasmus application.", cs: "Žádost o uznání předmětů můžete vytvořit po úspěšném podání žádosti o Erasmus." }} />
                  </UU5.Bricks.Text>
                  <UuErasmus.Bricks.SubjectsRecognition
                    uuIdentity={identity.uuIdentity}
                    colorSchema={"indigo"}
                    bgStyle={"outline"}
                    borderRadius={"3px"}
                    elevation={"2"}
                    baseUri={props.baseUri}
                    isStudent={isStudent}
                    buttonColoSchema="my-blue1"
                    buttonBorderRadius="30px"
                    buttonElevation="3"
                    buttonBgStyle="filled"
                  />
                </UU5.Bricks.Card>
              </UU5.Bricks.Div>
              <UU5.Bricks.Div style={{ display: 'flex' }}>
                <UU5.Bricks.Card style={{ width: "21rem", height: '15rem', padding: '2rem' }}>
                  <UU5.Bricks.Text style={{ color: '#0b2a70', fontSize: '23px', paddingBottom: '0.8rem', fontWeight: 500 }}>
                    <UU5.Bricks.Lsi lsi={{ en: "Internship program", cs: "Stážový program" }} />
                  </UU5.Bricks.Text>
                  <UU5.Bricks.Text style={{ color: 'black', fontSize: '16px', paddingBottom: '0.8rem' }}>
                    <UU5.Bricks.Lsi lsi={{ en: "For all students and graduates of Unicorn University.", cs: "Pro všechny studenty a absolventy Unicorn University." }} />
                  </UU5.Bricks.Text>
                  <UU5.Bricks.Button
                    colorSchema="my-blue1"
                    borderRadius="30px"
                    style={{ height: '2.5rem', display: 'block', marginTop: '1.8rem' }}>
                    <UU5.Bricks.Text colorSchema='white' style={{ fontSize: '16px' }}>
                      <UU5.Bricks.Lsi lsi={{ en: "Apply for internship", cs: "Přihlásit se na stáž" }} />
                    </UU5.Bricks.Text>
                  </UU5.Bricks.Button>
                </UU5.Bricks.Card >
              </UU5.Bricks.Div>

            </UU5.Bricks.Div>)}
          </UU5.Bricks.Div>
        </UU5.Bricks.Div>
      </UU5.Bricks.Div>
    );
    //@@viewOff:render
  },
});

export default ErasmusPortal;
