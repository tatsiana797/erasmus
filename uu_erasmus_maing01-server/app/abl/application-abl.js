"use strict";
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory, ObjectStoreError } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/application-error.js");
const ErrorsData = require("../api/errors/erasmus-data-error.js");
const { BinaryStoreError } = require("uu_appg01_binarystore");

const WARNINGS = {
  createUnsupportedKeys: {
    code: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
  listUnsupportedKeys: {
    code: `${Errors.List.UC_CODE}unsupportedKeys`,
  },
  updateUnsupportedKeys: {
    code: `${Errors.Update.UC_CODE}unsupportedKeys`,
  },
  getAttachmentUnsupportedKeys: {
    code: `${ErrorsData.GetAttachment.UC_CODE}unsupportedKeys`,
  },
  deleteAttachmentsUnsupportedKeys: {
    code: `${ErrorsData.DeleteAttachments.UC_CODE}unsupportedKeys`,
  },
  addAttachmentUnsupportedKeys: {
    code: `${ErrorsData.AddAttachment.UC_CODE}unsupportedKeys`,
  },
  getByUuIdentityUnsupportedKeys: {
    code: `${Errors.GetByUuIdentity.UC_CODE}unsupportedKeys`,
  },
  updateListUnsupportedKeys: {
    code: `${Errors.UpdateList.UC_CODE}unsupportedKeys`,
  }
};

class ApplicationAbl {

  constructor() {
    this.applicationDao = DaoFactory.getDao("application")
    this.validator = Validator.load();
    this.applicationDataDao = DaoFactory.getDao("applicationData");
    this.erasmusCommonDao = DaoFactory.getDao("erasmusCommon");
  }

  async updateList(awid, dtoIn) {
    let validationResult = this.validator.validate("applicationUpdateDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.updateListUnsupportedKeys.code,
      Errors.UpdateList.InvalidDtoIn);
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.applicationDao.updateList(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.UpdateList.ApplicationDaoUpdateListFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async getByUuIdentity(awid, dtoIn) {
    let validationResult = this.validator.validate("applicationGetByUuIdentityDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getByUuIdentityUnsupportedKeys.code,
      Errors.GetByUuIdentity.InvalidDtoIn
    );
    let application = await this.applicationDao.getByUuIdentity(awid, dtoIn.uuIdentity);
    if (!application) {
      console.warn("Application have not been created yet, no data to display")
      return {};
    } else {
      application.uuAppErrorMap = uuAppErrorMap;
      return application;
    }
  }

  async getAttachment(awid, dtoIn) {
    let validationResult = this.validator.validate("applicationGetAttachmentDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.getAttachmentUnsupportedKeys.code,
      ErrorsData.GetAttachment.InvalidDtoIn);
    let dtoOut;
    try {
      dtoOut = await this.applicationDataDao.getDataByCode(awid, dtoIn.code)
    } catch (e) {
      if (e instanceof BinaryStoreError) {
        throw new ErrorsData.GetAttachment.ApplicationDataDaoGetAttachmentFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoIn.awid = awid;
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async deleteAttachments(awid, dtoIn) {
    console.log(dtoIn);
    let validationResult = this.validator.validate("deleteAttachmentDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.deleteAttachmentsUnsupportedKeys.code,
      ErrorsData.DeleteAttachments.InvalidDtoIn);
    let application = await this.applicationDao.get(dtoIn.studentId);
    if (!application) {
      throw new Errors.DeleteAttachments.ApplicationDaoGetFailed({ uuAppErrorMap }, { studentId: dtoIn.studentId });
    }
    try {
      await this.applicationDataDao.deleteManyByCode(dtoIn.code)
    } catch (e) {
      if (e instanceof BinaryStoreError) {
        throw new ErrorsData.DeleteAttachments.ApplicationDataDaoDeleteAttachmentsFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.applicationDao.remove(application, dtoIn.list ? dtoIn.list : false);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.DeleteAttachments.ApplicationDaoRemoveFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    return dtoOut;
  }

  async update(awid, dtoIn) {
    let validationResult = this.validator.validate("applicationUpdateDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.updateUnsupportedKeys.code,
      Errors.Update.InvalidDtoIn);
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.applicationDao.update(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.Update.ApplicationDaoUpdateFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async addAttachment(awid, dtoIn) {
    let validationResult = this.validator.validate("addAttachmentDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.addAttachmentUnsupportedKeys.code,
      ErrorsData.AddAttachment.InvalidDtoIn);
    let application;
    application = await this.applicationDao.get(dtoIn.studentId);
    if (!application) {
      application = await this.applicationDao.create({ studentId: dtoIn.studentId });
    }
    if (!('erasmus' in application)) {
      application["erasmus"] = {
        "attachments":
        {
          cv: "",
          ml: "",
          syllabus: [],
          file: []
        },
        erasmusState: "active"
      }
    }

    let studentData;
    if (dtoIn.cv != "" && dtoIn.cv) {
      console.log(dtoIn.cv);
      try {
        studentData = await this.applicationDataDao.create({ awid }, dtoIn.cv);
      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.Create.ApplicationDataDaoCreateFailed({ uuAppErrorMap }, e);
        }
        throw e;
      }
      application.erasmus.attachments.cv = { code: studentData.code, name: dtoIn.cv.filename }
    }
    if (dtoIn.cv == "") {
      try {
        await this.applicationDataDao.deleteByCode(awid, application.erasmus.attachments.cv.code);
      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.DeleteAttachments.ApplicationDataDaoDeleteFailed({ uuAppErrorMap }, e);
        }
      }
      application.erasmus.attachments.cv = ""
    }

    if (dtoIn.ml != "" && dtoIn.ml) {
      try {
        studentData = await this.applicationDataDao.create({ awid }, dtoIn.ml);
      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.Create.ApplicationDataDaoCreateFailed({ uuAppErrorMap }, e);
        }
        throw e;
      }
      application.erasmus.attachments.ml = { code: studentData.code, name: dtoIn.ml.filename }
    }

    if (dtoIn.ml == "") {
      try {
        await this.applicationDataDao.deleteByCode(awid, application.erasmus.attachments.ml.code);
      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.DeleteAttachments.ApplicationDataDaoDeleteFailed({ uuAppErrorMap }, e);
        }
      }
      application.erasmus.attachments.ml = ""
    }

    if (dtoIn.syllabus && dtoIn.syllabus != '') {
      console.log(dtoIn, application);
      try {
        studentData = await this.applicationDataDao.create({ awid }, dtoIn.syllabus)

      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.Create.ApplicationDataDaoCreateFailed({ uuAppErrorMap }, e);
        }
        throw e;
      }
      application.erasmus.attachments.syllabus.push({ syllabusId: dtoIn.syllabusId, code: studentData.code, name: dtoIn.syllabus.filename });
    }

    if (dtoIn.syllabus == "") {
      let code = application.erasmus.attachments.syllabus.filter(item => {
        return item.syllabusId == dtoIn.syllabusId
      }).map(item => item.code)
      try {
        await this.applicationDataDao.deleteByCode(awid, code[0]);
      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.DeleteAttachments.ApplicationDataDaoDeleteFailed({ uuAppErrorMap }, e);
        }
      }
      application.erasmus.attachments.syllabus = application.erasmus.attachments.syllabus.filter(item => {
        return item.syllabusId != dtoIn.syllabusId
      })
    }

    if (dtoIn.file && dtoIn.file != '') {
      try {
        studentData = await this.applicationDataDao.create({ awid }, dtoIn.file)

      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.Create.ApplicationDataDaoCreateFailed({ uuAppErrorMap }, e);
        }
        throw e;
      }
      application.erasmus.attachments.file.push({ fileId: dtoIn.fileId, code: studentData.code, name: dtoIn.file.filename });
    }

    if (dtoIn.file == "") {
      let code = application.erasmus.attachments.file.filter(item => {
        return item.fileId == dtoIn.fileId
      }).map(item => item.code)
      try {
        await this.applicationDataDao.deleteByCode(awid, code[0]);
      } catch (e) {
        if (e instanceof BinaryStoreError) {
          throw new ErrorsData.DeleteAttachments.ApplicationDataDaoDeleteFailed({ uuAppErrorMap }, e);
        }
      }
      application.erasmus.attachments.file = application.erasmus.attachments.file.filter(item => {
        return item.fileId != dtoIn.fileId
      })
    }

    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.applicationDao.update(application);
    } catch (e) { // A4
      if (studentData) {
        await this.applicationDataDao.deleteByCode(awid, studentData.code);
      }
      if (e instanceof ObjectStoreError) {
        throw new ErrorsData.DeleteAttachments.ApplicationDataDaoDeleteFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async list(awid, dtoIn) {
    let validationResult = this.validator.validate("applicationListDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.listUnsupportedKeys.code,
      Errors.List.InvalidDtoIn
    );
    let applicationList;
    applicationList = await this.applicationDao.list(awid);
    applicationList.uuAppErrorMap = uuAppErrorMap;
    return applicationList;
  }

  async create(awid, dtoIn) {
    let validationResult = this.validator.validate("applicationCreateDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.createUnsupportedKeys.code, Errors.Create.InvalidDtoIn);
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.applicationDao.create(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.Create.ApplicationDaoCreateFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

}

module.exports = new ApplicationAbl();
