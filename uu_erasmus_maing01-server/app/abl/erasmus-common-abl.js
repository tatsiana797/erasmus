"use strict";
const Path = require("path");
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/erasmus-common-error.js");

const WARNINGS = {
  getErasmusInfoUnsupportedKeys: {
    code: `${Errors.GetErasmusInfo.UC_CODE}unsupportedKeys`,
  },
  addErasmusInfoUnsupportedKeys: {
    code: `${Errors.AddErasmusInfo.UC_CODE}unsupportedKeys`,
  },
  updateErasmusInfoUnsupportedKeys: {
    code: `${Errors.UpdateErasmusInfo.UC_CODE}unsupportedKeys`,
  },
  listErasmusInfoUnsupportedKeys: {
    code: `${Errors.ListErasmusInfo.UC_CODE}unsupportedKeys`,
  },
};

const DEFAULTS = {
  sortBy: "lastName",
  order: "asc",
  pageIndex: 0,
  pageSize: 2000
};

class ErasmusCommonAbl {

  constructor() {
    this.validator = Validator.load();
    this.erasmusCommonDao = DaoFactory.getDao("erasmusCommon");
  }

  async listErasmusInfo(awid, dtoIn) {
    let validationResult = this.validator.validate("listErasmusInfoDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.listErasmusInfoUnsupportedKeys.code,
      Errors.ListErasmusInfo.InvalidDtoIn
    );
    if (!dtoIn.sortBy) dtoIn.sortBy = DEFAULTS.sortBy;
    if (!dtoIn.order) dtoIn.order = DEFAULTS.order;
    if (!dtoIn.pageInfo) dtoIn.pageInfo = {};
    if (!dtoIn.pageInfo.pageSize) dtoIn.pageInfo.pageSize = DEFAULTS.pageSize;
    if (!dtoIn.pageInfo.pageIndex) dtoIn.pageInfo.pageIndex = DEFAULTS.pageIndex;
    let listErasmusInfo;
    listErasmusInfo = await this.erasmusCommonDao.listErasmusInfo(awid, dtoIn.sortBy, dtoIn.order, dtoIn.pageInfo);
    listErasmusInfo.uuAppErrorMap = uuAppErrorMap;
    return listErasmusInfo;
  }

  async updateErasmusInfo(awid, dtoIn) {
    let validationResult = this.validator.validate("updateErasmusInfoDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.updateErasmusInfoUnsupportedKeys.code,
      Errors.UpdateErasmusInfo.InvalidDtoIn);

    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.erasmusCommonDao.updateErasmusInfo(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.UpdateErasmusInfo.ErasmusCommonDaoUpdateErasmusInfoFailed({ uuAppErrorMap }, e);
      }    
      throw e;
    };

    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async addErasmusInfo(awid, dtoIn) {
    let validationResult = this.validator.validate("addErasmusInfoDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.addErasmusInfoUnsupportedKeys.code,
      Errors.AddErasmusInfo.InvalidDtoIn
    );
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.erasmusCommonDao.addErasmusInfo(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.AddErasmusInfo.ErasmusCommonDaoAddErasmusInfoFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }

  async getErasmusInfo(awid, dtoIn) {
    let validationResult = this.validator.validate("getErasmusInfoDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getErasmusInfoUnsupportedKeys.code,
      Errors.GetErasmusInfo.InvalidDtoIn
    );
    let commonData = await this.erasmusCommonDao.getErasmusInfo(awid, dtoIn.semesterId);
    if (!commonData) {
      throw new Errors.GetErasmusInfo.ErasmusCommonDaoGetErasmusInfoFailed(uuAppErrorMap);
    }
    commonData.uuAppErrorMap = uuAppErrorMap;
    return commonData;
  }

}

module.exports = new ErasmusCommonAbl();
