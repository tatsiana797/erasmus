"use strict";
const { Validator } = require("uu_appg01_server").Validation;
const { DaoFactory, ObjectStoreError } = require("uu_appg01_server").ObjectStore;
const { ValidationHelper } = require("uu_appg01_server").AppServer;
const Errors = require("../api/errors/student-error.js");

const WARNINGS = {
  getUnsupportedKeys: {
    code: `${Errors.Get.UC_CODE}unsupportedKeys`,
  },
  createUnsupportedKeys: {
    code: `${Errors.Create.UC_CODE}unsupportedKeys`,
  },
};

class StudentAbl {
  constructor() {
    this.validator = Validator.load();
    this.studentDao = DaoFactory.getDao("student");
    this.erasmusCommonDao = DaoFactory.getDao("erasmusCommon");
  }

  async create(awid, dtoIn) {
    let validationResult = this.validator.validate("studentCreateDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(dtoIn, validationResult,
      WARNINGS.createUnsupportedKeys.code, Errors.Create.InvalidDtoIn);
    dtoIn.awid = awid;
    let dtoOut;
    try {
      dtoOut = await this.studentDao.create(dtoIn);
    } catch (e) {
      if (e instanceof ObjectStoreError) {
        throw new Errors.Create.StudentDaoCreateFailed({ uuAppErrorMap }, e);
      }
      throw e;
    }
    dtoOut.uuAppErrorMap = uuAppErrorMap;
    return dtoOut;
  }


  async get(awid, dtoIn) {
    let validationResult = this.validator.validate("studentGetDtoInType", dtoIn);
    let uuAppErrorMap = ValidationHelper.processValidationResult(
      dtoIn,
      validationResult,
      WARNINGS.getUnsupportedKeys.code,
      Errors.Get.InvalidDtoIn
    );
    let student = await this.studentDao.get(awid, dtoIn.uuIdentity);
    if (!student) {
      throw new Errors.Get.StudentDaoGetFailed(uuAppErrorMap, { uuIdentity: dtoIn.uuIdentity });
    }
    student.uuAppErrorMap = uuAppErrorMap;
    return student;
  }

}

module.exports = new StudentAbl();
