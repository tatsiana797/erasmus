"use strict";
const ApplicationAbl = require("../../abl/application-abl.js");

class ApplicationController {

  updateList(ucEnv) {
    return ApplicationAbl.updateList(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  getByUuIdentity(ucEnv) {
    return ApplicationAbl.getByUuIdentity(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  async getAttachment(ucEnv) {
    let dtoOut = await ApplicationAbl.getAttachment(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
    return ucEnv.setBinaryDtoOut(dtoOut, "inline");
  }

  deleteAttachments(ucEnv) {
    return ApplicationAbl.deleteAttachments(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  update(ucEnv) {
    return ApplicationAbl.update(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  addAttachment(ucEnv) {
    return ApplicationAbl.addAttachment(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  list(ucEnv) {
    return ApplicationAbl.list(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  create(ucEnv) {
    return ApplicationAbl.create(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

}

module.exports = new ApplicationController();
