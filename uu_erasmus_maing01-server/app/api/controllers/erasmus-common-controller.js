"use strict";
const StudentAbl = require("../../abl/student-abl.js");
const ErasmusCommonAbl = require("../../abl/erasmus-common-abl.js");

class ErasmusCommonController {

  listErasmusInfo(ucEnv) {
    return ErasmusCommonAbl.listErasmusInfo(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  updateErasmusInfo(ucEnv) {
    return ErasmusCommonAbl.updateErasmusInfo(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  addErasmusInfo(ucEnv) {
    return ErasmusCommonAbl.addErasmusInfo(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }

  getErasmusInfo(ucEnv) {
    return ErasmusCommonAbl.getErasmusInfo(ucEnv.getUri().getAwid(), ucEnv.getDtoIn());
  }
}

module.exports = new ErasmusCommonController();
