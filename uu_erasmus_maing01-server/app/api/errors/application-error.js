"use strict";

const ErasmusMainUseCaseError = require("./erasmus-main-use-case-error.js");
const APPLICATION_ERROR_PREFIX = `${ErasmusMainUseCaseError.ERROR_PREFIX}application/`;

const Create = {
  UC_CODE: `${APPLICATION_ERROR_PREFIX}create/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDaoCreateFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}applicationDaoCreateFailed`;
      this.message = "Create application by application Dao create failed.";
    }
  }
};

const List = {
  UC_CODE: `${APPLICATION_ERROR_PREFIX}list/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDaoListFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${List.UC_CODE}applicationDaoListFailed`;
      this.message = "List application by application Dao list failed.";
    }
  }
};

const Update = {
  UC_CODE: `${APPLICATION_ERROR_PREFIX}update/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDaoUpdateFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Update.UC_CODE}applicationDaoUpdateFailed`;
      this.message = "Update application by application Dao update failed.";
    }
  }
};

const DeleteAttachments = {
  UC_CODE: `${APPLICATION_ERROR_PREFIX}deleteAttachments/`,
  ApplicationDaoGetFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}applicationDaoGetFailed`;
      this.message = "Get application by application Dao get failed.";
    }
  },
  ApplicationDaoRemoveFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}applicationDaoRemoveFailed`;
      this.message = "Remove application by application Dao remove failed.";
    }
  },
};


const GetByUuIdentity = {
  UC_CODE: `${APPLICATION_ERROR_PREFIX}getByUuIdentity/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${GetByUuIdentity.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDaoGetByUuIdentityFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${GetByUuIdentity.UC_CODE}applicationDaoGetByUuIdentityFailed`;
      this.message = "Get application by application Dao getByUuIdentity failed.";
    }
  },
};

const UpdateList = {
  UC_CODE: `${APPLICATION_ERROR_PREFIX}updateList/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${UpdateList.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDaoUpdateListFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${UpdateList.UC_CODE}applicationDaoUpdateListFailed`;
      this.message = "Update application by application Dao updateList failed.";
    }
  }
};

module.exports = {
  UpdateList,
  GetByUuIdentity,
  DeleteAttachments,
  List,
  Create,
  Update,
};
