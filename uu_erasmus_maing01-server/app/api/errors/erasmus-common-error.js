"use strict";

const ErasmusMainUseCaseError = require("./erasmus-main-use-case-error.js");
const ERASMUS_COMMON_ERROR_PREFIX = `${ErasmusMainUseCaseError.ERROR_PREFIX}erasmusCommon/`;

const GetErasmusInfo = {
  UC_CODE: `${ERASMUS_COMMON_ERROR_PREFIX}getErasmusInfo/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${GetErasmusInfo.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ErasmusCommonDaoGetErasmusInfoFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${GetErasmusInfo.UC_CODE}erasmusCommonDaoGetErasmusInfoFailed`;
      this.message = "Get common Erasmus information by ErasmusCommon Dao getErasmusInfo failed.";
    }
  }
};

const AddErasmusInfo = {
  UC_CODE: `${ERASMUS_COMMON_ERROR_PREFIX}addErasmusInfo/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${AddErasmusInfo.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ErasmusCommonDaoAddErasmusInfoFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${AddErasmusInfo.UC_CODE}erasmusCommonDaoAddErasmusInfoFailed`;
      this.message = "Add common Erasmus information by ErasmusCommon Dao addErasmusInfo failed.";
    }
  }
};

const UpdateErasmusInfo = {
  UC_CODE: `${ERASMUS_COMMON_ERROR_PREFIX}updateErasmusInfo/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${UpdateErasmusInfo.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ErasmusCommonDaoUpdateErasmusInfoFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${UpdateErasmusInfo.UC_CODE}erasmusCommonDaoUpdateErasmusInfoFailed`;
      this.message = "Update common Erasmus information by ErasmusCommon Dao updateErasmusInfo failed.";
    }
  }
};

const ListErasmusInfo = {
  UC_CODE: `${ERASMUS_COMMON_ERROR_PREFIX}listErasmusInfo/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${ListErasmusInfo.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
};

module.exports = {
  ListErasmusInfo,
  UpdateErasmusInfo,
  AddErasmusInfo,
  GetErasmusInfo,
};
