"use strict";

const ErasmusMainUseCaseError = require("./erasmus-main-use-case-error.js");
const ERASMUS_DATA_ERROR_PREFIX = `${ErasmusMainUseCaseError.ERROR_PREFIX}erasmus-data/`;

const GetAttachment = {
  UC_CODE: `${ERASMUS_DATA_ERROR_PREFIX}getAttachment/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${GetAttachment.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDataDaoGetAttachmentFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${GetAttachment.UC_CODE}applicationDaoGetFailed`;
      this.message = "Get file by applicationData Dao getDataByCode failed.";
    }
  }
};

const DeleteAttachments = {
  UC_CODE: `${ERASMUS_DATA_ERROR_PREFIX}deleteAttachments/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDataDaoDeleteAttachmentsFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}applicationDataDaoDeleteAttachmentsFailed`;
      this.message = "Delete attachments application by applicationData Dao deleteManyByCode failed.";
    }
  },
  ApplicationDataDaoDeleteFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}applicationDataDaoDeleteFailed`;
      this.message = "Delete attachments application by applicationData Dao deleteByCode failed.";
    }
  },
};

const AddAttachment = {
  UC_CODE: `${ERASMUS_DATA_ERROR_PREFIX}addAttachment/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDataDaoAddAttachmentFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${DeleteAttachments.UC_CODE}applicationDataDaoDeleteAttachmentsFailed`;
      this.message = "Delete attachments application by applicationData Dao deleteManyByCode failed.";
    }
  },
};

const Create = {
  UC_CODE: `${ERASMUS_DATA_ERROR_PREFIX}addAttachment/`,
  InvalidDtoIn: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}invalidDtoIn`;
      this.message = "DtoIn is not valid.";
    }
  },
  ApplicationDataDaoCreateFailed: class extends ErasmusMainUseCaseError {
    constructor() {
      super(...arguments);
      this.code = `${Create.UC_CODE}applicationDataDaoCreateFailed`;
      this.message = "Add file by applicationData Dao create failed.";
    }
  },
};


module.exports = {
  GetAttachment,
  DeleteAttachments,
  AddAttachment,
  Create
};
