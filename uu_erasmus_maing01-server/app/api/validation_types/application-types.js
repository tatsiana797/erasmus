// /* eslint-disable */
const applicationCreateDtoInType = shape({
    studentId: string(100).isRequired(),
})

const applicationGetByUuIdentityDtoInType = shape({
    uuIdentity: string(50).isRequired()
})

const applicationListDtoInType = shape({
})

const addAttachmentDtoInType = shape({
    studentId: string(100).isRequired(),
    cv: binary(),
    ml: binary(),
    syllabus: binary(),
    syllabusId: string(100),
    file: binary(),
    fileId: string(100)
})

const deleteAttachmentDtoInType = shape({
    list: boolean(),
    studentId: string(100).isRequired(),
    id: id(),
    code: array(),
})

const applicationGetAttachmentDtoInType = shape({
    code: string(500).isRequired()
})

const applicationUpdateDtoInType = shape({
    id: id().isRequired(),
    studentId: string(100).isRequired(),
    firstName: string(100),
    lastName: string(100),
    uuIdentity: string(50),
    erasmus: shape({
        personalInfo: shape({
            dateOfBirth: string(20),
            nationality: string(100),
            email: string(100),
            phone: string(100),
            adress: string(500),
            banckAccount: string(100),
            bankName: string(100),
            swift: string(100),
        }),
        studyProgramme: string(50),
        personalNote: string(1500),
        noteForCoordinator: string(1500),
        attachments: shape({
            cv: shape({
                code: string(500),
                name: string(500)
            }),
            ml: shape({
                code: string(500),
                name: string(500)
            }),
            syllabus: array(shape({
                code: string(500),
                syllabusId: string(500),
                name: string(500)
            })),
            file: array(shape({
                code: string(500),
                fileId: string(500),
                name: string(500)
            }))
        }),
        language: array(shape({
            name: string(500),
            level: string(500),
        }), 5),
        erasmusState: oneOf(["active", "closed"]),
        currentSemester: oneOf(["1", "2", "3", "4", "5", "6", "7", "8", "9", "10"]),
        semesterYear: string(500),
        motivationDescription: string(5000),
        interviewDate: string(5000),
        requestForPrerecogn: string(5000),
        insurance: string(5000),
        learningAgreement: string(5000),
        grant: string(5000),
        destionations: array(shape({
            universityApprovalStatus: oneOf(["initial", "alternative-final", "final", "canceled"]),
            order: string(500),
            school: string(500),
            country: string(500),
            from: Date(),
            to: Date(),
            numberOfCreadits: string(500)
        }), 3),
        subjectsRecognition: array(shape({
            subjectNameForeignUni: string(250),
            subjectNameUcl: string(250)
        }), 10),
    })
})

