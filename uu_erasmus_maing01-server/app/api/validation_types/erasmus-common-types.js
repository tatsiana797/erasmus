/* eslint-disable */
const getErasmusInfoDtoInType = shape({
    semesterId: string(100).isRequired(),
})

const addErasmusInfoDtoInType = shape({
    semesterId: string(100).isRequired(),
    commonNotes: string(3000),
    interviewDates: array(),
})

const updateErasmusInfoDtoInType = shape({
    id: id().isRequired(),
    interviewDates: array(),
    commonNotes: string(3000),
})

const listErasmusInfoDtoInType = shape({
    sortBy: uu5String(),
    order: oneOf(["asc", "desc"]),
    pageInfo: shape({
        pageIndex: integer(),
        pageSize: integer(),
    }),
})