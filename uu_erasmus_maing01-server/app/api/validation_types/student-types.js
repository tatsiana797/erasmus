// /* eslint-disable */

const studentGetDtoInType = shape({
    uuIdentity: string(50).isRequired()
})

const studentCreateDtoInType = shape({
    uuIdentity: string(50).isRequired(),
    firstName: string(100).isRequired(),
    lastName: string(100).isRequired()
})