"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;

class ApplicationMongo extends UuObjectDao {

  async createSchema() {
    await super.createIndex({ awid: 1, id: 1 }, { unique: true });
  }

  async create(uuObject) {
    return await super.insertOne(uuObject);
  };

  async get(studentId) {
    return await super.findOne({ studentId: studentId });
  };

  async getByUuIdentity(awid, uuIdentity) {
    return await super.findOne({ awid, uuIdentity });
  };

  async list(awid) {
    return await super.aggregate([
      { "$sort": { "firstName": 1 } },
      {
        "$group": {
          "_id": "$erasmus.semesterYear",
          "itemList": {
            "$push": {
              "erasmus": "$erasmus",
              "studentId": "$studentId",
              "sys": "$sys",
              "applicationId": "$_id",
              "awid": awid,
              "firstName": "$firstName",
              "lastName": "$lastName",
              "uuIdentity": "$uuIdentity"
            }
          }
        }
      },
      { "$sort" : { "_id" : -1} }
    ])
  }

  async update(application) {
    let filter = { id: application.id };
    return await super.findOneAndUpdate(filter, application, "NONE");
  }

  async updateList(application) {
    let filter = { id: application.id };
    await super.findOneAndUpdate(filter, application, "NONE");
    return this.list()
  }

  async remove(uuObject, list) {
    let filter = {
      id: uuObject.id,
    };
    if (list) {
      await super.deleteOne(filter)
      return this.list()
    } else {
      return await super.deleteOne(filter)
    }
  }
}

module.exports = ApplicationMongo;
