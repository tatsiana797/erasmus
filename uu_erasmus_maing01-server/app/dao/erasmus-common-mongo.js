"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;

class ErasmusCommonMongo extends UuObjectDao {

  async createSchema() {
    await super.createIndex({ awid: 1, id: 1 }, { unique: true });
  }

  async updateErasmusInfo(dtoIn) {
    let filter = { id: dtoIn.id, awid: dtoIn.awid };
    return await super.findOneAndUpdate(filter, dtoIn, "NONE");
  }

  async addErasmusInfo(uuObject) {
    return await super.insertOne(uuObject);
  };

  async getErasmusInfo(awid, semesterId) {
    return await super.findOne({ awid, semesterId });
  };

  async listErasmusInfo(awid, sortBy, order, pageInfo) {
    const filter = {
      awid
    };
    const sort = {
      [sortBy]: order === "asc" ? 1 : -1,
    };
    return await super.find(filter, pageInfo, sort);
  }

}

module.exports = ErasmusCommonMongo;
