"use strict";
const { UuObjectDao } = require("uu_appg01_server").ObjectStore;

class StudentMongo extends UuObjectDao {

  async createSchema() {
    await super.createIndex({ awid: 1, id: 1 }, { unique: true });
  };

  async get(awid, uuIdentity) {
    return await super.findOne({ awid, uuIdentity });
  };

  async create(uuObject) {
    console.log(uuObject);
    return await super.insertOne(uuObject);
  };
}

module.exports = StudentMongo;
